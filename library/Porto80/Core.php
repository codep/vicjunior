<?php 
class Porto80_Core extends Zend_Controller_Plugin_Abstract {
	
	public function preDispatch(Zend_Controller_Request_Abstract $request) {
		
		//RESGATANDO OBJETO VIEW
		$view = Zend_Controller_Action_HelperBroker::getExistingHelper('ViewRenderer')->view;
				
		//SETANDO MENU ATIVO
		$view->menu_ativo = $request->getControllerName();
		
		$view->headMeta()
			->appendHttpEquiv('Content-Type', 'text/html; charset=iso-8859-1')
			->appendHttpEquiv('Content-Language', 'pt-BR')
			->appendHttpEquiv('X-UA-Compatible', 'IE=EmulateIE7', array('conditional' => 'IE'))
			->setName('COPYRIGHT', 'Porto80.com.br')
			->setName('AUTHOR', 'Porto80.com.br')
		 ;
		
		$csite = array('index', 'contato', 'error', 'usuario', 'quem-somos', 'profissionais', 'produtos', 'carrinho', 'pedidos','onde-encontro','noticias','pesquisar');
		$cpainel = array('admin-revenda','admin-resumo','admin-configuracao','admin-pedido','admin-cliente','admin-cliente-grupo','admin-produto','admin-produto-categoria','admin-produto-linha','admin-produto-embalagem','admin-produto-relatorio','admin-produto-ordem','admin-usuario','admin-contato','admin-profissional','admin-profissional-categoria','admin-contato','admin-noticia','admin-noticia-categoria');
		
		if ( in_array($view->menu_ativo,$cpainel) ) {

			//PAINEL DE CONTROLE
	
			$view->headScript()
				->appendFile( 'http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js' ,'text/javascript')
				->appendFile( $view->baseUrl('/admin_js/swfobject.js') ,'text/javascript')
				->appendFile( $view->baseUrl('/admin_js/jquery.nyroModal.pack.js') ,'text/javascript')
				->appendFile( $view->baseUrl('/admin_js/notifications.js') ,'text/javascript')
				->appendFile( $view->baseUrl('/admin_js/func.js') ,'text/javascript')
				->appendFile( 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.15/jquery-ui.min.js' ,'text/javascript')
				->appendFile( $view->baseUrl('/admin_js/jquery.ui.datepicker-pt-BR.js') ,'text/javascript')
	        	->appendFile( $view->baseUrl('/admin_js/html5.js'), 'text/javascript',  array('conditional' => 'IE') )
	        	->appendFile( $view->baseUrl('/admin_js/IE8.js'), 'text/javascript',  array('conditional' => 'lt IE 8') )
			;
				
	        $view->headLink()
	        	->appendStylesheet( 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.15/themes/base/jquery-ui.css' )
	        	->appendStylesheet( $view->baseUrl('/admin_css/style.css') )
	        	->appendStylesheet( $view->baseUrl('/admin_css/notifications.css') )
	        	->appendStylesheet( $view->baseUrl('/admin_css/custom.css') )
	        	->appendStylesheet( $view->baseUrl('/admin_css/ie.css'), 'all', 'IE' )
				->headLink(array('rel' => 'shortcut icon', 'href' => $view->baseUrl('admin_img/favicons/favicon.ico')), 'PREPEND')
				->headLink(array('rel' => 'icon', 'href' => $view->baseUrl('admin_img/favicons/favicon.ico')), 'PREPEND')
				->headLink(array('rel' => 'favicon', 'href' => $view->baseUrl('admin_img/favicons/favicon.ico')), 'PREPEND')
				->headLink(array('rel' => 'apple-touch-icon', 'href' => $view->baseUrl('admin_img/favicons/apple.png')), 'PREPEND')
	        ;
	        
	        $layout= Zend_Controller_Action_HelperBroker::getStaticHelper('Layout');
	        
	        $layout->setLayout('layout_admin');
			
		}
		
		if ( in_array($view->menu_ativo,$csite) ) {
			
			$view->headScript()
				->appendFile( 'http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js' ,'text/javascript')
				->appendFile( 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js' ,'text/javascript')				
				->appendFile( $view->baseUrl('/site_js/cufon-yui.js') ,'text/javascript')
				->appendFile( $view->baseUrl('/site_js/myradpro.font.js') ,'text/javascript')
				->appendFile( $view->baseUrl('/site_js/flash_detect.v1.7.js') ,'text/javascript')
				->appendFile( $view->baseUrl('/site_js/jquery.tipTip.min.js') ,'text/javascript')
				->appendFile( $view->baseUrl('/site_js/carrinho.js?v=3') ,'text/javascript')
			;
				
	        $view->headLink()
	        	->appendStylesheet( 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css' )
	        	->appendStylesheet( $view->baseUrl('/site_css/style.css') )
	        	->appendStylesheet( $view->baseUrl('/site_css/tipTip.css') )
				->appendStylesheet( $view->baseUrl('/site_css/notifications.css') )
	        ;
	        
	        $dbAdapter = Zend_Db_Table::getDefaultAdapter ();
	        $select = $dbAdapter->select()->from(array('P'=>'parametros'),array('param_uid_msn','home_horario_func'))->where('status=1')->limit(1);
	        $paramDados = $dbAdapter->fetchRow($select);
	    	$view->skypeid = $paramDados['param_uid_msn'];
	    	$view->horario_func = $paramDados['home_horario_func'];
	    	
	    	/* CLIENTES */
	    	$select = $dbAdapter->select()->from(array('C'=>'cliente'),array('codigo','razao_social'))->joinInner(array('U'=>'usuario'), 'U.id_cliente=C.id_cliente',array('id_usuario'))->order('C.razao_social ASC');
	    	$view->adm_clientes = $dbAdapter->fetchAll($select);
	    	
	    	IF ($request->getControllerName()=='usuario' && ($request->getActionName()=='login' ||  $request->getActionName()=='logout')) {
	    		setcookie("adm_id_usuario", 0,time()-3600,'/');
	    		$view->adm_id_usuario = 0;
	    	} else {
		    	$adm_id_usuario = $request->getPost('adm_cliente','N');
		    	if (is_numeric($adm_id_usuario)) {
		    		setcookie("adm_id_usuario", $adm_id_usuario,0,'/');
		    		$view->adm_id_usuario = $adm_id_usuario;
		    	} else if ( isset($_COOKIE['adm_id_usuario']) && is_numeric($_COOKIE['adm_id_usuario']) ) {
		    		$view->adm_id_usuario = $_COOKIE['adm_id_usuario'];
		    	} else {
		    		$view->adm_id_usuario = 0;
		    	}
	    	}
			
		}
		
        $flashMessenger = Zend_Controller_Action_HelperBroker::getStaticHelper('FlashMessenger');
        $view->flashMsg = $flashMessenger->getMessages();
		
	}
	
	public static function formatUrl($string) {
		
		$url = self::url_amigavel($string);
		
	    if(strpos($url, "://") === false) {
	    	$url = 'http://'.$url;	    	
	    }
	
	    return $url;
		
	}
	
	public static function converteFloat($num='',$round=2, $ponto=true, $moeda=false) {
		
		if ($round==false) $round=2;
		if ($num=='') $num=0;
		if ($moeda===true) $moeda='R$';
		if ($moeda===false) $moeda='';
		
		if (is_float($num) || is_numeric($num)) {
			if ($ponto==true) $num = number_format($num,$round,',','.');
			else $num = number_format($num,$round,',','');
		} else if (strpos($num,',')!==false) {
			$num=preg_replace('/[^0-9,]/','',$num);
			$num = floatval(str_replace(',','.',$num));
			if ($round!=false && is_numeric($round)) $num=round($num,$round);
		}
		return (!empty($moeda)?$moeda.' ':'').$num;
	}
	
	public static function converteCep($cep='') {
		if ($cep=='') return false;
		
		if (is_numeric($cep)) {
			$resultado=substr($cep,0,5).'-'.substr($cep,5,3);
		} else {
			$resultado=preg_replace('/[^0-9]/','',$cep);
		}
		return $resultado;
	}
	
	public static function converteData($data='') {
		if ($data=='') return false;
		
		$zdata = new Zend_Date($data);
		
		if (strpos($data,'/')>0) {
			$resultado=$zdata->toString('YYYY-MM-dd');
		} else {
			$resultado=$zdata->toString('dd/MM/YYYY');
		}
		
		if (strpos($data,':')>0 && substr($data,-8)!='00:00:00') {
			$resultado.=' '.$zdata->toString('HH:mm:ss');
		}
		
		return $resultado;
	}

	public static function converteTelefone($telefone='') {
		if ($telefone=='') return false;
		
		if (is_numeric($telefone)) {
			switch( strlen($telefone) ) {
				default;
					$resultado='';
					break;
				case 8: //xxxx-xxxx
					$resultado=substr($telefone,0,4).'-'.substr($telefone,4,4);
					break;
				case 10: //(xx) xxxx-xxxx
					$resultado='('.substr($telefone,0,2).')'.' '.substr($telefone,2,4).'-'.substr($telefone,6,4);
					break;
				case 12: //+xx xx xxxx-xxxx
					$resultado='+'.substr($telefone,0,2).' '.substr($telefone,2,2).' '.substr($telefone,4,4).'-'.substr($telefone,8,4);
					break;
			}
		} else {
			$resultado=preg_replace('/[^0-9]/','',$telefone);
		}
		return $resultado;
	}
	
	public static function formatEventoData($data='') {
		
    	if ($data=='') {
    		$data = date('Y-m-d');
    	} else {
    		if (!Zend_Date::isDate($data,'YYYY-MM-dd')) {
    			$data = date('Y-m-d');
    		}
    	}
    	
    	
    	
    	$locale = new Zend_Locale('pt_BR');
		$dataobj = new Zend_Date($data, 'YYYY-MM-dd', $locale);
		
    	//$datafinal = $dataobj->toString("EEEE',' d 'de' MMMM 'de' YYYY");
    	$datafinal = $dataobj->toString("d 'de' MMMM 'de' YYYY");
    	
    	$datafinal = ucfirst(utf8_decode($datafinal));
    	
    	return $datafinal;
		
	}
	
	public static function formatDbData($data='') {
		
    	if ($data=='') {
    		$data = date('d/m/Y');
    	} else {
    		if (!Zend_Date::isDate($data,'dd/mm/YYYY')) {
    			$data = date('d/m/Y');
    		}
    	}
    	
    	$locale = new Zend_Locale('pt_BR');
    	
    	$data_nova = implode(preg_match("~-~", $data) == 0 ? "-" : "/", array_reverse(explode(preg_match("~-~", $data) == 0 ? "/" : "-", $data)));
    	
    	return $data_nova;
		
	}
	
	public static function getDepoimentos($total=5, $sort=true) {
		
		$xmldoc = new DOMDocument();
		$xmldoc->load(APPLICATION_PATH . DIRECTORY_SEPARATOR . 'configs' . DIRECTORY_SEPARATOR . 'depoimentos.xml');
		
		$depoimentos = $xmldoc->getElementsByTagName( "depoimento" );
		
		$xml=''; $x=1;

		foreach( $depoimentos as $depoimento ) {
			
			if ($x>$total) break;
			
			$mensagem = $depoimento->getElementsByTagName( "mensagem" );
			$mensagem = utf8_decode($mensagem->item(0)->nodeValue);
			
			$autor = $depoimento->getElementsByTagName( "autor" );
			$autor = utf8_decode($autor->item(0)->nodeValue);

			$xml[] = array('autor'=>$autor,'mensagem'=>$mensagem);
			
			$x++;
		}
		
		if ($xml=='') $xml=array();
		
		if ($sort) sort($xml);

		return $xml;		
		
	} 
	
	public static function getNewMessage() {
		
		try {
			$dbAdapter = Zend_Db_Table::getDefaultAdapter ();
			
			$select = $dbAdapter->select()->from('contato',array('total_mensagem'=>'count(id_contato)'))->where('status=0');
			$total_mensagem = $dbAdapter->fetchOne($select);
			return $total_mensagem;
			
		} catch (Zend_Db_Exception $e) {
			$erro = $e->getMessage();
			return 0;
		}		
		
	}
	
	public static function getProdutoValor($id_produto=0, $local='', $id_cliente=0, $tipo_desconto='', $id_pedido=0) {
		
		if (empty($tipo_desconto) || $tipo_desconto=='undefined') $tipo_desconto='PRAZO'; //PADR�O
		else $tipo_desconto=strtoupper($tipo_desconto);
		try {
			$dbAdapter = Zend_Db_Table::getDefaultAdapter ();
			
			/* PEGANDO DESCONTOS */
	        $select = $dbAdapter->select()->from(array('P'=>'parametros'),array('desconto_vista','desconto_prazo'))->where('status=1')->limit(1);
	        $descontos = $dbAdapter->fetchRow($select);
	        $desconto['VISTA'] = (isset($descontos['desconto_vista']) && is_numeric($descontos['desconto_vista'])?floatval($descontos['desconto_vista']):0);
			$desconto['PRAZO'] = (isset($descontos['desconto_prazo']) && is_numeric($descontos['desconto_prazo'])?floatval($descontos['desconto_prazo']):0);
			
			/* PEGANDO PRODUTO */
			$select = $dbAdapter->select()->from(array('P'=>'produto'))
			->joinInner(array('PE'=>'produto_extra'),'PE.id_produto='.$id_produto)
			->joinInner(array('PC'=>'produto_categoria'),'PC.id_produto_categoria=P.id_produto_categoria',array())
			->joinInner(array('PL'=>'produto_linha'),'PL.id_produto_linha=PC.id_produto_linha',array('id_produto_linha'))
			->where('P.status=1 AND PE.local="'.strtoupper($local).'" AND P.id_produto='.$id_produto);
			$produto = $dbAdapter->fetchRow($select);
			
			if (!$produto) {
				
				$retorno = array('liquido'=>0,'final'=>0,'margem'=>0);
				
			} else {
				
				if ($id_pedido>0) {
					//PEGANDO PRODUTO NO PEDIDO
					$select = $dbAdapter->select()->from(array('PP'=>'pedido_produto'))
					->where('PP.id_pedido='.$id_pedido.' AND PP.id_produto='.$id_produto);
					$pedido_produto = $dbAdapter->fetchRow($select);
					$valor_ajustado = $pedido_produto['valor_ajustado'];
				} else {
					$valor_ajustado=0;
				}
			
				//VERIFICANDO ACRESCIMO
				$cliente_acrescimo=0;
				if ($id_cliente>0) {
					$select = $dbAdapter->select()->from(array('CGA'=>'cliente_grupo_acrescimo'), 'acrescimo')
					->joinInner(array('CGP'=>'cliente_grupo_produto'),'CGP.id_cliente_grupo=CGA.id_cliente_grupo AND CGP.id_produto_linha=CGA.id_produto_linha', array())
					->where('CGP.id_cliente='.$id_cliente.' AND CGA.id_produto_linha='.$produto['id_produto_linha']);
					$cliente_acrescimo = floatval($dbAdapter->fetchOne($select));
				}
				
				//VERIFICANDO SE FOR FORÇADO UM VALOR LIQUIDO
				if ($valor_ajustado>0) $produto['preco'] = $valor_ajustado;
		
				//CALCULANDO VALOR
				$valor_tabela = $produto['preco'] + ($produto['preco'] * $cliente_acrescimo / 100);
				$valor_liquido = $valor_tabela - ($valor_tabela * $produto['desconto'] / 100);
				
				//DESCONTO COND.PAG.
				$valor_desconto = $valor_liquido * $desconto[$tipo_desconto] / 100;
				$valor_liquido = $valor_liquido - $valor_desconto;
				
				//ALIQUOTAS
				$aliquota['frete'] = round($produto['frete'] / 100,4);
				$aliquota['icms_origem'] = round($produto['icms_origem'] / 100,4);
				$aliquota['ipi'] = round($produto['ipi'] / 100,4);
				$aliquota['margem'] = round($produto['margem'] / 100,4);
				$aliquota['iva'] = round($produto['iva'] / 100,4);
				
				//CALCULO VALOR BASE
				$valor_frete = $valor_liquido * $aliquota['frete'];
				$valor_mercadoria = $valor_liquido + $valor_frete; 
				$valor_ipi = $valor_mercadoria * $aliquota['ipi'];
				$valor_mercadoria = $valor_mercadoria + $valor_ipi; 
				$valor_iva = ($valor_mercadoria * $aliquota['iva']);
				$valor_mercadoria = $valor_mercadoria + $valor_iva;
				
				/* //DEBUG  
				echo 'liquido: '.$valor_liquido.'<br>';
				echo 'frete: ('.$aliquota['frete'].'%) '.$valor_frete.'<br>';
				echo 'ipi: ('.$aliquota['ipi'].'%) '.$valor_ipi.'<br>';
				echo 'iva: ('.$aliquota['iva'].'%) '.$valor_iva.'<br>';
				echo 'vl mercadoria: '.round($valor_mercadoria,2).'<br>';
				*/

				//BASE/ICMS
				$valor_base_icms = $valor_liquido + $valor_frete;
				$valor_icms_origem = $valor_base_icms * $aliquota['icms_origem'];
				$valor_base_substrib = $valor_mercadoria;
				$valor_substrib = (($valor_base_substrib * $produto['icms_destino']) / 100) - $valor_icms_origem;
				//FINAL
				$valor_final = $valor_base_icms + $valor_substrib + $valor_ipi;
				//MARGEM
				//$valor_margem = $valor_final + ($valor_final * $aliquota['margem']);
				$valor_margem = $produto['preco'] + ($produto['preco'] * $aliquota['margem']);
				//RETORNO				
				$retorno = array(
					'ajustado'=>round($valor_ajustado,3),
					'liquido'=>round($valor_liquido,3),
					'final'=>round($valor_final,3),
					'margem'=>round($valor_margem,3),
					'baseicms'=>round($valor_base_icms,3),
					'icms_origem'=>round($valor_icms_origem,3),
					'icms_destino'=>$produto['icms_destino'],
					'basesubtrib'=>round($valor_base_substrib,3),
					'subtrib'=>round($valor_substrib,3),
					'ipi'=>round($valor_ipi,3),
					'frete'=>round($valor_frete,3),
					'iva'=>round($valor_iva,3),
					'desconto'=>round($valor_desconto,3)
				);
				
				/* //DEBUG 
					echo $produto['titulo'].' '.$produto['local'];
					echo '<pre>';
					print_r($retorno);
					echo '</pre>'; 
					die();
				*/
			
			}
			
			return $retorno;
			
		} catch (Zend_Db_Exception $e) {
			$erro = $e->getMessage();
			return 0;
		}			
		
	}
	
	public static function getDestaque($total=10) {
		
		try {
			$dbAdapter = Zend_Db_Table::getDefaultAdapter ();
			
			$select = $dbAdapter->select()->from(array('E'=>'evento'),array('E.*'))->joinInner(array('EF'=>'evento_foto'),'EF.id_evento = E.id_evento AND EF.destaque=1')->where('E.status=1 AND E.destaque=1')->order('E.data DESC')->limit($total);
			$dados = $dbAdapter->fetchAll($select);
			return $dados;
			
		} catch (Zend_Db_Exception $e) {
			$erro = $e->getMessage();
			return array();
		}		
		
	} 
	
	public static function getCalendario($mes='', $ano='') {
		
		if ($mes=='') $mes=date('m');
		if ($ano=='') $ano=date('Y');
		
		$calendario=array();
		
		try {
			
			$ultimo_dia = date('d', mktime(0,0,0,$mes+1,0,$ano));
			$semana_primeiro_dia = date('N',mktime(0,0,0,$mes,1,$ano));
						
			$smes=true; $dc=1; $d=1; $ds=1;
			while($smes==true) {
				
				if ($dc>=$semana_primeiro_dia && $dc<=$ultimo_dia+($semana_primeiro_dia-1)) {
					$gd = $d;
					$d++;
				} else {
					$gd = 0;
				}
				$calendario[$dc]['dia_mes']=$gd;
				$calendario[$dc]['semana']=date('W',mktime(0,0,0,$mes,$d,$ano));
				
				if ($ds>=7) {
					if ($dc>$ultimo_dia+$semana_primeiro_dia) {
						$smes=false;
					}
					$ds=0;
				}
				
				$ds++; $dc++;
				
			}
			
		} catch (Zend_Db_Exception $e) {
			$erro = $e->getMessage();
			return array();
		}		
		
		return $calendario;
		
	}
	
	public static function url_amigavel($string, $slug = '-') {
		
		$string = strtolower($string);
	
		// C�digo ASCII das vogais
		$ascii['a'] = range(224, 230);
		$ascii['e'] = range(232, 235);
		$ascii['i'] = range(236, 239);
		$ascii['o'] = array_merge(range(242, 246), array(240, 248));
		$ascii['u'] = range(249, 252);
	
		// C�digo ASCII dos outros caracteres
		$ascii['b'] = array(223);
		$ascii['c'] = array(231);
		$ascii['d'] = array(208);
		$ascii['n'] = array(241);
		$ascii['y'] = array(253, 255);
	
		foreach ($ascii as $key=>$item) {
			$acentos = '';
			foreach ($item as $codigo) $acentos .= chr($codigo);
			$troca[$key] = '/['.$acentos.']/i';
		}
	
		$string = preg_replace(array_values($troca), array_keys($troca), $string);
	
		// Slug?
		if ($slug) {
			// Troca tudo que n�o for letra ou n�mero por um caractere ($slug)
			$string = preg_replace('/[^a-z0-9#\\.]/i', $slug, $string);
			// Tira os caracteres ($slug) repetidos
			$string = preg_replace('/' . $slug . '{2,}/i', $slug, $string);
			$string = trim($string, $slug);
		}
	
		return $string;
	}
	
	public static function enviarEmailPedido($id_pedido=0,$tipo=0) {
		if ($tipo==0 || $id_pedido==0) return false;
		
			$dbAdapter = Zend_Db_Table::getDefaultAdapter();
			$view = Zend_Controller_Action_HelperBroker::getExistingHelper('ViewRenderer')->view;
		
			if ($tipo==3) { //CONCLUIDO
				
				$select = $dbAdapter->select()->from(array('P'=>'pedido'))
				->joinLeft(array('U'=>'usuario'),'U.id_usuario=P.id_usuario',array('U.id_cliente'))
				->joinLeft(array('C'=>'cliente'),'C.id_cliente=U.id_cliente',array('cliente_email'=>'C.email','cliente_nome'=>'C.razao_social','cliente_codigo'=>'C.codigo','cliente_cidade_uf'=>'CONCAT(C.cidade," - ",C.estado)'))
				->where('P.id_pedido='.$id_pedido)
				->limit(1);
				$dados = $dbAdapter->fetchRow($select);
				$cliente_email = $dados['cliente_email'];
				$codigo_pedido = $dados['codigo_pedido'];
				$view->pedido = $dados;
				
				/* PRODUTOS */
				$select = $dbAdapter->select()->from(array('PP'=>'pedido_produto'))
				->joinLeft(array('P'=>'produto'),'P.id_produto=PP.id_produto',array('titulo','codigo'))
				->joinLeft(array('PE'=>'produto_embalagem'),'PE.id_produto_embalagem=P.id_produto_embalagem',array('embalagem'=>'PE.titulo'))
				->where('PP.id_pedido='.$id_pedido)
				->order('PP.quantidade DESC');
				$dados = $dbAdapter->fetchAll($select);
				$view->pedido_produtos = $dados;
				$formulario = $view->render('pedido-modelo.phtml',null,true);
				
			} else if ($tipo==2) { //CANCELADO
				
				$formulario = 'SEU PEDIDO FOI EXCLUIDO.';
				$select = $dbAdapter->select()->from(array('P'=>'pedido'))
				->joinLeft(array('U'=>'usuario'),'U.id_usuario=P.id_usuario',array())
				->joinLeft(array('C'=>'cliente'),'C.id_cliente=U.id_cliente',array('cliente_email'=>'C.email'))
				->where('P.id_pedido='.$id_pedido);
				$cliente_email = $dbAdapter->fetchOne($select);
				$codigo_pedido = '';
				
			} else {
				return false;
			}
			
	    	try {
	    		
	    		//SELECIONANDO EMAIL DE CONTATO
	    		$dbAdapter = Zend_Db_Table::getDefaultAdapter ();
	    		$select = $dbAdapter->select()->from(array('P'=>'parametros'),array('param_email_contato'))->where('status=1')->limit(1);
	    		$emailcontato = $dbAdapter->fetchOne($select);
	    		
				$config = array('ssl' => 'tls', 'port' => 587, 'auth' => 'login', 'username' => 'comercial@vicjunior.com.br', 'password' => 'vicjunior');
				$transport = new Zend_Mail_Transport_Smtp('smtp.gmail.com', $config);
				$mail = new Zend_Mail();
				$mail->setBodyHtml($formulario);
				$mail->setFrom('comercial@vicjunior.com.br', 'VicJunior :: Comercial');
				$mail->addTo($cliente_email);
				$mail->addTo($emailcontato); //C�PIA PARA O VICJUNIOR
				//$mail->addTo('brunotporto@gmail.com'); //DEBUG
				$mail->setSubject('VIC JUNIOR :: PEDIDO '.$codigo_pedido);
				$mail->send($transport);
				return true;
				
	    	} catch (Zend_Mail_Exception $e) {
	    		return false;
	    	}
		
	}

	public static function seems_utf8($str) {
        $length = strlen($str);
        for ($i=0; $i < $length; $i++) {
                $c = ord($str[$i]);
                if ($c < 0x80) $n = 0; # 0bbbbbbb
                elseif (($c & 0xE0) == 0xC0) $n=1; # 110bbbbb
                elseif (($c & 0xF0) == 0xE0) $n=2; # 1110bbbb
                elseif (($c & 0xF8) == 0xF0) $n=3; # 11110bbb
                elseif (($c & 0xFC) == 0xF8) $n=4; # 111110bb
                elseif (($c & 0xFE) == 0xFC) $n=5; # 1111110b
                else return false; # Does not match any model
                for ($j=0; $j<$n; $j++) { # n bytes matching 10bbbbbb follow ?
                        if ((++$i == $length) || ((ord($str[$i]) & 0xC0) != 0x80))
                                return false;
                }
        }
        return true;
	}

	public static function remove_accents($string) {
		if ( !preg_match('/[\x80-\xff]/', $string) )
			return $string;
	
		if (self::seems_utf8($string)) {
			$chars = array(
			// Decompositions for Latin-1 Supplement
			chr(194).chr(170) => 'a', chr(194).chr(186) => 'o',
			chr(195).chr(128) => 'A', chr(195).chr(129) => 'A',
			chr(195).chr(130) => 'A', chr(195).chr(131) => 'A',
			chr(195).chr(132) => 'A', chr(195).chr(133) => 'A',
			chr(195).chr(134) => 'AE',chr(195).chr(135) => 'C',
			chr(195).chr(136) => 'E', chr(195).chr(137) => 'E',
			chr(195).chr(138) => 'E', chr(195).chr(139) => 'E',
			chr(195).chr(140) => 'I', chr(195).chr(141) => 'I',
			chr(195).chr(142) => 'I', chr(195).chr(143) => 'I',
			chr(195).chr(144) => 'D', chr(195).chr(145) => 'N',
			chr(195).chr(146) => 'O', chr(195).chr(147) => 'O',
			chr(195).chr(148) => 'O', chr(195).chr(149) => 'O',
			chr(195).chr(150) => 'O', chr(195).chr(153) => 'U',
			chr(195).chr(154) => 'U', chr(195).chr(155) => 'U',
			chr(195).chr(156) => 'U', chr(195).chr(157) => 'Y',
			chr(195).chr(158) => 'TH',chr(195).chr(159) => 's',
			chr(195).chr(160) => 'a', chr(195).chr(161) => 'a',
			chr(195).chr(162) => 'a', chr(195).chr(163) => 'a',
			chr(195).chr(164) => 'a', chr(195).chr(165) => 'a',
			chr(195).chr(166) => 'ae',chr(195).chr(167) => 'c',
			chr(195).chr(168) => 'e', chr(195).chr(169) => 'e',
			chr(195).chr(170) => 'e', chr(195).chr(171) => 'e',
			chr(195).chr(172) => 'i', chr(195).chr(173) => 'i',
			chr(195).chr(174) => 'i', chr(195).chr(175) => 'i',
			chr(195).chr(176) => 'd', chr(195).chr(177) => 'n',
			chr(195).chr(178) => 'o', chr(195).chr(179) => 'o',
			chr(195).chr(180) => 'o', chr(195).chr(181) => 'o',
			chr(195).chr(182) => 'o', chr(195).chr(184) => 'o',
			chr(195).chr(185) => 'u', chr(195).chr(186) => 'u',
			chr(195).chr(187) => 'u', chr(195).chr(188) => 'u',
			chr(195).chr(189) => 'y', chr(195).chr(190) => 'th',
			chr(195).chr(191) => 'y', chr(195).chr(152) => 'O',
			// Decompositions for Latin Extended-A
			chr(196).chr(128) => 'A', chr(196).chr(129) => 'a',
			chr(196).chr(130) => 'A', chr(196).chr(131) => 'a',
			chr(196).chr(132) => 'A', chr(196).chr(133) => 'a',
			chr(196).chr(134) => 'C', chr(196).chr(135) => 'c',
			chr(196).chr(136) => 'C', chr(196).chr(137) => 'c',
			chr(196).chr(138) => 'C', chr(196).chr(139) => 'c',
			chr(196).chr(140) => 'C', chr(196).chr(141) => 'c',
			chr(196).chr(142) => 'D', chr(196).chr(143) => 'd',
			chr(196).chr(144) => 'D', chr(196).chr(145) => 'd',
			chr(196).chr(146) => 'E', chr(196).chr(147) => 'e',
			chr(196).chr(148) => 'E', chr(196).chr(149) => 'e',
			chr(196).chr(150) => 'E', chr(196).chr(151) => 'e',
			chr(196).chr(152) => 'E', chr(196).chr(153) => 'e',
			chr(196).chr(154) => 'E', chr(196).chr(155) => 'e',
			chr(196).chr(156) => 'G', chr(196).chr(157) => 'g',
			chr(196).chr(158) => 'G', chr(196).chr(159) => 'g',
			chr(196).chr(160) => 'G', chr(196).chr(161) => 'g',
			chr(196).chr(162) => 'G', chr(196).chr(163) => 'g',
			chr(196).chr(164) => 'H', chr(196).chr(165) => 'h',
			chr(196).chr(166) => 'H', chr(196).chr(167) => 'h',
			chr(196).chr(168) => 'I', chr(196).chr(169) => 'i',
			chr(196).chr(170) => 'I', chr(196).chr(171) => 'i',
			chr(196).chr(172) => 'I', chr(196).chr(173) => 'i',
			chr(196).chr(174) => 'I', chr(196).chr(175) => 'i',
			chr(196).chr(176) => 'I', chr(196).chr(177) => 'i',
			chr(196).chr(178) => 'IJ',chr(196).chr(179) => 'ij',
			chr(196).chr(180) => 'J', chr(196).chr(181) => 'j',
			chr(196).chr(182) => 'K', chr(196).chr(183) => 'k',
			chr(196).chr(184) => 'k', chr(196).chr(185) => 'L',
			chr(196).chr(186) => 'l', chr(196).chr(187) => 'L',
			chr(196).chr(188) => 'l', chr(196).chr(189) => 'L',
			chr(196).chr(190) => 'l', chr(196).chr(191) => 'L',
			chr(197).chr(128) => 'l', chr(197).chr(129) => 'L',
			chr(197).chr(130) => 'l', chr(197).chr(131) => 'N',
			chr(197).chr(132) => 'n', chr(197).chr(133) => 'N',
			chr(197).chr(134) => 'n', chr(197).chr(135) => 'N',
			chr(197).chr(136) => 'n', chr(197).chr(137) => 'N',
			chr(197).chr(138) => 'n', chr(197).chr(139) => 'N',
			chr(197).chr(140) => 'O', chr(197).chr(141) => 'o',
			chr(197).chr(142) => 'O', chr(197).chr(143) => 'o',
			chr(197).chr(144) => 'O', chr(197).chr(145) => 'o',
			chr(197).chr(146) => 'OE',chr(197).chr(147) => 'oe',
			chr(197).chr(148) => 'R',chr(197).chr(149) => 'r',
			chr(197).chr(150) => 'R',chr(197).chr(151) => 'r',
			chr(197).chr(152) => 'R',chr(197).chr(153) => 'r',
			chr(197).chr(154) => 'S',chr(197).chr(155) => 's',
			chr(197).chr(156) => 'S',chr(197).chr(157) => 's',
			chr(197).chr(158) => 'S',chr(197).chr(159) => 's',
			chr(197).chr(160) => 'S', chr(197).chr(161) => 's',
			chr(197).chr(162) => 'T', chr(197).chr(163) => 't',
			chr(197).chr(164) => 'T', chr(197).chr(165) => 't',
			chr(197).chr(166) => 'T', chr(197).chr(167) => 't',
			chr(197).chr(168) => 'U', chr(197).chr(169) => 'u',
			chr(197).chr(170) => 'U', chr(197).chr(171) => 'u',
			chr(197).chr(172) => 'U', chr(197).chr(173) => 'u',
			chr(197).chr(174) => 'U', chr(197).chr(175) => 'u',
			chr(197).chr(176) => 'U', chr(197).chr(177) => 'u',
			chr(197).chr(178) => 'U', chr(197).chr(179) => 'u',
			chr(197).chr(180) => 'W', chr(197).chr(181) => 'w',
			chr(197).chr(182) => 'Y', chr(197).chr(183) => 'y',
			chr(197).chr(184) => 'Y', chr(197).chr(185) => 'Z',
			chr(197).chr(186) => 'z', chr(197).chr(187) => 'Z',
			chr(197).chr(188) => 'z', chr(197).chr(189) => 'Z',
			chr(197).chr(190) => 'z', chr(197).chr(191) => 's',
			// Decompositions for Latin Extended-B
			chr(200).chr(152) => 'S', chr(200).chr(153) => 's',
			chr(200).chr(154) => 'T', chr(200).chr(155) => 't',
			// Euro Sign
			chr(226).chr(130).chr(172) => 'E',
			// GBP (Pound) Sign
			chr(194).chr(163) => '',
			// Vowels with diacritic (Vietnamese)
			// unmarked
			chr(198).chr(160) => 'O', chr(198).chr(161) => 'o',
			chr(198).chr(175) => 'U', chr(198).chr(176) => 'u',
			// grave accent
			chr(225).chr(186).chr(166) => 'A', chr(225).chr(186).chr(167) => 'a',
			chr(225).chr(186).chr(176) => 'A', chr(225).chr(186).chr(177) => 'a',
			chr(225).chr(187).chr(128) => 'E', chr(225).chr(187).chr(129) => 'e',
			chr(225).chr(187).chr(146) => 'O', chr(225).chr(187).chr(147) => 'o',
			chr(225).chr(187).chr(156) => 'O', chr(225).chr(187).chr(157) => 'o',
			chr(225).chr(187).chr(170) => 'U', chr(225).chr(187).chr(171) => 'u',
			chr(225).chr(187).chr(178) => 'Y', chr(225).chr(187).chr(179) => 'y',
			// hook
			chr(225).chr(186).chr(162) => 'A', chr(225).chr(186).chr(163) => 'a',
			chr(225).chr(186).chr(168) => 'A', chr(225).chr(186).chr(169) => 'a',
			chr(225).chr(186).chr(178) => 'A', chr(225).chr(186).chr(179) => 'a',
			chr(225).chr(186).chr(186) => 'E', chr(225).chr(186).chr(187) => 'e',
			chr(225).chr(187).chr(130) => 'E', chr(225).chr(187).chr(131) => 'e',
			chr(225).chr(187).chr(136) => 'I', chr(225).chr(187).chr(137) => 'i',
			chr(225).chr(187).chr(142) => 'O', chr(225).chr(187).chr(143) => 'o',
			chr(225).chr(187).chr(148) => 'O', chr(225).chr(187).chr(149) => 'o',
			chr(225).chr(187).chr(158) => 'O', chr(225).chr(187).chr(159) => 'o',
			chr(225).chr(187).chr(166) => 'U', chr(225).chr(187).chr(167) => 'u',
			chr(225).chr(187).chr(172) => 'U', chr(225).chr(187).chr(173) => 'u',
			chr(225).chr(187).chr(182) => 'Y', chr(225).chr(187).chr(183) => 'y',
			// tilde
			chr(225).chr(186).chr(170) => 'A', chr(225).chr(186).chr(171) => 'a',
			chr(225).chr(186).chr(180) => 'A', chr(225).chr(186).chr(181) => 'a',
			chr(225).chr(186).chr(188) => 'E', chr(225).chr(186).chr(189) => 'e',
			chr(225).chr(187).chr(132) => 'E', chr(225).chr(187).chr(133) => 'e',
			chr(225).chr(187).chr(150) => 'O', chr(225).chr(187).chr(151) => 'o',
			chr(225).chr(187).chr(160) => 'O', chr(225).chr(187).chr(161) => 'o',
			chr(225).chr(187).chr(174) => 'U', chr(225).chr(187).chr(175) => 'u',
			chr(225).chr(187).chr(184) => 'Y', chr(225).chr(187).chr(185) => 'y',
			// acute accent
			chr(225).chr(186).chr(164) => 'A', chr(225).chr(186).chr(165) => 'a',
			chr(225).chr(186).chr(174) => 'A', chr(225).chr(186).chr(175) => 'a',
			chr(225).chr(186).chr(190) => 'E', chr(225).chr(186).chr(191) => 'e',
			chr(225).chr(187).chr(144) => 'O', chr(225).chr(187).chr(145) => 'o',
			chr(225).chr(187).chr(154) => 'O', chr(225).chr(187).chr(155) => 'o',
			chr(225).chr(187).chr(168) => 'U', chr(225).chr(187).chr(169) => 'u',
			// dot below
			chr(225).chr(186).chr(160) => 'A', chr(225).chr(186).chr(161) => 'a',
			chr(225).chr(186).chr(172) => 'A', chr(225).chr(186).chr(173) => 'a',
			chr(225).chr(186).chr(182) => 'A', chr(225).chr(186).chr(183) => 'a',
			chr(225).chr(186).chr(184) => 'E', chr(225).chr(186).chr(185) => 'e',
			chr(225).chr(187).chr(134) => 'E', chr(225).chr(187).chr(135) => 'e',
			chr(225).chr(187).chr(138) => 'I', chr(225).chr(187).chr(139) => 'i',
			chr(225).chr(187).chr(140) => 'O', chr(225).chr(187).chr(141) => 'o',
			chr(225).chr(187).chr(152) => 'O', chr(225).chr(187).chr(153) => 'o',
			chr(225).chr(187).chr(162) => 'O', chr(225).chr(187).chr(163) => 'o',
			chr(225).chr(187).chr(164) => 'U', chr(225).chr(187).chr(165) => 'u',
			chr(225).chr(187).chr(176) => 'U', chr(225).chr(187).chr(177) => 'u',
			chr(225).chr(187).chr(180) => 'Y', chr(225).chr(187).chr(181) => 'y',
			// Vowels with diacritic (Chinese, Hanyu Pinyin)
			chr(201).chr(145) => 'a',
			// macron
			chr(199).chr(149) => 'U', chr(199).chr(150) => 'u',
			// acute accent
			chr(199).chr(151) => 'U', chr(199).chr(152) => 'u',
			// caron
			chr(199).chr(141) => 'A', chr(199).chr(142) => 'a',
			chr(199).chr(143) => 'I', chr(199).chr(144) => 'i',
			chr(199).chr(145) => 'O', chr(199).chr(146) => 'o',
			chr(199).chr(147) => 'U', chr(199).chr(148) => 'u',
			chr(199).chr(153) => 'U', chr(199).chr(154) => 'u',
			// grave accent
			chr(199).chr(155) => 'U', chr(199).chr(156) => 'u',
			);
	
			// Used for locale-specific rules
			$locale = get_locale();
	
			if ( 'de_DE' == $locale ) {
				$chars[ chr(195).chr(132) ] = 'Ae';
				$chars[ chr(195).chr(164) ] = 'ae';
				$chars[ chr(195).chr(150) ] = 'Oe';
				$chars[ chr(195).chr(182) ] = 'oe';
				$chars[ chr(195).chr(156) ] = 'Ue';
				$chars[ chr(195).chr(188) ] = 'ue';
				$chars[ chr(195).chr(159) ] = 'ss';
			} elseif ( 'da_DK' === $locale ) {
				$chars[ chr(195).chr(134) ] = 'Ae';
	 			$chars[ chr(195).chr(166) ] = 'ae';
				$chars[ chr(195).chr(152) ] = 'Oe';
				$chars[ chr(195).chr(184) ] = 'oe';
				$chars[ chr(195).chr(133) ] = 'Aa';
				$chars[ chr(195).chr(165) ] = 'aa';
			}
	
			$string = strtr($string, $chars);
		} else {
			// Assume ISO-8859-1 if not UTF-8
			$chars['in'] = chr(128).chr(131).chr(138).chr(142).chr(154).chr(158)
				.chr(159).chr(162).chr(165).chr(181).chr(192).chr(193).chr(194)
				.chr(195).chr(196).chr(197).chr(199).chr(200).chr(201).chr(202)
				.chr(203).chr(204).chr(205).chr(206).chr(207).chr(209).chr(210)
				.chr(211).chr(212).chr(213).chr(214).chr(216).chr(217).chr(218)
				.chr(219).chr(220).chr(221).chr(224).chr(225).chr(226).chr(227)
				.chr(228).chr(229).chr(231).chr(232).chr(233).chr(234).chr(235)
				.chr(236).chr(237).chr(238).chr(239).chr(241).chr(242).chr(243)
				.chr(244).chr(245).chr(246).chr(248).chr(249).chr(250).chr(251)
				.chr(252).chr(253).chr(255);
	
			$chars['out'] = "EfSZszYcYuAAAAAACEEEEIIIINOOOOOOUUUUYaaaaaaceeeeiiiinoooooouuuuyy";
	
			$string = strtr($string, $chars['in'], $chars['out']);
			$double_chars['in'] = array(chr(140), chr(156), chr(198), chr(208), chr(222), chr(223), chr(230), chr(240), chr(254));
			$double_chars['out'] = array('OE', 'oe', 'AE', 'DH', 'TH', 'ss', 'ae', 'dh', 'th');
			$string = str_replace($double_chars['in'], $double_chars['out'], $string);
		}
	
		return $string;
	}

	
}