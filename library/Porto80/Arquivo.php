<?php
require_once 'thumb/ThumbLib.inc.php'; //IMPORTA CLASSE DE IMAGEM
class Porto80_Arquivo extends Zend_Controller_Plugin_Abstract {
	
	private $uploadDir = '';
	
	public function __construct() {
		
		$this->uploadDir = realpath(APPLICATION_PATH.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'upload').DIRECTORY_SEPARATOR;
		 
	}
	
    public function upload($arquivo='', $pasta='') {
    	
    	$retorno = array(
    		'mensagem'=>'',
    		'status'=>0
    	);
    	
        if ($arquivo!='' && $_SERVER['REQUEST_METHOD']==='POST') {
    	
	    	if ($pasta!='') {
	    		$pasta = str_replace('/',DIRECTORY_SEPARATOR,$pasta);
	    		$pasta = substr($pasta,-1)==DIRECTORY_SEPARATOR?$pasta:$pasta.DIRECTORY_SEPARATOR;
	    		
	    		$pasta = $this->uploadDir.$pasta;
	    		if (!is_dir($pasta)) mkdir($pasta);
	    	} else {
	    		$pasta = $this->uploadDir;
	    	}
	    	
    		if (isset($_FILES)) {
			    $tempFile = $_FILES[$arquivo]['tmp_name'];
			    $targetFile = $_FILES[$arquivo]['name'];
			} else {
			    $tempFile = $targetFile = '';
			}
			
        	try {
		        $fileName=''; $valid=true;
		        
		        if ($tempFile!='') {
		        	$fileExt = isset($_REQUEST['fileext'])?$_REQUEST['fileext']:'';
		        	
		        	if ($fileExt!='') { 
			            $fileTypes  = str_replace('*.','',$_REQUEST['fileext']);
			            $fileTypes  = str_replace(';','|',$fileTypes);
			            $typesArray = preg_split('/\|/',$fileTypes);
			            $fileParts  = pathinfo(strtolower($targetFile));
			            
			            if (!in_array($fileParts['extension'],$typesArray)) {
        	            	
			                $retorno['status']=0;
			                $retorno['mensagem'] = "Tipo de arquivo inv�lido";
			                $valid=false;
		             
	            		}
		        	}
		            
		            if ($valid) {
		                $fext  = strtolower(strrchr($targetFile, '.'));
		                $fname = Porto80_Core::url_amigavel(str_ireplace($fext,'',$targetFile),'_');
		                
		                //VERIFICANDO SE JA EXISTE UM ARQUIVO COM ESSE nome
		                if (is_file($pasta.$fname.$fext)) {
		                    //ADICIONANDO DIFERENCIAL PARA N�O COINCIDIR OS ARQUIVOS
		                    $fname .='_'.time();
		                }
		                
		                $fileName = $fname.$fext;
		                $destFile =  $pasta . $fileName;
		                
		                if ( !move_uploaded_file($tempFile,$destFile) ) {
		                    // error
		                    $retorno['status'] = 0;
		                    $retorno['mensagem'] = "N�o foi poss�vel enviar o arquivo.";
		                }
		
		                $retorno['status']=1;
		                $retorno['mensagem'] = $fileName;
	              	} 
		        } else {
		            $retorno['status']=0;
		            $retorno['mensagem'] = "Arquivo inv�lido";
		        }
				
			} catch (Exception $e) {
	            $retorno['status']=0;
	            $retorno['mensagem'] = $e->getMessage();
			}
        }
        
        return $retorno;
        
    }
	
    public function delete($arquivo='', $pasta='') {
    	
    	$retorno = array(
    		'status'=>0,
    		'mensagem'=>'',
    		'isfile'=>false,
    	);
    	
    	if ($arquivo!='') {
    		
    		if ($pasta!='') {
    			$pasta = str_replace('/',DIRECTORY_SEPARATOR,$pasta);
    			$pasta = substr($pasta,-1)==DIRECTORY_SEPARATOR?$pasta:$pasta.DIRECTORY_SEPARATOR;
    		}
    		
    		try {
	    		$fullPath = $this->uploadDir.$pasta;
	    		if (is_file($fullPath.$arquivo)) {
	    			if (unlink($fullPath.$arquivo)) {
	    				$retorno['status']=1;
	    			}
	    		}
    		} catch (Exception $e) {
    			$retorno['mensagem']=$e->getMessage();
    		}
    	
    	}
    	
		return $retorno;     	
    	
    }
    
    public function isFile($arquivo='',$pasta='') {
    	
    	$retorno = array(
    		'status'=>0,
    		'mensagem'=>'',
    	);
    	
    	if ($arquivo!='') {
    		
    		if ($pasta!='') {
    			$pasta = str_replace('/',DIRECTORY_SEPARATOR,$pasta);
    			$pasta = substr($pasta,-1)==DIRECTORY_SEPARATOR?$pasta:$pasta.DIRECTORY_SEPARATOR;
    		}
    		
    		try {
	    		$fullPath = $this->uploadDir.$pasta;
	    		if (is_file($fullPath.$arquivo)) {
	    			$retorno['mensagem']=$fullPath.$arquivo;
	    			$retorno['status']=1;
	    		}
    		} catch (Exception $e) {
    			$retorno['mensagem']=$e->getMessage();
    		}
    	
    	}
    	
    	return $retorno;    	
    	
    }
    
    public function sizeImg($arquivo='',$pasta='', $largura=0, $altura=0) {
    	
    	$retorno = array(
    		'status'=>0,
    		'mensagem'=>''
    	);
    	
    	if ($arquivo!='') {
    		
    		if ($pasta!='') {
    			$pasta = str_replace('/',DIRECTORY_SEPARATOR,$pasta);
    			$pasta = substr($pasta,-1)==DIRECTORY_SEPARATOR?$pasta:$pasta.DIRECTORY_SEPARATOR;
    		}
    		
    		try {
	    		$fullPath = $this->uploadDir.$pasta;
	    		$fullFile = $fullPath.$arquivo;
	    		if (is_file($fullFile)) {
	    			$thumb = PhpThumbFactory::create($fullFile);
	    			$dim = $thumb->getCurrentDimensions();
    				if ($dim['height']<=$dim['width']) { //ALTURA MAIOR QUE LARGURA
    					$thumb->resize($largura,0); 
					} else {
						$thumb->resize(0,$altura);
					}
					$thumb->save($fullFile);
	    		}
    		} catch (Exception $e) {
    			$retorno['mensagem']=$e->getMessage();
    		}
    	
    	}
    	
		return $retorno;     	
    	
    	
    }
    
    public function marcadagua($foto='', $pasta='') {
    	
    	$retorno = array(
    		'status'=>0,
    		'mensagem'=>''
    	);
    	
    	if ($foto!='') {
    		
    		$marca = $this->uploadDir.DIRECTORY_SEPARATOR.'marcadagua.png';
    		
        	if ($pasta!='') {
    			$pasta = str_replace('/',DIRECTORY_SEPARATOR,$pasta);
    			$pasta = substr($pasta,-1)==DIRECTORY_SEPARATOR?$pasta:$pasta.DIRECTORY_SEPARATOR;
    		}

    		try {
	    		$fullPath = $this->uploadDir.$pasta;
	    		$fullFile = $fullPath.$foto;
	    		if (is_file($fullFile)) {
	    			$thumb = PhpThumbFactory::create($fullFile);
	    			//$thumb->createWatermark($marca, 'rb', 10);
	    			$thumb->createWatermark($marca);
					$thumb->save($fullFile);
					unset($thumb);
	    		}
    		} catch (Exception $e) {
    			$retorno['mensagem']=$e->getMessage();
    		}
    	
    	}
    	
		return $retorno;     	
    	
    }
    
}