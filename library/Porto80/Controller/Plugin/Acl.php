<?php 
class Porto80_Controller_Plugin_Acl extends Zend_Controller_Plugin_Abstract {
	
	private $perfil = '';
	private $acl = '';
	private $currentUrl = '';
	
    //SETA AS PERMISS�ES
	public function preDispatch(Zend_Controller_Request_Abstract $request) {
		
		//RESGATANDO OBJETO VIEW
		$view = Zend_Controller_Action_HelperBroker::getExistingHelper('ViewRenderer')->view;

		//SETANDO MENU ATIVO
		$view->menuativo = $request->getControllerName();
		
		//CONECTA AO BANCO DE DADOS
		$dbAdapter = Zend_Db_Table::getDefaultAdapter ();
	
		//CARREGANDO PERMISS�ES DO BANCO DE DADOS
		$this->acl = new Zend_Acl();
		
		//ROLES
		$select = $dbAdapter->select()->from('role');
        $roles = $dbAdapter->fetchAll($select);
        foreach($roles as $role) {
        	//echo '<p>'.$role['role'].' '.$role['role_copy'].'</p>';
        	if ($role['role_copy']!='') {
        		if (strpos($role['role_copy'],';')!==false) {
        			$roleP = explode(';',$role['role_copy']);
        		} else {
        			$roleP[] = $role['role_copy'];
        		}
        		$this->acl->addRole( new Zend_Acl_Role($role['role']), $roleP );
        	} else {
        		//N�O EXISTE PAI
                $this->acl->addRole( new Zend_Acl_Role($role['role']) );
        	}
        }
        
		
        //RESOURCE
		$select = $dbAdapter->select()->from('resource');
        $resources = $dbAdapter->fetchAll($select);
        foreach($resources as $resource) {
        	$this->acl->addResource( new Zend_Acl_Resource($resource['resource']) );
        }
        
        //PRIVILEGE/PERMISSION
		$select = $dbAdapter->select()->from('permission');
        $permissions = $dbAdapter->fetchAll($select);
        foreach($permissions as $permission) {
        	if ($permission['permission']==1) {
        		$this->acl->allow( $permission['role'], $permission['resource'], $permission['action']);
        	} else {
        		$this->acl->deny( $permission['role'], $permission['resource'], $permission['action']);
        	}
        }		
        
		//VERIFICA PERFIL E USU�RIO
    	$session = Zend_Session::namespaceGet(MODULE_NAME);
    	$auth = isset($session['storage'])?$session['storage']:'';
    	
    	if ($auth=='') {
			$this->perfil = 'visitante';
		} else {
			$this->perfil = $auth->role;
			$view->auth=$auth;
		}
		$view->user_perfil=$this->perfil;
        
		Zend_View_Helper_Navigation_HelperAbstract::setDefaultAcl($this->acl);
		Zend_View_Helper_Navigation_HelperAbstract::setDefaultRole($this->perfil);
		
		//VERIFICA SE O USU�RIO TEM ACESSO A P�GINA QUE ELE ESTA TENTANDO ACESSAR 
		if (!self::isAllowed()) {
			$r = new Zend_Controller_Action_Helper_Redirector;
			$r->gotoUrl('/');
			$r->redirectAndExit();
		}

	}
	
	public static function getPerfil() {
		return $this->perfil;
	}
	
	public function dispatchLoopStartup(Zend_Controller_Request_Abstract $request) {
		if (!isset($_SESSION['prevUrl'])) $this->currentUrl = '/';
		else if ($request->getControllerName()!='login') {	
			$this->currentUrl = $request->getRequestUri();
		}
	}
	
	public function dispatchLoopShutdown() {
		$_SESSION['prevUrl'] = $this->currentUrl;
	}
	
	public function isAllowed($user_perfil='', $resource='', $action='') {
		
		if ($resource=='') $resource = $this->_request->getControllerName();
		if ($action=='') $action = $this->_request->getActionName();
		if ($action=='index' || $action=='') $action=null;
		if ($user_perfil=='') $user_perfil = $this->perfil;
		
		return $this->acl->isAllowed($user_perfil, $resource, $action);
		
	}
	
	
}