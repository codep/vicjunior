<?php
class Zend_View_Helper_Alerta extends Zend_View_Helper_Abstract {
	
	public function alerta($mensagem='',$tipo='') {
		
		if ($mensagem=='') {
			return '';	
		}
		
		switch($tipo) {
			case 'success': $tipolabel = strtoupper(' sucesso! '); break;
			case 'error': $tipolabel = strtoupper(' erro! '); break;
			case 'warning': $tipolabel = strtoupper(' aviso! '); break;
			case 'info': $tipolabel = strtoupper(' informação! '); break; 
			case 'tip': $tipolabel = strtoupper(' dica! '); break;
			default: $tipolabel = strtoupper(''); break;
		}
		$html = '<div class="notification '.$tipo.'"><span class="strong">'.$tipolabel.'</span>'.utf8_encode($mensagem).'</div>';
 
		return $html;
	}
	
}

