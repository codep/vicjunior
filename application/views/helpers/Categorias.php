<?php
class Zend_View_Helper_Categorias extends Zend_View_Helper_Abstract {
	
	private $arvore;
	
	public function categorias($arvore) {
		
		$this->status = 1;
		$this->arvore = $arvore; 
		return $this;
	}
	
	private function loopCat($categorias) {

		$html= '<ul>';
		foreach($categorias as $categoria) {
			
			if ($categoria['status']==0) $this->status=0;
			$html.= '<li>';
			$html.= '<a rel="'.$categoria['id_produto_categoria'].'" title="'.$categoria['descricao'].'<br /><img src=\''.$this->view->baseUrl('imagem/show/rc/180/g/'.$categoria['id_galeria'].'/i/'.$categoria['filename']).'\'  />">'.($this->status==1?$categoria['titulo']:'<strike>'.$categoria['titulo'].'</strike>').'</a>';
			
			if (is_array($categoria['subcategorias'])) {
				$html.= $this->loopCat($categoria['subcategorias']);
				$this->status=1;
			}
			
			$html.= '</li>';
		}
		$html.= '</ul>';
		
		return $html;
		
	}
	
	public function getCkHtml() {
		return $this->loopCat($this->arvore);
	}
	
}

