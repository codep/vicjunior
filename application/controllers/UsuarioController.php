<?php

class UsuarioController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
    	
    	$dbAdapter = Zend_Db_Table::getDefaultAdapter ();
    	
    	$id_usuario = $this->view->auth->id_usuario;
    	
    	$select = $dbAdapter->select()->from(array('U'=>'usuario'))->joinInner(array('C'=>'cliente'),'C.id_cliente=U.id_cliente')->where('U.id_usuario='.$id_usuario);
    	$dados = $dbAdapter->fetchRow($select);
    	$this->view->dados = $dados;
    	
        //PRECISO PARA ABRIR VISUALIZAR
    	$this->view->headScript()
			->appendFile( $this->view->baseUrl('/site_js/jquery.maskedinput-1.2.2.min.js'),'text/javascript')
			->appendFile( $this->view->baseUrl('/site_js/jquery.validate_pack.js'),'text/javascript')
		;
		
		$this->_helper->viewRenderer->render('formulario');		
    }
    
    public function loginAction()
    {
        $session = Zend_Session::namespaceGet(MODULE_NAME);
    	$auth = isset($session['storage'])?$session['storage']:'';
		if ($auth!='') {
			$this->_helper->redirector('index','index');
		}
    }
    
    public function validarAction()
    {
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		
		$retorno = array(
			'mensagem'=>'',
			'status'=>0
		);
		
		if ($this->_request->isPost ()) {
			
			//ZERAR SESS�O
			Zend_Session::namespaceUnset(MODULE_NAME);
			//TENTAR LOGAR NOVAMENTE
			try {
				Zend_Loader::loadClass ( 'Zend_Filter_StripTags' );
				$filter = new Zend_Filter_StripTags ();
				
				$username = $filter->filter ( $this->_request->getPost ( 'username' ) );
				$password = $filter->filter ( $this->_request->getPost ( 'password' ) );
				
				if (! empty ( $username ) && ! empty ( $password )) {
					
					$dbAdapter = Zend_Db_Table::getDefaultAdapter ();
										
					Zend_Loader::loadClass ( 'Zend_Auth_Adapter_DbTable' );
					$authAdapter = new Zend_Auth_Adapter_DbTable ( $dbAdapter );
					$select = $authAdapter->setTableName ( 'usuario' )->setIdentityColumn ( 'usuario' )->setCredentialColumn( 'senha' )->setCredentialTreatment("SHA1(?)") ->setIdentity( $username )->setCredential ( $password )->getDbSelect()->where('status=1');
					//Configura o Zend_Auth
					$auth = Zend_Auth::getInstance();
					
					try {
						$auth->setStorage(new Zend_Auth_Storage_Session(MODULE_NAME));
						$result = $auth->authenticate($authAdapter);
					} catch (Zend_Exception $e) {
						$retorno['mensagem']=$e->getMessage();
					}

					$dbAdapter->closeConnection ();
					
					switch ($result->getCode()) {
						case Zend_Auth_Result::SUCCESS :
							$data = $authAdapter->getResultRowObject( null, "senha" );
							$auth->getStorage()->write( $data );
							$retorno['status']=1;
							$retorno['mensagem']='Autenticado com sucesso!';
							$retorno['redirect']=$this->view->baseUrl('');
							break;
						default:
						case Zend_Auth_Result::FAILURE_IDENTITY_NOT_FOUND :
						case Zend_Auth_Result::FAILURE_CREDENTIAL_INVALID :
							$retorno['mensagem']='Usu�rio ou Senha inv�lidos';
							break;
					}
				
				} else {
					$retorno['mensagem']='Preencha corretamente todos os campos!';
				}
			} catch ( Zend_Db_Adapter_Exception $e ) {
				$retorno['mensagem']=$e->getMessage();
			} catch ( Zend_Exception $e ) {
				$retorno['mensagem']=$e->getMessage();
			}
		} else {
			$retorno['mensagem'] = 'Envio de dados de forma inv�lida';
		}
		
		$retorno['mensagem'] = rawurlencode($retorno['mensagem']);
		echo json_encode($retorno);
    }    
    
	public function logoutAction() {
		//REMOVE REGISTRO / SESSAO E ENVIA PARA TELA DE LOGIN
		Zend_Registry::_unsetInstance();
		Zend_Session::namespaceUnset(MODULE_NAME);
		$this->_helper->redirector('index','index');
	}
    
    public function novoAction()
    {
        //PRECISO PARA ABRIR VISUALIZAR
    	$this->view->headScript()
			->appendFile( $this->view->baseUrl('/site_js/jquery.maskedinput-1.2.2.min.js'),'text/javascript')
			->appendFile( $this->view->baseUrl('/site_js/jquery.validate_pack.js'),'text/javascript')
		;
		
		$this->_helper->viewRenderer->render('formulario');		
    }
    
    public function salvarAction()
    {
		if ($this->_request->isPost()) {
		
			$dbAdapter = Zend_Db_Table::getDefaultAdapter ();
			
			$id_usuario = $this->_request->getPost('id_usuario', 0);
			$id_cliente = $this->_request->getPost('id_cliente', 0);
			
			//INSERINDO CLIENTE
			$dadosCliente = array(
				'nome'=>$this->_request->getPost('nome'),
				'razao_social'=>$this->_request->getPost('razao_social'),
				'cnpj'=>$this->_request->getPost('cnpj'),
				'ie'=>$this->_request->getPost('ie'),
				'endereco'=>$this->_request->getPost('endereco'),
				'bairro'=>$this->_request->getPost('bairro'),
				'cidade'=>$this->_request->getPost('cidade'),
				'estado'=>$this->_request->getPost('estado'),
				'cep'=> Porto80_Core::converteCep($this->_request->getPost('cep')),
				'telefone_com'=>Porto80_Core::converteTelefone($this->_request->getPost('telefone_com')),
				'telefone_fax'=>Porto80_Core::converteTelefone($this->_request->getPost('telefone_fax')),
				'responsavel'=>$this->_request->getPost('responsavel'),
				'resp_cel'=>Porto80_Core::converteTelefone($this->_request->getPost('resp_cel')),
				'email'=>$this->_request->getPost('email'),
				'emailpublico'=>$this->_request->getPost('emailpublico'),
				'exibirpublico'=>$this->_request->getPost('exibirpublico'),
				'observacao'=>'',
				'foto'=>''
			);
			//INSERINDO USU�RIO
			$dadosUsuario = array(
				'usuario'=>$this->_request->getPost('usuario'),
				'senha'=>sha1($this->_request->getPost('senha')),
				'role'=>'cliente'
			);
			
			if ($id_usuario==0 && $id_cliente==0) {
			
				//VERIFICA DUPLICA��O DO USU�RIO
				$select = $dbAdapter->select()->from('usuario', 'id_usuario')->where('usuario="'.$this->_request->getPost('usuario').'"');
				$resultado = $dbAdapter->fetchRow($select);
				if (isset($resultado['id_usuario'])) {
					
					$this->view->mensagem='Usu�rio duplicado!';
					$this->view->mensagem2='Esse usu�rio j� existe, informe outro usu�rio.';					
					
				} else {
					
					$dadosUsuario= array_merge($dadosUsuario,array(
						'local_padrao'=>'SP',
						'status'=>0
					));
					
					$dbAdapter->insert('cliente',$dadosCliente);
					$lastID = $dbAdapter->lastInsertId('cliente','id_cliente');
					$dadosUsuario['id_cliente']=$lastID;
					$dbAdapter->insert('usuario',$dadosUsuario);
					$this->view->mensagem='Seu cadastro foi enviado com sucesso!';
					$this->view->mensagem2='Seu cadastro ser� an�lisado e caso liberado voc� poder� come�ar a efetuar os pedidos.';
				}
			
			} else {
				
					unset($dadosUsuario['senha']);
					$novasenha = $this->_request->getPost('novasenha','');
					if ($novasenha!='') $dadosUsuario['senha'] = sha1($novasenha);
				
					$dbAdapter->update('cliente',$dadosCliente,'id_cliente='.$id_cliente);
					$dbAdapter->update('usuario',$dadosUsuario,'id_usuario='.$id_usuario);
					$this->view->mensagem='Seu cadastro foi atualizado com sucesso!';
					$this->view->mensagem2='';
					
			}
		}
		
    }    

}

