<?php

class AdminContatoController extends Zend_Controller_Action
{

    public function init()
    {

    }

    public function indexAction()
    {
		$this->_helper->redirector('pesquisar','admin-contato');
    }
    
    public function pesquisarAction() {
    	
        $pagina = $this->_request->getParam('pagina', 1) ;
        
        try {
        	
			$dbAdapter = Zend_Db_Table::getDefaultAdapter ();
	        
			/* FILTRO */
	        $key = $this->getRequest()->getPost('key');
	        $this->view->key = $key;
	        
	        $select = $dbAdapter->select()->from(array('C'=>'contato'))->order('C.status ASC')->order('C.data_envio DESC');
	        
	        if ($key!='') {
	        	$select->where('C.nome LIKE "%'.$key.'%" OR C.empresa LIKE "%'.$key.'%" OR C.email LIKE "%'.$key.'%" OR C.telefone LIKE "%'.$key.'%"');
	        }
	        
	        $result = $dbAdapter->fetchAll($select);
	    	
		    /* PAGINA��O */
		    $dados = Zend_Paginator::factory($result);
		    $dados->setCurrentPageNumber( intval($pagina) );
		    $dados->setItemCountPerPage(20);
	        $this->view->dados = $dados;
        	
        	
        } catch (Zend_Db_Exception $e) {
        	$this->_helper->FlashMessenger( array('warning' => htmlentities($e->getMessage()) ) );
        	$this->_helper->redirector('index','admin-resumo');
        }
    	
    }
    
    public function visualizarAction() {
    	
		$id = $this->_request->getParam('id');
		
    	if (!$id) {
    		$this->_helper->FlashMessenger( array('warning' => htmlentities('O registro informado n�o existe. Verifique e tente novamente.') ) );
    		$this->_helper->redirector('pesquisar','admin-contato');
    	}
		
    	$this->view->headScript()
    		->appendFile( $this->view->baseUrl('/admin_js/jquery.validate_pack.js'),'text/javascript')
			->appendFile( $this->view->baseUrl('/admin_js/tiny_mce/jquery.tinymce.js'),'text/javascript')
		;
		
    	//CARREGAR BANCO DE DADOS		
		try {
			
			$dbAdapter = Zend_Db_Table::getDefaultAdapter ();
			
			/* MENSAGEM */
			$select = $dbAdapter->select()->from(array('C'=>'contato'))->where('C.id_contato='.$id)->limit(1);			
			$dados = $dbAdapter->fetchRow($select);
			$this->view->dados = $dados;
			
			/* ALTERANDO STATUS PARA LIDO */
			$update = $dbAdapter->update('contato',array('status'=>1),'id_contato='.$id);
				
		} catch (Zend_Db_Exception $e) {
			
			$this->_helper->FlashMessenger( array('warning' => htmlentities( $e->getMessage()) ) );
			$this->_helper->redirector('pesquisar','admin-contato');
		}
    	
    	$this->_helper->viewRenderer->render('formulario');
    }
    
    public function responderAction() {
    	
        $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		
    	try {
    		
    		$dbAdapter = Zend_Db_Table::getDefaultAdapter ();
    		
        	$id = $this->_request->getPost('id_contato');
        	$resposta = $this->_request->getPost('resposta');
    		
	    	if (!$id) {
	    		$this->_helper->FlashMessenger( array('warning' => htmlentities('Aten��o! ID incorreto, tente novamente.') ) );
	    	} else if ($resposta=='') {
	    		$this->_helper->FlashMessenger( array('warning' => htmlentities('Aten��o! Voc� precisa escrever uma mensagem de resposta.') ) );
	    	} else {
	    		
	        	$select = $dbAdapter->select()->from(array('P'=>'parametros'),array('param_email_contato'))->where('status=1')->limit(1);
	    		$email_contato = $dbAdapter->fetchOne($select);
    			
        		//RECUPERANDO REGISTRO
        		$select = $dbAdapter->select()->from('contato')->where('id_contato='.$id);
        		$contato = $dbAdapter->fetchRow($select);
        		
				$corpo = '<br /><br /><hr /><br /><br /><b>Mensagem enviada:</b><br/>'.str_replace(chr(13).chr(10),"<br />",rawurldecode($contato['mensagem']));
				$resposta .= $corpo; 
        		
        		//ENVIANDO E-MAIL
				$mail = new Zend_Mail();
				$mail->setFrom(null,'VicJunior');
				$mail->setSubject('[RESPOSTA] vicjunior.com.br');
				$mail->addTo($contato['email'], $contato['nome']);
				$mail->addCc($email_contato,'vicjunior.com.br');
				$mail->setBodyHtml($resposta);
				$mail->send();
        		
        		//ATUALIZANDO STATUS DA MENSAGEM
        		$dbAdapter->update('contato',array('status'=>2),'id_contato='.$id);
	    		$this->_helper->FlashMessenger( array('success' => htmlentities('Resposta enviada com sucesso!') ) );
	    	}
    			
    	} catch (Zend_Db_Exception $e) {
    		$this->_helper->FlashMessenger( array('warning' => htmlentities($e->getMessage()) ) );
    	}
		
    	$this->_helper->redirector('pesquisar','admin-contato');
    	
    }
    
    public function excluirAction() {
    	
        $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
    	
    	try {
    		
    		$dbAdapter = Zend_Db_Table::getDefaultAdapter ();
    		
        	$id = $this->_request->getParam('id');
    		
	    	if (!$id) {
	    		$this->_helper->FlashMessenger( array('warning' => htmlentities('Aten��o! ID incorreto, tente novamente.') ) );
	    	} else {	    		
    			//REMOVENDO REGISTROS
	    		$delete = $dbAdapter->delete('contato','id_contato = '.$id);
	    		//MENSAGEM DE SUCESSO
	    		$this->_helper->FlashMessenger( array('success' => htmlentities('Registro exclu�do!') ) );
	    	}
    			
    	} catch (Zend_Db_Exception $e) {
    		$this->_helper->FlashMessenger( array('warning' => htmlentities( $e->getMessage()) ) );
    	}
		
    	$this->_helper->redirector('pesquisar','admin-contato');
    	
    }


}

