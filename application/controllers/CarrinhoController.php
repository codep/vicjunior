<?php

class CarrinhoController extends Zend_Controller_Action
{

    public function init()
    {}

    public function indexAction()
    {
    	
    	if (!isset($this->view->auth)) {
			$this->_helper->FlashMessenger( array('warning' => htmlentities('Voc� precisa estar autenticado para vizualizar seu carrinho')) );
			$this->_helper->redirector('index','index');
    	}
		
    	
		//CARREGAR BANCO DE DADOS		
		try {
			
			$dbAdapter = Zend_Db_Table::getDefaultAdapter ();
			$registro = Zend_Registry::getInstance();
			
	        //VERIFICA SE POSSUI CLIENTE LOGADO
	        $id_usuario = isset($_COOKIE['adm_id_usuario']) && $_COOKIE['adm_id_usuario'] > 0 ? $_COOKIE['adm_id_usuario'] : (isset($this->view->auth->id_usuario) ? $this->view->auth->id_usuario : 0);
						
			//---REVENDAS
			$revenda = $this->_request->getParam('revenda', '');
	        //SELECIONAR AS REVENDAS
	        $select = $dbAdapter->select()->from(array('PR' => 'produto_revenda'))->joinInner(array('PL' => 'produto_linha'), 'PR.id_produto_revenda=PL.id_produto_revenda', array())->joinInner(array('PC' => 'produto_categoria'), 'PL.id_produto_linha=PC.id_produto_linha', array())->joinInner(array('P' => 'produto'), 'PC.id_produto_categoria=P.id_produto_categoria', array())->group('PL.id_produto_revenda')->order('PR.ordem ASC');
	        if (!empty($id_usuario)) {
	            $select->joinInner(array('CR' => 'cliente_revenda'), 'PL.id_produto_revenda=CR.id_produto_revenda', array())->joinInner(array('U' => 'usuario'), 'U.id_cliente=CR.id_cliente', array())->where('U.id_usuario="' . $id_usuario . '"');
	        }
	        $this->view->revendas = $dbAdapter->fetchAll($select);
			if (empty($revenda)) {
				$rev1 = reset($this->view->revendas);
				$this->getResponse()->setRedirect($this->view->baseUrl().'/carrinho/r/'.$rev1['permlink']);	
			} else {
	        	$select_rev = $dbAdapter->select()->from('produto_revenda')->where('permlink="'.$revenda.'"');
	        	$revenda = $dbAdapter->fetchRow($select_rev);
				$this->view->revenda = $revenda;
			}
			//FIX PARA NÃO TER ERRO
			if (!isset($revenda['id_produto_revenda'])) {
				$revenda=array('id_produto_revenda'=>0,'permlink'=>'');
			}
        	
        	//VERIFICANDO LOCAL DO CLIENTE SELECIONADO
        	$select_usuario = $dbAdapter->select()->from('usuario',array('id_cliente','local_padrao'))->where('id_usuario='.$id_usuario);
        	$rs_usuario= $dbAdapter->fetchRow($select_usuario);
        	if (isset($rs_usuario) && count($rs_usuario)>0 && !empty($rs_usuario)) {
	        	$id_cliente = $rs_usuario['id_cliente'];
	        	$local_padrao = $rs_usuario['local_padrao'];
	        	if (!empty($id_cliente) && $id_cliente>0) {
	        		//SELECIONAR INFORMA��O DO CLIENTE
	        		$select_cliente = $dbAdapter->select()->from('cliente')->where('id_cliente='.$id_cliente);
	        		$rs_cliente = $dbAdapter->fetchRow($select_cliente);
	        		$cliente_obs=$rs_cliente['observacao'];
	        	}  else {
	        		$cliente_obs='';
	        	}
	        	$this->view->cliente_obs=$cliente_obs;
			} else {
				$id_cliente = 0;
				$local_padrao = '';
			}

			$desconto = isset($_COOKIE['carrinho_'.$revenda['permlink'].'_mp']) ? $_COOKIE['carrinho_'.$revenda['permlink'].'_mp'] : 'PRAZO';
        	//$desconto = strtoupper($this->_request->getParam('mp','PRAZO'));
        	$this->view->mp=$desconto;
			//echo $id_usuario;
			
			//PRODUTOS CARRINHO
        	$select = $dbAdapter->select()->from(array('C'=>'carrinho'))
        	->joinInner(array('P'=>'produto'),'C.id_produto=P.id_produto')
        	->joinInner(array('PC'=>'produto_categoria'),'PC.id_produto_categoria=P.id_produto_categoria',array())
        	//->joinInner(array('PE'=>'produto_extra'),'P.id_produto=PE.id_produto AND PE.local="'.$local_padrao.'"',array('local','frete','margem','desconto','preco','iva','ipi','icms_origem','icms_destino'))
        	->joinInner(array('PE'=>'produto_extra'),'P.id_produto=PE.id_produto',array('local','frete','margem','desconto','preco','iva','ipi','icms_origem','icms_destino'))->group('PE.id_produto')
        	->joinLeft(array('E'=>'produto_embalagem'),'E.id_produto_embalagem=P.id_produto_embalagem',array('embalagem_titulo'=>'E.titulo'))
        	->where('C.id_usuario='.$id_usuario)->where('C.id_produto_revenda='.$revenda['id_produto_revenda']);
	    	$produtos = $dbAdapter->fetchAll($select);
	    	
			$total_item=$total_valor_liquido=$total_valor_final=0;
	    	$bloco_produtos='';
	    	if (is_array($produtos) && count($produtos) > 0 ) {
		    	//ARRUMANDO PRODUTOS PARA AGRUPAR
		    	foreach($produtos as $produto) {
		    		//USANDO LOCAL DO USUÁRIO
		    		$produto_valor = Porto80_Core::getProdutoValor($produto['id_produto'], $local_padrao, $id_cliente,$desconto);
					if (!isset($produto_valor['liquido']) || empty($produto_valor['liquido'])) {
						//CASO NÃO TENHA, USAR O LOCAL DO PRODUTO
		    			$produto_valor = Porto80_Core::getProdutoValor($produto['id_produto'], $produto['local'], $id_cliente,$desconto);
					}
		    		$produto['vlrliquido']=Porto80_Core::converteFloat($produto_valor['liquido'],2);
		    		$produto['vlrfinal']=Porto80_Core::converteFloat($produto_valor['final'],2);
		    		$produto['vlrmargem']=Porto80_Core::converteFloat($produto_valor['margem'],2);
		    		$bloco_produtos[$produto['grupo']][$produto['titulo']][]=$produto;
					
		    	}
	    	} else {
	    		$bloco_produtos=array();
	    	}
	    	$this->view->produtos=$bloco_produtos;
	    	
	    	/* TOTAIS 
    		$select = $dbAdapter->select()->from(array('C'=>'carrinho'))->where('C.id_usuario='.$id_usuario)->where('C.id_produto_revenda='.$revenda['id_produto_revenda']);
    		$itens = $dbAdapter->fetchAll($select);
    		$total_item=$total_valor_liquido=$total_valor_final=0;
    		
    		foreach($itens as $item) {

    			//QUANTIDADE DE PRODUTOS
    			$total_item =  $total_item + $item['quantidade'];
    			
    			//BUSCANDO VALOR DO PRODUTO 
				$produto_valor = Porto80_Core::getProdutoValor($item['id_produto'], $local_padrao, $id_cliente);
    			$total_valor_liquido = $total_valor_liquido + ($item['quantidade'] * $produto_valor['liquido']);
    			$total_valor_final = $total_valor_final + ($item['quantidade'] * $produto_valor['final']);
				$id_produtos[]=$item['id_produto'];
    			
    		}
			$this->view->total_item = $total_item;
			*/
    		
    		
				
		} catch (Zend_Db_Exception $e) {
			
			$this->_helper->FlashMessenger( array('warning' => htmlentities( $e->getMessage()) ) );
			$this->_helper->redirector('index','index');
			
		}
    	
    }
    
    public function fecharPedidoAction() {
    	
    	if (!isset($this->view->auth)) {
    		
			$this->_helper->FlashMessenger( array('warning' => htmlentities('Usuario nao autenticado')) );
			$this->_helper->redirector('index','carrinho');
			
    	} else {
    	
			//CARREGAR BANCO DE DADOS		
			try {
				
				$dbAdapter = Zend_Db_Table::getDefaultAdapter ();
				
				$id_produto_revenda = $this->_request->getPost('id_produto_revenda',0);
				$id_usuario = isset($_COOKIE['adm_id_usuario'])?$_COOKIE['adm_id_usuario']:$this->view->auth->id_usuario;
				
				//PEGA ITENS ATUAIS DO CARRINHO
				$select = $dbAdapter->select()->from(array('C'=>'carrinho'))->where('C.id_usuario='.$id_usuario)->where('C.id_produto_revenda='.$id_produto_revenda);
				$carrinho = $dbAdapter->fetchAll($select);
				
				//PEGA OS DADOS DA REVENDA
	        	$select_rev = $dbAdapter->select()->from('produto_revenda')->where('id_produto_revenda="'.$id_produto_revenda.'"');
	        	$revenda = $dbAdapter->fetchRow($select_rev);
				
				if (count($carrinho)>0) {
					
					$tipo_pag = strtoupper($this->_request->getPost('vistaprazo','PRAZO')); //METODO DE PAGAMENTO
					$obs_cli = $this->_request->getPost('obs_cliente',''); //OBSERVA��ES DO CLIENTE
					
	    			$select_cliente = $dbAdapter->select()->from('usuario',array('id_cliente','local_padrao'))->where('id_usuario='.$id_usuario);
	    			$rs_cliente = $dbAdapter->fetchRow($select_cliente);
	    			$id_cliente = $rs_cliente['id_cliente'];
	    			$local_padrao = $rs_cliente['local_padrao'];
					
					//forçar verificação para a revenda selecionada, caso esse produto não tenha esse local para essa revenda, então pegar a primeira disponível
					$produto_valor_tmp = Porto80_Core::getProdutoValor($carrinho[0]['id_produto'], 'SC', $id_cliente,$tipo_pag);
					if (empty($produto_valor_tmp['final'])) {
						//NÃO EXISTE VALORES PARA ESSE LOCAL_PADRÃO, ENTÃO PEGAR O PRIMEIRO LOCAL PADRÃO PARA ESSE PRODUTO E USAR COMO NOVO LOCAL PADRÃO
	                	$select = $dbAdapter->select()->from(array('P' => 'produto'))->joinInner(array('PE' => 'produto_extra'), 'P.id_produto=PE.id_produto', array('local'))
	                	->where('P.id_produto='.$carrinho[0]['id_produto'])->limit(1);
						$rs_local = $dbAdapter->fetchRow($select);
						if (!empty($rs_local['local'])) {
							$local_padrao = $rs_local['local'];
						} else {
							throw new Exception("Erro ao selecionar local de destino do pedido.", 1);
						}
						/*
						echo '<pre>';
						var_dump($produto_valor_tmp);
						echo '</pre>';
						echo $local_padrao; die();
						*/
					}
										
					//CRIAR CAMPO NA TABELA PEDIDO
		    		$insert = $dbAdapter->insert('pedido',array(
		    			'id_usuario'=>$id_usuario,
						'id_produto_revenda'=>$id_produto_revenda,
		    			'codigo_pedido'=>'',
			    		'cond_pag'=>'',
			    		'tipo_pag'=>$tipo_pag,
		    			'observacoes_cliente'=>$obs_cli,
	    				'local'=>$local_padrao,
			    		'datahora'=>date('Y-m-d H:i:s'),
			    		'status'=>1
		    		));
		    		$lastPedidoID = $dbAdapter->lastInsertId('pedido','id_pedido');
					
					//ADICIONAR OS ITENS AO PEDIDO CRIADO
					$total_peso=0;
					$total_valor_liquido=0;
					$total_valor_final=0;
					$total_frete=0;
					$total_baseicms=0;
					$total_icms_origem=0;
					$total_icms_destino=0;
					$total_basesubtrib=0;
					$total_subtrib=0;
					$total_ipi=0;
					$total_desconto=0;
					foreach($carrinho as $item) {						
			    		$insert= $dbAdapter->insert('pedido_produto',array(
			    			'id_pedido'=>$lastPedidoID,
			    			'id_produto'=>$item['id_produto'],
				    		'quantidade'=>$item['quantidade'],
				    		'valor_liquido'=>$item['valor_liquido'],
				    		'valor_unitario'=>$item['valor_unitario']
			    		));
			    		
			    		//BUSCANDO INFORMA��ES SOBRE O PRODUTO
			    		$sqlprod = $dbAdapter->select()->from(array('P'=>'produto'))->where('P.id_produto='.$item['id_produto']);
			    		$rsproduto = $dbAdapter->fetchRow($sqlprod);
			    		$total_peso=$total_peso+($rsproduto['peso']*$item['quantidade']);
			    		
			    		//TRIBUTOS
						$produto_valor = Porto80_Core::getProdutoValor($item['id_produto'], $local_padrao, $id_cliente,$tipo_pag);
		    			$total_valor_liquido = $total_valor_liquido + floatval($item['quantidade']*$produto_valor['liquido']);
		    			$total_valor_final = $total_valor_final + floatval($item['quantidade']*$produto_valor['final']);
		    			$total_frete = $total_frete + floatval($item['quantidade']*$produto_valor['frete']);
		    			$total_baseicms = $total_baseicms + floatval($item['quantidade']*$produto_valor['baseicms']);
		    			$total_icms_origem = $total_icms_origem + floatval($item['quantidade']*$produto_valor['icms_origem']);
		    			$total_icms_destino = $total_icms_destino + floatval($item['quantidade']*$produto_valor['icms_destino']);
		    			$total_basesubtrib = $total_basesubtrib + floatval($item['quantidade']*$produto_valor['basesubtrib']);
		    			$total_subtrib = $total_subtrib + floatval($item['quantidade']*$produto_valor['subtrib']);
		    			$total_ipi = $total_ipi + floatval($item['quantidade']*$produto_valor['ipi']);
		    			$total_desconto = $total_desconto + floatval($item['quantidade']*$produto_valor['desconto']);
					}

					if (floatval($total_valor_liquido) < floatval($revenda['pedido_valor_minimo'])) {
						$this->_helper->FlashMessenger( array('warning' => htmlentities('Essa revenda possui um limite minimo de pedido de R$ '.Porto80_Core::converteFloat($revenda['pedido_valor_minimo'],2).'! Nao e possivel fechar um pedido abaixo desse valor')) );
						$this->_helper->redirector('index','carrinho',null,array('revenda'=>$revenda['permlink']));
					} else {
					
						//ATUALIZAR INFORMACOES SOBRE O PRODUTO
						$update = $dbAdapter->update('pedido',array(
							'peso'=>$total_peso,
							'frete'=>$total_frete,
							'baseicms'=>$total_baseicms,
							'icms_origem'=>$total_icms_origem,
							'icms_destino'=>$total_icms_destino,
							'basesubtrib'=>$total_basesubtrib,
							'subtrib'=>$total_subtrib,
							'ipi'=>$total_ipi,
							'total_desc'=>$total_desconto,
							'total_liquido'=>$total_valor_liquido,
							'total_final'=>$total_valor_final,
						),'id_pedido='.$lastPedidoID);
						
						//ZERAR CARRINHO
						$dbAdapter->delete('carrinho','id_usuario='.$id_usuario.' AND id_produto_revenda='.$id_produto_revenda);
					
					}
					
				} else {
					$this->_helper->FlashMessenger( array('warning' => htmlentities('Carrinho vazio, selecione alguns itens.')) );
					$this->_helper->redirector('index','carrinho',null,array('r'=>$revenda['permlink']));
				}
					
			} catch (Zend_Db_Exception $e) {
				
				$this->_helper->FlashMessenger( array('warning' => htmlentities( $e->getMessage()) ) );
				$this->_helper->redirector('index','carrinho',null,array('r'=>$revenda['permlink']));
				
			}
			
    	}
    	
    }
    
    public function statusAction() {
    	
        $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
    	
    	$id_usuario = isset($_COOKIE['adm_id_usuario'])&&$_COOKIE['adm_id_usuario']>0?$_COOKIE['adm_id_usuario']:$this->view->auth->id_usuario;
    	$desconto = $this->_getParam('desconto','');
    	
    	$status=0; $mensagem=''; $id_produtos=''; $total_peso=$total_item=$total_valor_liquido=$total_valor_final=0;
    	
    	if (empty($id_usuario)) {
    		$mensagem='Todos os campos devem ser informados.';
    	} else {
    	
	    	try {
	    		
	    		$dbAdapter = Zend_Db_Table::getDefaultAdapter ();
				
				//---REVENDAS
				$revenda = $this->_getParam('revenda', '');
				if (empty($revenda)) {
					$revenda=array('id_produto_revenda'=>0,'permlink'=>'');
				} else {
		        	$select_rev = $dbAdapter->select()->from('produto_revenda')->where('permlink="'.$revenda.'"');
		        	$revenda = $dbAdapter->fetchRow($select_rev);
				}
				
	    		//CLIENTE
	    		$select_cliente = $dbAdapter->select()->from('usuario',array('id_cliente','local_padrao'))->where('id_usuario='.$id_usuario);
	    		$rs_cliente = $dbAdapter->fetchRow($select_cliente);
	    		$id_cliente = $rs_cliente['id_cliente'];
	    		$local_padrao = $rs_cliente['local_padrao'];
	    		
	    		$select = $dbAdapter->select()->from(array('C'=>'carrinho'))
	    		->joinInner(array('P'=>'produto'), 'P.id_produto=C.id_produto', array('peso'))
				->joinInner(array('PE'=>'produto_extra'),'P.id_produto=PE.id_produto',array('local'))->group('PE.id_produto')
	    		->where('C.id_usuario='.$id_usuario);
				if (isset($revenda['id_produto_revenda']) && !empty($revenda['id_produto_revenda'])) {
					$select->where('C.id_produto_revenda='.$revenda['id_produto_revenda']);
				}
	    		$itens = $dbAdapter->fetchAll($select);
	    		
	    		foreach($itens as $item) {
	    			
	    			/* PESO */
	    			$total_peso = $total_peso + ($item['peso']*$item['quantidade']);

	    			/* QUANTIDADE DE PRODUTOS */
	    			$total_item =  $total_item + $item['quantidade'];
	    			
	    			/* BUSCANDO VALOR DO PRODUTO */
		    		$produto_valor = Porto80_Core::getProdutoValor($item['id_produto'], $local_padrao, $id_cliente, $desconto);
					if (!isset($produto_valor['liquido']) || empty($produto_valor['liquido'])) {
						//CASO NÃO TENHA, USAR O LOCAL DO PRODUTO
		    			$produto_valor = Porto80_Core::getProdutoValor($item['id_produto'], $item['local'], $id_cliente, $desconto);
					}
	    			$total_valor_liquido = $total_valor_liquido + ($item['quantidade']*$produto_valor['liquido']);
	    			$total_valor_final = $total_valor_final + ($item['quantidade']*$produto_valor['final']);
	    			
					$id_produtos[]=$item['id_produto'];
	    			
	    		}
	    		
	    		if (is_array($id_produtos)) $id_produtos = implode(',',array_unique($id_produtos));
	    		$status=1;
	    		
	    	} catch (Zend_Db_Exception $e) {
				$mensagem = $e->getMessage();
				$this->_helper->FlashMessenger( array('warning' => htmlentities($mensagem) ) );
			}

    	}
    	
    	$retorno = array(
    		'status'=>$status,
    		'mensagem'=>rawurlencode($mensagem),
    		'total_peso'=>number_format(round($total_peso,3),3,',','.'),
    		'total_item'=>$total_item,
    		'total_valor_liquido'=>Porto80_Core::converteFloat($total_valor_liquido,2),
    		'float_valor_liquido'=>$total_valor_liquido,
    		'total_valor_final'=>Porto80_Core::converteFloat($total_valor_final,2),
    		'float_valor_final'=>$total_valor_final,
    		'id_produtos'=>$id_produtos
    	);
    	
    	if ($this->_request->isXmlHttpRequest()) {
    		echo Zend_Json::encode($retorno);
    	} else {
    		//var_dump($retorno);
    	}
    	
    	
    }
    
    /* FUN��O PARA ADICIONAR PRODUTO NO CARRINHO */
    public function addProdutoAction() {
    	
        $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
    	
    	$id_produto = $this->_request->getParam('id');
    	$quantidade = $this->_request->getParam('q');
    	$id_usuario = isset($_COOKIE['adm_id_usuario'])&&$_COOKIE['adm_id_usuario']>0?$_COOKIE['adm_id_usuario']:$this->view->auth->id_usuario;
    	
    	$status=0; $mensagem='';
    	
    	if (empty($id_produto)||empty($quantidade)||empty($id_usuario)) {
    		$mensagem='Todos os campos devem ser informados.';
    	} else {
    	
	    	try {
	    		
	    		$dbAdapter = Zend_Db_Table::getDefaultAdapter ();
	    		
	    		$select_cliente = $dbAdapter->select()->from('usuario',array('id_cliente','local_padrao'))->where('id_usuario='.$id_usuario);
	    		$rs_cliente = $dbAdapter->fetchRow($select_cliente);
	    		$id_cliente = $rs_cliente['id_cliente'];
	    		$local_padrao = $rs_cliente['local_padrao'];
								
				$select = $dbAdapter->select()->from(array('PR' => 'produto_revenda'),array('PR.id_produto_revenda'))
				->joinInner(array('PL' => 'produto_linha'), 'PR.id_produto_revenda=PL.id_produto_revenda', array())
				->joinInner(array('PC' => 'produto_categoria'), 'PL.id_produto_linha=PC.id_produto_linha', array())
				->joinInner(array('P' => 'produto'), 'PC.id_produto_categoria=P.id_produto_categoria', array())
				->group('PL.id_produto_revenda')->where('P.id_produto='.$id_produto)->limit(1);
	    		$rs_produto_revenda = $dbAdapter->fetchRow($select);
	    		$id_produto_revenda = $rs_produto_revenda['id_produto_revenda']; 
	    		
	    		//PEGANDO O VALOR DO PRODUTO
	    		$produto_valor = Porto80_Core::getProdutoValor($id_produto, $local_padrao, $id_cliente);
	    		$valor_unitario = $produto_valor['final'];
	    		$valor_liquido = $produto_valor['liquido'];
	    		
	    		// VERIFICA SE O ITEM JA EXISTE NO CARRINHO
	    		$select_qtd  = $dbAdapter->select()->from('carrinho','quantidade')->where('id_usuario='.$id_usuario.' AND id_produto='.$id_produto);
	    		$old_quantidade = $dbAdapter->fetchOne($select_qtd);
	    		
	    		if (!empty($old_quantidade)) {
	    			
	    			//ATUALIZA O PRODUTO JA EXISTENTE
	    			$quantidade = $old_quantidade + $quantidade;
	    			$dbAdapter->update('carrinho',array('quantidade'=>$quantidade), 'id_produto='.$id_produto.' AND id_usuario='.$id_usuario);
	    			
	    		} else {
		    		$dbAdapter->insert('carrinho',array(
		    			'id_usuario'=>$id_usuario,
		    			'id_produto'=>$id_produto,
		    			'id_produto_revenda'=>$id_produto_revenda,
		    			'quantidade'=>$quantidade,
		    			'valor_liquido'=>$valor_liquido,
		    			'valor_unitario'=>$valor_unitario
	    			));	    			
	    		}
	    		$status=1;
	    		
	    	} catch (Zend_Db_Exception $e) {
				$mensagem = $e->getMessage();
				$this->_helper->FlashMessenger( array('warning' => htmlentities($mensagem) ) );
			}

    	}
    	
    	if ($this->_request->isXmlHttpRequest()) {
    		$retorno = array('status'=>$status,'mensagem'=>rawurlencode($mensagem));
    		echo Zend_Json::encode($retorno);
    	}
    	
    }
    
    
    /* FUN��O PARA REMOVER PRODUTO DO CARRINHO */
    public function delProdutoAction() {
    	
        $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
    	
		$id_produto = $this->_request->getParam('id');
    	$id_usuario = isset($_COOKIE['adm_id_usuario'])&&$_COOKIE['adm_id_usuario']>0?$_COOKIE['adm_id_usuario']:$this->view->auth->id_usuario;
    	
    	$status=0; $mensagem='';
    	
    	if (empty($id_produto)||empty($id_usuario)) {
    		$mensagem='Todos os campos devem ser informados.';
    	} else {
    	
	    	try {
	    		
	    		$dbAdapter = Zend_Db_Table::getDefaultAdapter ();
	    		
	    		$select_cliente = $dbAdapter->select()->from('usuario',array('id_cliente','local_padrao'))->where('id_usuario='.$id_usuario);
	    		$rs_cliente = $dbAdapter->fetchRow($select_cliente);
	    		$id_cliente = $rs_cliente['id_cliente'];
	    		$local_padrao = $rs_cliente['local_padrao'];
	    		
	    		$dbAdapter->delete('carrinho','id_produto='.$id_produto.' AND id_usuario='.$id_usuario);
	    		$status=1;
	    		
	    	} catch (Zend_Db_Exception $e) {
				$mensagem = $e->getMessage();
				$this->_helper->FlashMessenger( array('warning' => htmlentities( $mensagem) ) );
			}

    	}
    	
    	if ($this->_request->isXmlHttpRequest()) {
    		$retorno = array('status'=>$status,'mensagem'=>rawurlencode($mensagem));
    		echo Zend_Json::encode($retorno);
    	}
    	
    }
    
    /* FUN��O PARA ATUALIZAR PRODUTO DO CARRINHO */
    public function updProdutoAction() {
    	
        $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
    	
		$id_produto = $this->_request->getParam('id');
    	$quantidade = $this->_request->getParam('q');
    	$id_usuario = isset($_COOKIE['adm_id_usuario'])&&$_COOKIE['adm_id_usuario']>0?$_COOKIE['adm_id_usuario']:$this->view->auth->id_usuario;
    	
    	$status=0; $mensagem='';
    	
    	if (empty($id_produto)||empty($quantidade)||empty($id_usuario)) {
    		$mensagem='Todos os campos devem ser informados.';
    	} else {
    	
	    	try {
	    		
	    		$dbAdapter = Zend_Db_Table::getDefaultAdapter ();
	    		
	    		$select_cliente = $dbAdapter->select()->from('usuario',array('id_cliente','local_padrao'))->where('id_usuario='.$id_usuario);
	    		$rs_cliente = $dbAdapter->fetchRow($select_cliente);
	    		$id_cliente = $rs_cliente['id_cliente'];
	    		$local_padrao = $rs_cliente['local_padrao'];
				
				$select = $dbAdapter->select()->from(array('PR' => 'produto_revenda'),array('PR.id_produto_revenda'))
				->joinInner(array('PL' => 'produto_linha'), 'PR.id_produto_revenda=PL.id_produto_revenda', array())
				->joinInner(array('PC' => 'produto_categoria'), 'PL.id_produto_linha=PC.id_produto_linha', array())
				->joinInner(array('P' => 'produto'), 'PC.id_produto_categoria=P.id_produto_categoria', array())
				->group('PL.id_produto_revenda')->where('P.id_produto='.$id_produto)->limit(1);
	    		$rs_produto_revenda = $dbAdapter->fetchRow($select);
	    		$id_produto_revenda = $rs_produto_revenda['id_produto_revenda'];
	    		
	    		//verifica se o produto ja esta no carrinho
	    		$select = $dbAdapter->select()->from('carrinho','quantidade')->where('id_usuario='.$id_usuario.' AND id_produto='.$id_produto);
	    		$qtdcar = $dbAdapter->fetchOne($select);
	    		if ($qtdcar<=0) {
	    			
	    			//PEGANDO O VALOR DO PRODUTO
	    			$select_id_cliente = $dbAdapter->select()->from('usuario','id_cliente')->where('id_usuario='.$id_usuario);
	    			$id_cliente = $dbAdapter->fetchOne($select_id_cliente);
	    			$produto_valor = Porto80_Core::getProdutoValor($id_produto, $local_padrao, $id_cliente);
	    			$valor_unitario = $produto_valor['final'];
	    			$valor_liquido = $produto_valor['liquido'];
	    			 
	    			// VERIFICA SE O ITEM JA EXISTE NO CARRINHO
	    			$select_qtd  = $dbAdapter->select()->from('carrinho','quantidade')->where('id_usuario='.$id_usuario.' AND id_produto='.$id_produto);
	    			$old_quantidade = $dbAdapter->fetchOne($select_qtd);
	    			 
	    			if (!empty($old_quantidade)) {
	    			
	    				//ATUALIZA O PRODUTO JA EXISTENTE
	    				$quantidade = $old_quantidade + $quantidade;
	    				$dbAdapter->update('carrinho',array('quantidade'=>$quantidade), 'id_produto='.$id_produto.' AND id_usuario='.$id_usuario);
	    			
	    			} else {
	    				$dbAdapter->insert('carrinho',array(
	    						'id_usuario'=>$id_usuario,
	    						'id_produto'=>$id_produto,
	    						'id_produto_revenda'=>$id_produto_revenda,
	    						'quantidade'=>$quantidade,
	    						'valor_liquido'=>$valor_liquido,
	    						'valor_unitario'=>$valor_unitario
	    				));
	    			}
	    			
	    		} else {
	    		
		    		$dbAdapter->update('carrinho',array('quantidade'=>$quantidade), 'id_produto='.$id_produto.' AND id_usuario='.$id_usuario);
		    		
	    		}
	    		
	    		$status=1;
	    		
	    	} catch (Zend_Db_Exception $e) {
				$mensagem = $e->getMessage();
				$this->_helper->FlashMessenger( array('warning' => htmlentities($mensagem) ) );
			}

    	}
    	
    	if ($this->_request->isXmlHttpRequest()) {
    		$retorno = array('status'=>$status,'mensagem'=>rawurlencode($mensagem));
    		echo Zend_Json::encode($retorno);
    	}
    	
    }

	public function updMetodoPagamentoAction() {
        $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		
		$revenda = $this->_request->getParam('revenda','');
		if (!empty($revenda)) {
			$desconto = strtoupper($this->_request->getParam('mp','PRAZO'));
			echo setcookie('carrinho_'.$revenda.'_mp',$desconto,0,'/');
		} else {
			echo 0;
		}
	}

}

