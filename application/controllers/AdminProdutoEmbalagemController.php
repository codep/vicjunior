<?php

class AdminProdutoEmbalagemController extends Zend_Controller_Action
{

    public function init()
    {
    }

    public function indexAction()
    {
		$this->_helper->redirector('pesquisar','admin-produto-embalagem');
    }
    
    public function pesquisarAction() {
    	
        $pagina = $this->_request->getParam('pagina', 1) ;
        
        try {
        	
			$dbAdapter = Zend_Db_Table::getDefaultAdapter ();
	        
			/* FILTRO */
	        $key = $this->getRequest()->getPost('key');
	        $this->view->key = $key;
	        
	        $select = $dbAdapter->select()->from(array('PE'=>'produto_embalagem'))->order('PE.ordem ASC')->group('PE.id_produto_embalagem');
	        
	        if ($key!='') {
	        	$select->where('PE.titulo LIKE "%'.$key.'%"');
	        }
	        
	        $result = $dbAdapter->fetchAll($select);
	    	
		    /* PAGINA��O */
		    $dados = Zend_Paginator::factory($result);
		    $dados->setCurrentPageNumber( intval($pagina) );
		    $dados->setItemCountPerPage(20);
	        $this->view->dados = $dados;
        	
        	
        } catch (Zend_Db_Exception $e) {
        	$this->_helper->FlashMessenger( array('warning' => htmlentities($e->getMessage()) ) );
        	$this->_helper->redirector('index','admin-resumo');
        }
    	
    	
    }

    public function novoAction()
    {
    	$this->view->headScript()
			->appendFile( $this->view->baseUrl('/admin_js/jquery.validate_pack.js'),'text/javascript')
		;
		
		$this->_helper->viewRenderer->render('formulario');
		
    }
    
    public function editarAction() {
    	
		$id = $this->_request->getParam('id');
		
    	if (!$id) {
    		$this->_helper->FlashMessenger( array('warning' => htmlentities('O registro informado n�o existe. Verifique e tente novamente.') ) );
    		$this->_helper->redirector('pesquisar','admin-produto-embalagem');
    	}
		
    	$this->view->headScript()
			->appendFile( $this->view->baseUrl('/admin_js/jquery.validate_pack.js'),'text/javascript')
		;
		
    	//CARREGAR BANCO DE DADOS		
		try {
			
			$dbAdapter = Zend_Db_Table::getDefaultAdapter ();
			
			$select = $dbAdapter->select()->from(array('PE'=>'produto_embalagem'))->where('id_produto_embalagem='.$id)->limit(1);			
			$dados = $dbAdapter->fetchRow($select);
			$this->view->dados = $dados;
				
		} catch (Zend_Db_Exception $e) {
			
			$this->_helper->FlashMessenger( array('warning' => htmlentities( $e->getMessage()) ) );
			$this->_helper->redirector('pesquisar','admin-produto-embalagem');
			
		}
    	
    	$this->_helper->viewRenderer->render('formulario');
    }
    
    public function salvarAction() {
    	
        $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);    	
    	
		if ($this->_request->isPost()) {
			
			$id_produto_embalagem = $this->_request->getPost('id_produto_embalagem', 0);
			
			try {
				
				$dbAdapter = Zend_Db_Table::getDefaultAdapter ();
				
    			$dados = array(
    				'titulo'=>			$this->getRequest()->getPost('titulo'),
    				'ordem'=>			$this->getRequest()->getPost('ordem')
    			);
    			
		    	if ($id_produto_embalagem<=0) {
		    		//INCLUINDO REGISTRO
		    		$insert= $dbAdapter->insert('produto_embalagem',$dados);
		    		$lastID = $dbAdapter->lastInsertId('produto_embalagem','id_produto_embalagem');
		    		$this->_helper->FlashMessenger( array('success' => htmlentities('Registro inclu�do!') ) );
		    	} else {
		    		//ATUALIZANDO REGISTRO
		    		$update = $dbAdapter->update('produto_embalagem',$dados,'id_produto_embalagem='.$id_produto_embalagem);
		    		$this->_helper->FlashMessenger( array('success' => htmlentities('Registro atualizado!') ) );
		    		$lastID = $id_produto_embalagem;
		    	}
		    	
		    	$this->_helper->redirector('pesquisar','admin-produto-embalagem');
					
			} catch (Zend_Db_Exception $e) {
				$this->_helper->FlashMessenger( array('error' => htmlentities($e->getMessage()) ) );
				$this->_helper->redirector('pesquisar','admin-produto-embalagem');
			}
			
		}
    	
    }
    
    public function excluirAction() {
    	
        $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
    	
    	try {
    		
    		$dbAdapter = Zend_Db_Table::getDefaultAdapter ();
    		
        	$id = $this->_request->getParam('id');
    		
	    	if (!$id) {
	    		$this->_helper->FlashMessenger( array('warning' => htmlentities('Aten��o! ID incorreto, tente novamente.') ) );
	    	} else {	    		
    			//REMOVENDO REGISTROS
	    		$delete = $dbAdapter->delete('produto_embalagem','id_produto_embalagem = '.$id);
	    		//MENSAGEM DE SUCESSO
	    		$this->_helper->FlashMessenger( array('success' => htmlentities('Registro exclu�do!') ) );
	    	}
    			
    	} catch (Zend_Db_Exception $e) {
    		$this->_helper->FlashMessenger( array('warning' => htmlentities( $e->getMessage()) ) );
    	}
		
    	$this->_helper->redirector('pesquisar','admin-produto-embalagem');
    	
    }

}

