<?php

class AjaxController extends Zend_Controller_Action
{

    public function init()
    {
        $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
    }

    public function indexAction()
    {

    	
    }
    
    public function fileUploadAction() {
    	
    	$msg='';$status=0;
    	
	    $pathUpload = dirname(APPLICATION_PATH) . DIRECTORY_SEPARATOR . 'upload' . DIRECTORY_SEPARATOR;
	
		if($_SERVER['REQUEST_METHOD']==='POST') {
		    $tipoEnvio       =  isset($_POST['tipoEnvio'])?$this->url_amigavel($_POST['tipoEnvio']):'arquivo'; 
		    $pastaDest       =  isset($_POST['pastaDest'])?$this->url_amigavel($_POST['pastaDest']).DIRECTORY_SEPARATOR:'';
		    
			$pathUpload		.= $tipoEnvio . DIRECTORY_SEPARATOR;
			if (!is_dir($pathUpload) && $tipoEnvio!='') mkdir($pathUpload);
			
		    $dirImg = $pathUpload.$pastaDest;
			if (!is_dir($dirImg) && $pastaDest!='') mkdir($dirImg);		    
		    
		} else {
		    die('Acesso inv�lido');
		}
		
		if (isset($_FILES)) {
		    $tempFile = $_FILES['Filedata']['tmp_name'];
		    $targetFile = $_FILES['Filedata']['name'];
		} else {
		    $tempFile = $targetFile = '';
		}
	    	
		try {
	        $fileName='';
	        if ($tempFile!='') {
	            $fileTypes  = str_replace('*.','',$_REQUEST['fileext']);
	            $fileTypes  = str_replace(';','|',$fileTypes);
	            $typesArray = preg_split('/\|/',$fileTypes);
	            $fileParts  = pathinfo(strtolower($_FILES['Filedata']['name']));
	
	            if (in_array($fileParts['extension'],$typesArray)) {
	            	
	                
	                if (!is_dir($dirImg) && $pastaDest!='') mkdir($dirImg);
	                $dirImg = str_replace('//','/',$dirImg);
	
	                $fext  = strtolower(strrchr($targetFile, '.'));
	                $fname = $this->url_amigavel(str_ireplace($fext,'',$targetFile),'_');
	                
	                //VERIFICANDO SE JA EXISTE UM ARQUIVO COM ESSE nome
	                if (is_file($dirImg.$fname.$fext)) {
	                    //ADICIONANDO DIFERENCIAL PARA N�O COINCIDIR OS ARQUIVOS
	                    $fname .='_'.time();
	                }
	                
	                $fileName = $fname.$fext;
	                $destFile =  $dirImg . $fileName;
	                
	                if ( !move_uploaded_file($tempFile,$destFile) ) {
	                    // error
	                    $status=0;
	                    $msg = "N�o foi poss�vel enviar o arquivo.";
	                }
	
	                $status=1;
	                $msg = "Arquivo Enviado com sucesso!";
	            } else {
	                $status=0;
	                $msg = "Tipo de arquivo inv�lido";
	            }
	        } else {
	            $status=0;
	            $msg = "Arquivo inv�lido";
	        }
			
		} catch (Exception $e) {
            $status=0;
            $msg = $e->getMessage();
		}
        $retorno = array('status'=>$status,'msg'=>rawurlencode($msg),'fname'=>$fileName,'path'=>rawurlencode($tipoEnvio.DIRECTORY_SEPARATOR.$pastaDest));
        die( json_encode($retorno) );
    }
    
    public function cepAction() 
    {
    	$cep = $this->getRequest()->getPost('codigo');
    	
    	$resultado=array(
    		'tipo'=>0,
    		'mensagem'=>'CEP INCORRETO',
    		'endereco'=>'',
    		'bairro'=>'',
    		'cidade'=>'',
    		'estado'=>''
    	);
    	
		if (!empty($cep)) {
    	
	    	$webservice_url     = 'http://webservice.uni5.net/web_cep.php';
			$webservice_query    = array(
	    		'auth'    => '4f279ae2651cf5daf727cf12dfbf3840', //Chave de autentica��o do WebService - Consultar seu painel de controle
	    		'formato' => 'query_string', //Valores poss�veis: xml, query_string ou javascript
	    		'cep'     => $cep //CEP que ser� pesquisado
			);
	
			//Forma URL
			$webservice_url .= '?';
			foreach($webservice_query as $get_key => $get_value){
			    $webservice_url .= $get_key.'='.urlencode($get_value).'&';
			}
			
			try {
				parse_str(file_get_contents($webservice_url), $retorno);
			} catch (Exception $e) {
				$resultado['mensagem']=$e->getMessage();
				
			}
			
			//var_dump($retorno);
			//die();
			
			switch($retorno['resultado']){  
			    case '2':
			    	$resultado=array(
			    		'tipo'=>2,
			    		'mensagem'=>"Cidade com CEP unico",
			    		'endereco'=>'',
			    		'bairro'=>'',
			    		'cidade'=>rawurlencode($retorno['cidade']),
			    		'estado'=>rawurlencode($retorno['uf'])
			    	);
			    break;  
			      
			    case '1':		    	
			    	$resultado=array(
			    		'tipo'=>1,
			    		'mensagem'=>"Cidade com logradouro completo",
			    		'endereco'=>rawurlencode($retorno['tipo_logradouro']).' '.rawurlencode($retorno['logradouro']),
			    		'bairro'=>rawurlencode($retorno['bairro']),
			    		'cidade'=>rawurlencode($retorno['cidade']),
			    		'estado'=>rawurlencode($retorno['uf'])
			    	);
			    break;  
	 
			}
			
		}
		
		echo json_encode($resultado);		
    	
    }

    
    /***
	 * Fun��o para remover acentos de uma string
	 *
	 * @autor Thiago Belem <contato@thiagobelem.net>
	 */
	function url_amigavel($string, $slug = '-') {
		
		$string = strtolower($string);
	
		// C�digo ASCII das vogais
		$ascii['a'] = range(224, 230);
		$ascii['e'] = range(232, 235);
		$ascii['i'] = range(236, 239);
		$ascii['o'] = array_merge(range(242, 246), array(240, 248));
		$ascii['u'] = range(249, 252);
	
		// C�digo ASCII dos outros caracteres
		$ascii['b'] = array(223);
		$ascii['c'] = array(231);
		$ascii['d'] = array(208);
		$ascii['n'] = array(241);
		$ascii['y'] = array(253, 255);
	
		foreach ($ascii as $key=>$item) {
			$acentos = '';
			foreach ($item AS $codigo) $acentos .= chr($codigo);
			$troca[$key] = '/['.$acentos.']/i';
		}
	
		$string = preg_replace(array_values($troca), array_keys($troca), $string);
	
		// Slug?
		if ($slug) {
			// Troca tudo que n�o for letra ou n�mero por um caractere ($slug)
			$string = preg_replace('/[^a-z0-9]/i', $slug, $string);
			// Tira os caracteres ($slug) repetidos
			$string = preg_replace('/' . $slug . '{2,}/i', $slug, $string);
			$string = trim($string, $slug);
		}
	
		return $string;
	}

}

