<?php

class AdminProdutoCategoriaController extends Zend_Controller_Action
{

    public function init()
    {
    }

    public function indexAction()
    {
		$this->_helper->redirector('pesquisar','admin-produto-categoria');
    }
    
    public function pesquisarAction() {
    	
        $pagina = $this->_request->getParam('pagina', 1) ;
        
        try {
        	
			$dbAdapter = Zend_Db_Table::getDefaultAdapter ();
	        
			/* FILTRO */
			$id_produto_revenda = $this->getRequest()->getParam('revenda');
	        $key = $this->getRequest()->getParam('key');
	        $this->view->key = $key;
	        $this->view->id_produto_revenda = $id_produto_revenda;
	        
	        $select = $dbAdapter->select()->from(array('PC'=>'produto_categoria'),array('PC.*','total_produtos'=>'COUNT(P.id_produto)'))
	        ->joinInner(array('PG'=>'produto_linha'),'PG.id_produto_linha=PC.id_produto_linha',array('grupo'=>'PG.titulo'))
	        ->joinLeft(array('P'=>'produto'),'P.id_produto_categoria=PC.id_produto_categoria',array())
	        ->order('PG.titulo ASC')->order('PC.titulo ASC')->group('PC.id_produto_categoria');
	        
	        if ($key!='') {
	        	$select->where('PC.titulo LIKE "%'.$key.'%" OR PG.titulo LIKE "%'.$key.'%"');
	        }
	        
	        /* REVENDA */
	        if (!empty($id_produto_revenda)) {
	        	$select->where('PG.id_produto_revenda='.$id_produto_revenda);
	        }	        
	        
	        $result = $dbAdapter->fetchAll($select);
	    	
		    /* PAGINACAO */
		    $dados = Zend_Paginator::factory($result);
		    $dados->setCurrentPageNumber( intval($pagina) );
		    $dados->setItemCountPerPage(20);
	        $this->view->dados = $dados;
	        
	        /* REVENDAS */
	        $select  = $dbAdapter->select()->from(array('PR'=>'produto_revenda'))->order('PR.titulo ASC');
	        $this->view->revendas = $dbAdapter->fetchAll($select);
        	
        } catch (Zend_Db_Exception $e) {
        	$this->_helper->FlashMessenger( array('warning' => htmlentities($e->getMessage()) ) );
        	$this->_helper->redirector('index','admin-resumo');
        }
    	
    	
    }

    public function novoAction()
    {
    	$this->view->headScript()
			->appendFile( $this->view->baseUrl('/js/jquery.validate_pack.js'),'text/javascript')
		;
    		
        //CARREGAR BANCO DE DADOS		
		try {
			
			$dbAdapter = Zend_Db_Table::getDefaultAdapter ();
			
			/* GRUPOS */
			$select = $dbAdapter->select()->from(array('PG'=>'produto_linha'))->order('PG.titulo ASC');
			$dados = $dbAdapter->fetchAll($select);
			$this->view->grupos = $dados;
							
		} catch (Zend_Db_Exception $e) {
			
			$this->_helper->FlashMessenger( array('warning' => htmlentities( $e->getMessage()) ) );
			$this->_helper->redirector('pesquisar','admin-produto-categoria');
			
		}
		
		$this->_helper->viewRenderer->render('formulario');
		
    }
    
    public function editarAction() {
    	
		$id = $this->_request->getParam('id');
		
    	if (!$id) {
    		$this->_helper->FlashMessenger( array('warning' => htmlentities('O registro informado n�o existe. Verifique e tente novamente.') ) );
    		$this->_helper->redirector('pesquisar','admin-produto-categoria');
    	}
		
    	$this->view->headScript()
			->appendFile( $this->view->baseUrl('/js/jquery.validate_pack.js'),'text/javascript')
		;
		
    	//CARREGAR BANCO DE DADOS		
		try {
			
			$dbAdapter = Zend_Db_Table::getDefaultAdapter ();
			
			/* GRUPOS */
			$select = $dbAdapter->select()->from(array('PG'=>'produto_linha'))->order('PG.titulo ASC');
			$dados = $dbAdapter->fetchAll($select);
			$this->view->grupos = $dados;
			
			/* DADOS */
			$select = $dbAdapter->select()->from(array('PG'=>'produto_categoria'))->where('id_produto_categoria='.$id)->limit(1);			
			$dados = $dbAdapter->fetchRow($select);
			$this->view->dados = $dados;
				
		} catch (Zend_Db_Exception $e) {
			
			$this->_helper->FlashMessenger( array('warning' => htmlentities( $e->getMessage()) ) );
			$this->_helper->redirector('pesquisar','admin-produto-categoria');
			
		}
    	
    	$this->_helper->viewRenderer->render('formulario');
    }
    
    public function salvarAction() {
    	
        $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);    	
    	
		if ($this->_request->isPost()) {
			
			$id_produto_categoria = $this->_request->getPost('id_produto_categoria', 0);
			
			try {
				
				$dbAdapter = Zend_Db_Table::getDefaultAdapter ();
				
    			$dados = array(
    				'id_produto_linha'=>	$this->getRequest()->getPost('id_produto_linha'),
        			'permlink'=>			Porto80_Core::url_amigavel($this->getRequest()->getPost('titulo')),
    				'titulo'=>				$this->getRequest()->getPost('titulo')
    			);
    			
		    	if ($id_produto_categoria<=0) {
		    		//INCLUINDO REGISTRO
		    		$insert= $dbAdapter->insert('produto_categoria',$dados);
		    		$lastID = $dbAdapter->lastInsertId('produto_categoria','id_produto_categoria');
		    		$this->_helper->FlashMessenger( array('success' => htmlentities('Registro inclu�do!') ) );
		    	} else {
		    		//ATUALIZANDO REGISTRO
		    		$update = $dbAdapter->update('produto_categoria',$dados,'id_produto_categoria='.$id_produto_categoria);
		    		$this->_helper->FlashMessenger( array('success' => htmlentities('Registro atualizado!') ) );
		    		$lastID = $id_produto_categoria;
		    	}
		    	
		    	$this->_helper->redirector('pesquisar','admin-produto-categoria');
					
			} catch (Zend_Db_Exception $e) {
				$this->_helper->FlashMessenger( array('error' => htmlentities($e->getMessage()) ) );
				$this->_helper->redirector('pesquisar','admin-produto-categoria');
			}
			
		}
    	
    }
    
    public function excluirAction() {
    	
        $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
    	
    	try {
    		
    		$dbAdapter = Zend_Db_Table::getDefaultAdapter ();
    		
        	$id = $this->_request->getParam('id');
    		
	    	if (!$id) {
	    		$this->_helper->FlashMessenger( array('warning' => htmlentities('Aten��o! ID incorreto, tente novamente.') ) );
	    	} else {	    		
    			//REMOVENDO REGISTROS
	    		$delete = $dbAdapter->delete('produto_categoria','id_produto_categoria = '.$id);
	    		//MENSAGEM DE SUCESSO
	    		$this->_helper->FlashMessenger( array('success' => htmlentities('Registro exclu�do!') ) );
	    	}
    			
    	} catch (Zend_Db_Exception $e) {
    		$this->_helper->FlashMessenger( array('warning' => htmlentities( $e->getMessage()) ) );
    	}
		
    	$this->_helper->redirector('pesquisar','admin-produto-categoria');
    	
    }
    

}

