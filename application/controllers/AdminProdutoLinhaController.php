<?php

class AdminProdutoLinhaController extends Zend_Controller_Action
{

    public function init()
    {
    }

    public function indexAction()
    {
		$this->_helper->redirector('pesquisar','admin-produto-linha');
    }
    
    public function pesquisarAction() {
    	
        $pagina = $this->_request->getParam('pagina', 1) ;
        
        try {
        	
			$dbAdapter = Zend_Db_Table::getDefaultAdapter ();
	        
			/* FILTRO */
	        $key = $this->getRequest()->getPost('key');
	        $this->view->key = $key;
	        
	        $select = $dbAdapter->select()->from(array('PG'=>'produto_linha'),array('PG.*','total_categorias'=>'COUNT(id_produto_categoria)'))
	        ->joinLeft(array('PR'=>'produto_revenda'),'PR.id_produto_revenda=PG.id_produto_revenda',array('revenda'=>'PR.titulo'))
	        ->joinLeft(array('PC'=>'produto_categoria'),'PC.id_produto_linha=PG.id_produto_linha',array())
	        ->order('PG.titulo ASC')->group('PG.id_produto_linha');
	        
	        if ($key!='') {
	        	$select->where('PG.titulo LIKE "%'.$key.'%"');
	        }
	        
	        $result = $dbAdapter->fetchAll($select);
	    	
		    /* PAGINACAO */
		    $dados = Zend_Paginator::factory($result);
		    $dados->setCurrentPageNumber( intval($pagina) );
		    $dados->setItemCountPerPage(20);
	        $this->view->dados = $dados;
        	
        	
        } catch (Zend_Db_Exception $e) {
        	$this->_helper->FlashMessenger( array('warning' => htmlentities($e->getMessage()) ) );
        	$this->_helper->redirector('index','admin-resumo');
        }
    	
    	
    }

    public function novoAction()
    {
    	$this->view->headScript()
    		->appendFile( $this->view->baseUrl('/admin_js/jquery-custom-file-input.js'),'text/javascript')
			->appendFile( $this->view->baseUrl('/admin_js/jquery.validate_pack.js'),'text/javascript')
		;
		
		//CARREGAR BANCO DE DADOS
		try {
		
			$dbAdapter = Zend_Db_Table::getDefaultAdapter ();
			
			/* REVENDAS */
			$select  = $dbAdapter->select()->from(array('PR'=>'produto_revenda'))->order('PR.titulo ASC');
			$this->view->revendas = $dbAdapter->fetchAll($select);
		
		} catch (Zend_Db_Exception $e) {
				
			$this->_helper->FlashMessenger( array('warning' => htmlentities( $e->getMessage()) ) );
			$this->_helper->redirector('pesquisar','admin-produto-linha');
				
		}
		
		$this->_helper->viewRenderer->render('formulario');
		
    }
    
    public function editarAction() {
    	
		$id = $this->_request->getParam('id');
		
    	if (!$id) {
    		$this->_helper->FlashMessenger( array('warning' => htmlentities('O registro informado n�o existe. Verifique e tente novamente.') ) );
    		$this->_helper->redirector('pesquisar','admin-produto-linha');
    	}
		
    	$this->view->headScript()
    		->appendFile( $this->view->baseUrl('/admin_js/jquery-custom-file-input.js'),'text/javascript')
			->appendFile( $this->view->baseUrl('/admin_js/jquery.validate_pack.js'),'text/javascript')
		;
		
    	//CARREGAR BANCO DE DADOS		
		try {
			
			$dbAdapter = Zend_Db_Table::getDefaultAdapter ();
			
			/* REVENDAS */
			$select  = $dbAdapter->select()->from(array('PR'=>'produto_revenda'))->order('PR.titulo ASC');
			$this->view->revendas = $dbAdapter->fetchAll($select);
			
			$select = $dbAdapter->select()->from(array('PG'=>'produto_linha'))->where('id_produto_linha='.$id)->limit(1);			
			$dados = $dbAdapter->fetchRow($select);
			$this->view->dados = $dados;
				
		} catch (Zend_Db_Exception $e) {
			
			$this->_helper->FlashMessenger( array('warning' => htmlentities( $e->getMessage()) ) );
			$this->_helper->redirector('pesquisar','admin-produto-linha');
			
		}
    	
    	$this->_helper->viewRenderer->render('formulario');
    }
    
    public function salvarAction() {
    	
        $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);    	
    	
		if ($this->_request->isPost()) {
			
			$id_produto_linha = $this->_request->getPost('id_produto_linha', 0);
			
			try {
				
				$dbAdapter = Zend_Db_Table::getDefaultAdapter ();
				
    			$dados = array(
    				'id_produto_revenda'=>			$this->getRequest()->getPost('id_produto_revenda'),
    				'titulo'=>			htmlentities($this->getRequest()->getPost('titulo')),
    				'permlink'=>		Porto80_Core::url_amigavel($this->getRequest()->getPost('titulo'))
    			);
    			
    			//VERIFICANDO FOTO
    			if (isset($_POST['limpar_imagem']) && $_POST['limpar_imagem']=='1') {
    				$dados['foto']='';
    				if ($id_produto_linha>0) {
    					$libFile = new Porto80_Arquivo();
    					$select = $dbAdapter->select()->from('produto_linha',array('foto'))->where('id_produto_linha='.$id_produto_linha);
    					$resultadoC = $dbAdapter->fetchRow($select);
    					$oldFoto = $resultadoC['foto'];
    					$cmdFile = $libFile->isFile($oldFoto,'banner');
    					if ($cmdFile['status']==1) {
    						$cmd = $libFile->delete($oldFoto,'banner');
    					}
    				}
    			}  else {
    				$newFoto = isset($_FILES['foto'])?$_FILES['foto']['name']:'';
    				if ($newFoto!='') {
    					$libFile = new Porto80_Arquivo();
    					if ($id_produto_linha>0) {
    						$select = $dbAdapter->select()->from('produto_linha',array('foto'))->where('id_produto_linha='.$id_produto_linha);
    						$resultadoC = $dbAdapter->fetchRow($select);
    						$oldFoto = $resultadoC['foto'];
    						$cmdFile = $libFile->isFile($oldFoto,'banner');
    						if ($cmdFile['status']==1) {
    							$cmd = $libFile->delete($oldFoto,'banner');
    						} else {
    							$this->_helper->FlashMessenger( array('error' => htmlentities($cmdFile['mensagem']) ) );
    						}
    					}
    					//FAZENDO UPLOAD DA FOTO
    					$cmd = $libFile->upload('foto','banner');
    					if ($cmd['status']==1) {
    						$newFoto = $cmd['mensagem'];
    						$redim = $libFile->sizeImg($newFoto,'banner',910,140);
    						$dados['foto']=$newFoto;
    					} else {
    						$this->_helper->FlashMessenger( array('error' => htmlentities($cmd['mensagem']) ) );
    					}
    				}    				
    			}
    			    			
		    	if ($id_produto_linha<=0) {
		    		//INCLUINDO REGISTRO
		    		$insert= $dbAdapter->insert('produto_linha',$dados);
		    		$lastID = $dbAdapter->lastInsertId('produto_linha','id_produto_linha');
		    		$this->_helper->FlashMessenger( array('success' => htmlentities('Registro inclu�do!') ) );
		    	} else {
		    		//ATUALIZANDO REGISTRO
		    		$update = $dbAdapter->update('produto_linha',$dados,'id_produto_linha='.$id_produto_linha);
		    		$this->_helper->FlashMessenger( array('success' => htmlentities('Registro atualizado!') ) );
		    		$lastID = $id_produto_linha;
		    	}
		    	
		    	$this->_helper->redirector('pesquisar','admin-produto-linha');
					
			} catch (Zend_Db_Exception $e) {
				$this->_helper->FlashMessenger( array('error' => htmlentities($e->getMessage()) ) );
				$this->_helper->redirector('pesquisar','admin-produto-linha');
			}
			
		}
    	
    }
    
    public function excluirAction() {
    	
        $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
    	
    	try {
    		
    		$dbAdapter = Zend_Db_Table::getDefaultAdapter ();
    		
        	$id = $this->_request->getParam('id');
    		
	    	if (!$id) {
	    		$this->_helper->FlashMessenger( array('warning' => htmlentities('Aten��o! ID incorreto, tente novamente.') ) );
	    	} else {	    		
    			//REMOVENDO REGISTROS
	    		$delete = $dbAdapter->delete('produto_linha','id_produto_linha = '.$id);
	    		//MENSAGEM DE SUCESSO
	    		$this->_helper->FlashMessenger( array('success' => htmlentities('Registro exclu�do!') ) );
	    	}
    			
    	} catch (Zend_Db_Exception $e) {
    		$this->_helper->FlashMessenger( array('warning' => htmlentities( $e->getMessage()) ) );
    	}
		
    	$this->_helper->redirector('pesquisar','admin-produto-linha');
    	
    }
    
    public function ordenarAction() {
    	
        try {
        	
			$dbAdapter = Zend_Db_Table::getDefaultAdapter ();
	               
	        $select = $dbAdapter->select()->from(array('PL'=>'produto_linha'))
	        ->joinLeft(array('PR'=>'produto_revenda'),'PR.id_produto_revenda=PL.id_produto_revenda',array('revenda'=>'PR.titulo'))
	        ->order('PR.ordem ASC')->order('PL.ordem ASC')->order('PL.titulo ASC');
	        
	        $revendas = '';
	        $linhas = $dbAdapter->fetchAll($select);
	        foreach($linhas as $linha) {
	        	$revendas[$linha['revenda']][]=array(	
	        		'id_produto_linha'=>$linha['id_produto_linha'],
	        		'titulo'=>$linha['titulo']
				);
	        }
	            
	        $this->view->dados = $revendas;
        	        	
        } catch (Zend_Db_Exception $e) {
        	$this->_helper->FlashMessenger( array('warning' => htmlentities($e->getMessage()) ) );
        	$this->_helper->redirector('index','admin-resumo');
        }
        
        /* ARMAZENA A URL DE PESQUISA */
    	$filtros = new Zend_Session_Namespace('filtros');
    	$keyfiltro = md5($this->_request->getControllerName().'_'.$this->_request->getActionName());
		$filtros->$keyfiltro = $this->view->url();
		$this->view->keyfiltro = $keyfiltro;
    	
    }
    
    public function salvarOrdemLinhaAction() {
    	
        $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		
		$status=0; $mensagem='';
    	
        try {
        	
			$dbAdapter = Zend_Db_Table::getDefaultAdapter ();
			
			$linhas = $this->getRequest()->getPost('linha');
			
			if (is_array($linhas)) {
				foreach($linhas as $ordem => $id_produto_linha) {
					$dbAdapter->update('produto_linha',array('ordem'=>$ordem+1),'id_produto_linha='.$id_produto_linha);
				}
				$status=1; $mensagem='Dados atualizados com sucesso!';
			}
    	
		} catch (Zend_Db_Exception $e) {
        	$mensagem=$e->getMessage();
        }
        
        $resultado = array('status'=>$status,'mensagem'=>rawurlencode($mensagem));
        
        echo Zend_Json::encode($resultado);
    	
    }

}

