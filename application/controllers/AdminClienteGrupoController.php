<?php

class AdminClienteGrupoController extends Zend_Controller_Action
{

    public function init()
    {
    }

    public function indexAction()
    {
		$this->_helper->redirector('pesquisar','admin-cliente-grupo');
    }
    
    public function pesquisarAction() {
    	
        $pagina = $this->_request->getParam('pagina', 1) ;
        
        try {
        	
			$dbAdapter = Zend_Db_Table::getDefaultAdapter ();
	        
			/* FILTRO */
	        $key = $this->getRequest()->getPost('key');
	        $this->view->key = $key;
	        
	        $select = $dbAdapter->select()->from(array('CG'=>'cliente_grupo'))
	        ->order('CG.titulo ASC');
	        
	        //->joinInner(array('CG'=>'cliente_grupo'),'CG.id_cliente_grupo=CGA.id_cliente_grupo',array('grupo'=>'CG.titulo'))
			//->joinInner(array('PL'=>'produto_linha'),'PL.id_produto_linha=CGA.id_produto_linha',array('linha'=>'PL.titulo'))
	        
	        if ($key!='') {
	        	$select->where('CG.titulo LIKE "%'.$key.'%"');
	        }
	        
	        $resultados = $dbAdapter->fetchAll($select);
	        
	        $dados='';
	        foreach($resultados as $resultado) {
	        	//ACRESCIMO
	        	$select_ac = $dbAdapter->select()->from(array('CGA'=>'cliente_grupo_acrescimo'),array('acrescimo'))
	        	->joinInner(array('PL'=>'produto_linha'),'PL.id_produto_linha=CGA.id_produto_linha',array('linha'=>'titulo'))
	        	->where('id_cliente_grupo='.$resultado['id_cliente_grupo'])->order('PL.titulo ASC');
	        	$dados_ac = $dbAdapter->fetchAll($select_ac);
	        	
	        	//NOVO DADOS
	        	$dados[]=array(
	        		'id_cliente_grupo'=>$resultado['id_cliente_grupo'],
	        		'titulo'=>$resultado['titulo'],
					'dados_ac'=>$dados_ac
	        	);
	        }
	        
	        $this->view->dados = $dados;
	        
        	
        	
        } catch (Zend_Db_Exception $e) {
        	$this->_helper->FlashMessenger( array('warning' => htmlentities($e->getMessage()) ) );
        	$this->_helper->redirector('index','admin-resumo');
        }
    	
    	
    }

    public function novoAction()
    {
    	$this->view->headScript()
			->appendFile( $this->view->baseUrl('/admin_js/jquery.validate_pack.js'),'text/javascript')
		;
		
       	//CARREGAR BANCO DE DADOS		
		try {
			
			$dbAdapter = Zend_Db_Table::getDefaultAdapter ();
			
			/* LINHAS */
			$select = $dbAdapter->select()->from(array('PL'=>'produto_linha'), array('id_produto_linha','titulo'))
			->joinLeft(array('CGA'=>'cliente_grupo_acrescimo'),'CGA.id_produto_linha=PL.id_produto_linha AND CGA.id_cliente_grupo=0',array('acrescimo'));			
			$dados = $dbAdapter->fetchAll($select);
			$this->view->linhas = $dados;
				
		} catch (Zend_Db_Exception $e) {
			
			$this->_helper->FlashMessenger( array('warning' => htmlentities( $e->getMessage()) ) );
			$this->_helper->redirector('pesquisar','admin-cliente-grupo');
			
		}
		
		$this->_helper->viewRenderer->render('formulario');
		
    }
    
    public function editarAction() {
    	
		$id = $this->_request->getParam('id');
		
    	if (!$id) {
    		$this->_helper->FlashMessenger( array('warning' => htmlentities('O registro informado n�o existe. Verifique e tente novamente.') ) );
    		$this->_helper->redirector('pesquisar','admin-cliente-grupo');
    	}
    	
    	$this->view->headScript()
			->appendFile( $this->view->baseUrl('/admin_js/jquery.validate_pack.js'),'text/javascript')
		;
		
    	//CARREGAR BANCO DE DADOS		
		try {
			
			$dbAdapter = Zend_Db_Table::getDefaultAdapter ();
			
			/* LINHAS */
			$select = $dbAdapter->select()->from(array('PL'=>'produto_linha'), array('id_produto_linha','titulo'))
			->joinLeft(array('CGA'=>'cliente_grupo_acrescimo'),'CGA.id_produto_linha=PL.id_produto_linha AND CGA.id_cliente_grupo='.$id,array('acrescimo'));			
			$dados = $dbAdapter->fetchAll($select);
			$this->view->linhas = $dados;
			
			/* DADOS */
			$select = $dbAdapter->select()->from(array('CG'=>'cliente_grupo'))->where('id_cliente_grupo='.$id)->limit(1);			
			$dados = $dbAdapter->fetchRow($select);
			$this->view->dados = $dados;
				
		} catch (Zend_Db_Exception $e) {
			
			$this->_helper->FlashMessenger( array('warning' => htmlentities( $e->getMessage()) ) );
			$this->_helper->redirector('pesquisar','admin-cliente-grupo');
			
		}
    	
    	$this->_helper->viewRenderer->render('formulario');
    }
    
    public function salvarAction() {
    	
        $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);    	
    	
		if ($this->_request->isPost()) {
			
			$id_cliente_grupo = $this->_request->getPost('id_cliente_grupo', 0);
			
			try {
				
				$dbAdapter = Zend_Db_Table::getDefaultAdapter ();
				
    			$dados = array(
    				'titulo'=>				$this->getRequest()->getPost('titulo')
    			);
    			
		    	if ($id_cliente_grupo<=0) {
		    		//INCLUINDO REGISTRO
		    		$insert= $dbAdapter->insert('cliente_grupo',$dados);
		    		$lastID = $dbAdapter->lastInsertId('cliente_grupo','id_cliente_grupo');
		    		$this->_helper->FlashMessenger( array('success' => htmlentities('Registro inclu�do!') ) );
		    	} else {
		    		//ATUALIZANDO REGISTRO
		    		$update = $dbAdapter->update('cliente_grupo',$dados,'id_cliente_grupo='.$id_cliente_grupo);
		    		$this->_helper->FlashMessenger( array('success' => htmlentities('Registro atualizado!') ) );
		    		$lastID = $id_cliente_grupo;
		    	}
		    	
		    	/* ADICIONANDO ACRESCIMOS */
		    	$delete = $dbAdapter->delete('cliente_grupo_acrescimo','id_cliente_grupo = '.$lastID);
		    	$select = $dbAdapter->select()->from('produto_linha');
		    	$linhas = $dbAdapter->fetchAll($select);
		    	foreach($linhas as $linha) {
		    		$acrescimo = $this->_request->getPost('acrescimo'.$linha['id_produto_linha']);
		    		if (!isset($acrescimo) || $acrescimo=='') $acrescimo=0;
		    		if (strpos($acrescimo,",")!==false) $acrescimo = Porto80_Core::converteFloat($acrescimo);
		    		if ($acrescimo>0 && is_numeric($acrescimo) ) {
			    		$insert = $dbAdapter->insert('cliente_grupo_acrescimo',array(
			    			'id_produto_linha'=>$linha['id_produto_linha'],
			    			'id_cliente_grupo'=>$lastID,
			    			'acrescimo'=>$acrescimo
			    		));
		    		}
		    	}
		    	
		    	$this->_helper->redirector('pesquisar','admin-cliente-grupo');
					
			} catch (Zend_Db_Exception $e) {
				$this->_helper->FlashMessenger( array('error' => htmlentities($e->getMessage()) ) );
				$this->_helper->redirector('pesquisar','admin-cliente-grupo');
			}
			
		}
    	
    }
    
    public function excluirAction() {
    	
        $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
    	
    	try {
    		
    		$dbAdapter = Zend_Db_Table::getDefaultAdapter ();
    		
        	$id = $this->_request->getParam('id');
    		
	    	if (!$id) {
	    		$this->_helper->FlashMessenger( array('warning' => htmlentities('Aten��o! ID incorreto, tente novamente.') ) );
	    	} else {	    		
    			//REMOVENDO REGISTROS
	    		$delete = $dbAdapter->delete('cliente_grupo','id_cliente_grupo = '.$id);
	    		$delete = $dbAdapter->delete('cliente_grupo_acrescimo','id_cliente_grupo = '.$id);
	    		//MENSAGEM DE SUCESSO
	    		$this->_helper->FlashMessenger( array('success' => htmlentities('Registro exclu�do!') ) );
	    	}
    			
    	} catch (Zend_Db_Exception $e) {
    		$this->_helper->FlashMessenger( array('warning' => htmlentities( $e->getMessage()) ) );
    	}
		
    	$this->_helper->redirector('pesquisar','admin-cliente-grupo');
    	
    }

}

