<?php

class OndeEncontroController extends Zend_Controller_Action
{

    public function init()
    {

    }
    
	public function indexAction()
    {
        try {
    		
    		$dbAdapter = Zend_Db_Table::getDefaultAdapter ();
    		
			//SELECIONAR AS REVENDAS
	        $select = $dbAdapter->select()->from(array('PR'=>'produto_revenda'))
	        ->joinInner(array('CR'=>'cliente_revenda'),'CR.id_produto_revenda=PR.id_produto_revenda',array())
	        ->group('PR.id_produto_revenda')->order('PR.ordem ASC');
	        //VERIFICA SE POSSUI CLIENTE LOGADO
	        $id_usuario = isset($_COOKIE['adm_id_usuario'])&&$_COOKIE['adm_id_usuario']>0?$_COOKIE['adm_id_usuario']:(isset($this->view->auth->id_usuario)?$this->view->auth->id_usuario:0);
	        if (!empty($id_usuario)) {
	        	$select->joinInner(array('U'=>'usuario'),'U.id_cliente=CR.id_cliente',array())->where('U.id_usuario="'.$id_usuario.'"');
	        }
	    	$this->view->revendas = $dbAdapter->fetchAll($select);
    		
    	} catch (Zend_Db_Exception $e) {
			$this->_helper->FlashMessenger( array('warning' => htmlentities( $e->getMessage()) ) );
			$this->_helper->redirector('index','index');
			
		}
    }

    public function exibirAction()
    {
    	$revenda = $this->_request->getParam('revenda','');
    	$estado = $this->_request->getParam('estado');
    	$cidade = $this->_request->getParam('cidade');
    	
    	if (empty($revenda)) $this->_helper->redirector('index');
    	
		//CARREGAR BANCO DE DADOS		
		try {
			
			$dbAdapter = Zend_Db_Table::getDefaultAdapter ();
			
			//SELECIONAR INFORMA��ES DA REVENDA
	        $select = $dbAdapter->select()->from('produto_revenda')->where('permlink="'.$revenda.'"')->limit(1);
	    	$this->view->revenda = $dbAdapter->fetchRow($select);
	    	$id_produto_revenda = $this->view->revenda['id_produto_revenda'];
	    	
	    	//CIDADES
        	$select = $dbAdapter->select()->from(array('C'=>'cliente'),array('estado'=>'C.estado','cidade'=>'C.cidade','cidadeuf'=>'CONCAT(C.estado," - ",C.cidade)'))->joinInner(array('CR'=>'cliente_revenda'),'C.id_cliente=CR.id_cliente')->where('CR.id_produto_revenda="'.$id_produto_revenda.'" AND C.exibirpublico=1')->group('C.cidade')->order(array('C.estado ASC','C.cidade ASC'));
	    	$this->view->cidades = $dbAdapter->fetchAll($select);
        	//PEGA O ID DA LINHA
        	//$select = $dbAdapter->select()->from('cliente')->where('estado="'.$estado.'"')->limit(1);
        	//$id_estado = $dbAdapter->fetchOne($select);
	    	
			//VERIFICA SE EXISTE UMA CIDADE PARA EXIBIR OS CLIENTES
	    	if ($cidade!='') {
	    		
	        	$select = $dbAdapter->select()->from(array('C'=>'cliente'))->joinInner(array('CR'=>'cliente_revenda'),'C.id_cliente=CR.id_cliente')->where('CR.id_produto_revenda="'.$id_produto_revenda.'" AND C.estado="'.$estado.'" AND C.cidade="'.$cidade.'" AND C.exibirpublico=1')->order('C.nome ASC');
		    	$this->view->clientes = $dbAdapter->fetchAll($select);
    	    	$this->view->cidade = $cidade;
    	    	$this->view->estado = $estado;
    	    	//PEGA O ID DA LINHA
    	    	//$select = $dbAdapter->select()->from('cliente')->where('estado="'.$estado.'"')->limit(1);
    	    	//$id_estado = $dbAdapter->fetchOne($select);
	    	} else {
	    		//$id_estado = 0;
	    		$this->view->clientes = array();
	    	}

				
		} catch (Zend_Db_Exception $e) {
			
			$this->_helper->FlashMessenger( array('warning' => htmlentities( $e->getMessage()) ) );
			$this->_helper->redirector('index','index');
			
		}

    }

}

