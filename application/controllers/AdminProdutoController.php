<?php

class AdminProdutoController extends Zend_Controller_Action
{

    public function init()
    {
    }

    public function indexAction()
    {
		$this->_helper->redirector('pesquisar','admin-produto');
    }
    
    public function pesquisarAction() {
    	
        $pagina = $this->_request->getParam('pagina', 1) ;
        
        try {
        	
			$dbAdapter = Zend_Db_Table::getDefaultAdapter ();
	        
			/* FILTRO */
			$id_produto_revenda = $this->getRequest()->getParam('revenda');
	        $id_produto_categoria = $this->getRequest()->getParam('categoria');
	        $key = $this->getRequest()->getParam('key');
	        $this->view->key = $key;
	        $this->view->id_produto_categoria = $id_produto_categoria;
	        $this->view->id_produto_revenda = $id_produto_revenda;
	        
	        $select = $dbAdapter->select()->from(array('P'=>'produto'))
	        ->joinLeft(array('PC'=>'produto_categoria'),'PC.id_produto_categoria=P.id_produto_categoria',array('categoria'=>'PC.titulo'))
	        ->joinLeft(array('PE'=>'produto_embalagem'),'PE.id_produto_embalagem=P.id_produto_embalagem',array('embalagem'=>'PE.titulo'))
			->joinLeft(array('PL'=>'produto_linha'),'PL.id_produto_linha=PC.id_produto_linha',array('linha'=>'PL.titulo'))
	        ->order('PL.titulo ASC')->order('PC.titulo ASC')->order('P.titulo ASC')->order('PE.ordem ASC');
	        if (!empty($key)) $select->where('P.titulo LIKE "%'.$key.'%"');
	        
	        /* CATEGORIA */
			if (!empty($id_produto_categoria)) {
				$select->where('PC.id_produto_categoria='.$id_produto_categoria);
			}
			/* REVENDA */
			if (!empty($id_produto_revenda)) {
				$select->where('PL.id_produto_revenda='.$id_produto_revenda);
			}
	        
	        $result = $dbAdapter->fetchAll($select);
	    	
		    /* PAGINACAO */
		    $dados = Zend_Paginator::factory($result);
		    $dados->setCurrentPageNumber( intval($pagina) );
		    $dados->setItemCountPerPage(20);
	        $this->view->dados = $dados;
	        
			/* CATEGORIAS */
			$select = $dbAdapter->select()->from(array('PC'=>'produto_categoria'),array('categoria'=>'PC.titulo','PC.id_produto_categoria'))->joinInner(array('PG'=>'produto_linha'),'PG.id_produto_linha=PC.id_produto_linha',array('grupo'=>'PG.titulo'))->order('PG.titulo ASC')->order('PC.titulo ASC');
			$dados = $dbAdapter->fetchAll($select);
			$categorias='';
			foreach($dados as $dado) {
				$categorias[$dado['grupo']][]=array('id_produto_categoria'=>$dado['id_produto_categoria'],'titulo'=>$dado['categoria']);
			}
			if ($categorias=='') $categorias=array();
			$this->view->categorias = $categorias;
			
			/* REVENDAS */
			$select  = $dbAdapter->select()->from(array('PR'=>'produto_revenda'))->order('PR.titulo ASC');
			$this->view->revendas = $dbAdapter->fetchAll($select);
        	        	
        } catch (Zend_Db_Exception $e) {
        	$this->_helper->FlashMessenger( array('warning' => htmlentities($e->getMessage()) ) );
        	$this->_helper->redirector('index','admin-resumo');
        }
        
        /* ARMAZENA A URL DE PESQUISA */
    	$filtros = new Zend_Session_Namespace('filtros');
    	$keyfiltro = md5($this->_request->getControllerName().'_'.$this->_request->getActionName());
		$filtros->$keyfiltro = $this->view->url();
		$this->view->keyfiltro = $keyfiltro;
    	
    }
    
    public function novoAction()
    {
    	$this->view->headScript()
    		->appendFile( $this->view->baseUrl('/admin_js/jquery-custom-file-input.js'),'text/javascript')
			->appendFile( $this->view->baseUrl('/admin_js/jquery.validate_pack.js'),'text/javascript')
			->appendFile( $this->view->baseUrl('/admin_js/tiny_mce/jquery.tinymce.js'),'text/javascript')
			->appendFile( $this->view->baseUrl('/admin_js/jquery.maskedinput-1.2.2.min.js'),'text/javascript')
			->appendFile( $this->view->baseUrl('/admin_js/jquery.tipTip.min.js'),'text/javascript')
		;
		
		try {
			
			$dbAdapter = Zend_Db_Table::getDefaultAdapter ();
			
			/* CATEGORIAS */
			$select = $dbAdapter->select()->from(array('PC'=>'produto_categoria'),array('categoria'=>'PC.titulo','PC.id_produto_categoria'))
			->joinInner(array('PG'=>'produto_linha'),'PG.id_produto_linha=PC.id_produto_linha',array('grupo'=>'PG.titulo'))
			->joinInner(array('PR'=>'produto_revenda'),'PR.id_produto_revenda=PG.id_produto_revenda',array('revenda'=>'PR.titulo'))
			->order('PR.titulo ASC')->order('PG.titulo ASC')->order('PC.titulo ASC');
			$dados = $dbAdapter->fetchAll($select);
			$categorias='';
			foreach($dados as $dado) {
				$categorias[$dado['grupo'].' ['.$dado['revenda'].']'][]=array('id_produto_categoria'=>$dado['id_produto_categoria'],'titulo'=>$dado['categoria']);
			}
			if ($categorias=='') $categorias=array();
			$this->view->categorias = $categorias;
			
			/* EMBALAGENS */
			$select = $dbAdapter->select()->from(array('PE'=>'produto_embalagem'))->order('PE.ordem ASC');
			$dados = $dbAdapter->fetchAll($select);
			$this->view->embalagens = $dados;
			
			/* GRUPO */
			$select = $dbAdapter->select()->from(array('P'=>'produto'),array('P.titulo','P.grupo'))
			->joinInner(array('PC'=>'produto_categoria'),'PC.id_produto_categoria=P.id_produto_categoria',array('categoria_titulo'=>'PC.titulo'))
			->where('P.grupo>0')
			->group('P.grupo')
			->order('PC.titulo ASC')->order('P.titulo ASC');
			$dados = $dbAdapter->fetchAll($select);
			$grupos='';
			foreach($dados as $dado) {
				$grupos[$dado['categoria_titulo']][]=array('grupo'=>$dado['grupo'],'titulo'=>$dado['titulo']);
			}
			if ($grupos=='') $grupos=array();
			$this->view->grupos = $grupos;
			
			$this->view->produtos_extra = array();
			
		} catch (Zend_Db_Exception $e) {
        	$this->_helper->FlashMessenger( array('warning' => htmlentities($e->getMessage()) ) );
        	$this->_helper->redirector('pesquisar','admin-produto');
		}
		
		/* MANIPULA URL DE RETORNO */
    	$filtros = new Zend_Session_Namespace('filtros');
    	$keyfiltro = $this->_request->getParam('b');
    	$this->view->keyfiltro = $keyfiltro;
    	if (!empty($keyfiltro) && isset($filtros->$keyfiltro)) {
    		$this->view->url_back=$filtros->$keyfiltro;
    	} else {
    		$this->view->url_back=$this->view->baseUrl('admin-produto/pesquisar');
    	}
		
		$this->_helper->viewRenderer->render('formulario');
		
    }
    
    public function editarAction() {
    	
    	$this->view->headScript()
    		->appendFile( $this->view->baseUrl('/admin_js/jquery-custom-file-input.js'),'text/javascript')
			->appendFile( $this->view->baseUrl('/admin_js/jquery.validate_pack.js'),'text/javascript')
			->appendFile( $this->view->baseUrl('/admin_js/tiny_mce/jquery.tinymce.js'),'text/javascript')
			->appendFile( $this->view->baseUrl('/admin_js/jquery.maskedinput-1.2.2.min.js'),'text/javascript')
			->appendFile( $this->view->baseUrl('/admin_js/jquery.tipTip.min.js'),'text/javascript')
		;

		try {
			
			$id_produto = $this->_request->getParam('id');
						
			$dbAdapter = Zend_Db_Table::getDefaultAdapter ();
			
			/* CATEGORIAS */
			$select = $dbAdapter->select()->from(array('PC'=>'produto_categoria'),array('categoria'=>'PC.titulo','PC.id_produto_categoria'))
			->joinInner(array('PG'=>'produto_linha'),'PG.id_produto_linha=PC.id_produto_linha',array('grupo'=>'PG.titulo'))
			->joinInner(array('PR'=>'produto_revenda'),'PR.id_produto_revenda=PG.id_produto_revenda',array('revenda'=>'PR.titulo'))
			->order('PR.titulo ASC')->order('PG.titulo ASC')->order('PC.titulo ASC');
			$dados = $dbAdapter->fetchAll($select);
			$categorias='';
			foreach($dados as $dado) {
				$categorias[$dado['grupo'].' ['.$dado['revenda'].']'][]=array('id_produto_categoria'=>$dado['id_produto_categoria'],'titulo'=>$dado['categoria']);
			}
			if ($categorias=='') $categorias=array();
			$this->view->categorias = $categorias;
			
			/* EMBALAGENS */
			$select = $dbAdapter->select()->from(array('PE'=>'produto_embalagem'))->order('PE.ordem ASC');
			$dados = $dbAdapter->fetchAll($select);
			$this->view->embalagens = $dados;
			
			/* EXTRAS */
			$select = $dbAdapter->select()->from(array('PE'=>'produto_extra'))
			->where('PE.id_produto='.$id_produto)
			->order('PE.local ASC');
			$dados = $dbAdapter->fetchAll($select); $dadosExtra=array();
			foreach($dados as $extra) {
				$produto_valor = Porto80_Core::getProdutoValor($id_produto, $extra['local']);				
				$dadosExtra[] = array(
					'local'=>$extra['local'],
					'desconto'=>Porto80_Core::converteFloat($extra['desconto'],2,false),
					'preco'=>Porto80_Core::converteFloat($extra['preco'],2,false),
					'iva'=>Porto80_Core::converteFloat($extra['iva'],2,false),
					'ipi'=>Porto80_Core::converteFloat($extra['ipi'],2,false),
					'icms_origem'=>Porto80_Core::converteFloat($extra['icms_origem'],2,false),
					'icms_destino'=>Porto80_Core::converteFloat($extra['icms_destino'],2,false),
					'frete'=>Porto80_Core::converteFloat($extra['frete'],2,false),
					'margem'=>Porto80_Core::converteFloat($extra['margem'],2,false),
					'vlliquido'=>Porto80_Core::converteFloat($produto_valor['liquido'],2,false),
					'vlfinal'=>Porto80_Core::converteFloat($produto_valor['final'],2,false),
					'vlmargem'=>Porto80_Core::converteFloat($produto_valor['margem'],2,false)
				);
			}
			$this->view->produtos_extra = $dadosExtra;
			
			/* GRUPO */
			$select = $dbAdapter->select()->from(array('P'=>'produto'),array('P.titulo','P.grupo'))
			->joinInner(array('PC'=>'produto_categoria'),'PC.id_produto_categoria=P.id_produto_categoria',array('categoria_titulo'=>'PC.titulo'))
			->where('P.grupo>0')
			->group('P.grupo')
			->order('PC.titulo ASC')->order('P.titulo ASC');
			$dados = $dbAdapter->fetchAll($select);
			$grupos='';
			foreach($dados as $dado) {
				$grupos[$dado['categoria_titulo']][]=array('grupo'=>$dado['grupo'],'titulo'=>$dado['titulo']);
			}
			if ($grupos=='') $grupos=array();
			$this->view->grupos = $grupos;

			/* PRODUTO */
			$select = $dbAdapter->select()->from(array('P'=>'produto'))->where('P.id_produto='.$id_produto)->order('P.titulo ASC')->limit(1);
			$dados = $dbAdapter->fetchRow($select);
			$this->view->dados = $dados;
			
			/* TESTE VALOR */
			$select_produto_linha = $dbAdapter->select()->from('produto_categoria','id_produto_linha')->where('id_produto_categoria='.$dados['id_produto_categoria']);
			$id_produto_linha = $dbAdapter->fetchOne($select_produto_linha);
			$produto_valor = Porto80_Core::getProdutoValor($id_produto, 'SP');
			
			
			
			
		} catch (Zend_Db_Exception $e) {
        	$this->_helper->FlashMessenger( array('warning' => htmlentities($e->getMessage()) ) );
        	$this->_helper->redirector('pesquisar','admin-produto');
		}
		
		/* MANIPULA URL DE RETORNO */
    	$filtros = new Zend_Session_Namespace('filtros');
    	$keyfiltro = $this->_request->getParam('b');
    	$this->view->keyfiltro = $keyfiltro;
    	if (!empty($keyfiltro) && isset($filtros->$keyfiltro)) {
    		$this->view->url_back=$filtros->$keyfiltro;
    	} else {
    		$this->view->url_back=$this->view->baseUrl('admin-produto/pesquisar');
    	}
    	
    	/* ENVIA SAIDA DE DADOS */
    	$this->_helper->viewRenderer->render('formulario');
    	
    }
    
    public function getProdutoValorAction() {
    	
        $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		
		$id_produto = $this->_request->getParam('id');
		$local = $this->_request->getParam('local');
		
		$produto_valor = Porto80_Core::getProdutoValor($id_produto, $local);
		
		/* CONVERTE PARA FORMATO BR */
		foreach($produto_valor as $key=>$produto) $produto_valor[$key] = Porto80_Core::converteFloat($produto,2);
		
		echo Zend_Json::encode($produto_valor);
    	
    }
    
    public function salvarAction() {
    	
        $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);    	
    	
		if ($this->_request->isPost()) {
			
			$id_produto = $this->_request->getPost('id_produto', 0);
			
			try {
				
				$dbAdapter = Zend_Db_Table::getDefaultAdapter ();
				
				$grupo = $this->getRequest()->getPost('grupo');
				if (!is_numeric($grupo)) { 
					$grupo=0;
				} else if (is_numeric($grupo) && $grupo<0) { //GERAR NOVO GRUPO
					$select = $dbAdapter->select()->from(array('P'=>'produto'),array('MAX(grupo)+1'));
					$grupo = $dbAdapter->fetchOne($select);
				}
				
    			$dados = array(
    				'titulo'=>					$this->getRequest()->getPost('titulo'),
    				'id_produto_categoria'=>	$this->getRequest()->getPost('id_produto_categoria'),
    				'id_produto_embalagem'=>	$this->getRequest()->getPost('id_produto_embalagem'),
	    			'codigo'=>					$this->getRequest()->getPost('codigo'),
	    			'titulo'=>					strtoupper(trim(preg_replace('~\s{2,}~', ' ', $this->getRequest()->getPost('titulo')))),
    				'peso'=>					Porto80_Core::converteFloat( $this->getRequest()->getPost('peso'),2),
	    			'qtde_emb'=>				$this->getRequest()->getPost('qtde_emb'),
	    			'descricao'=>				htmlentities($this->getRequest()->getPost('descricao')),
	    			'foto'=>					$this->getRequest()->getPost('foto'),
	    			'status'=>					$this->getRequest()->getPost('status')
    			);
    			
    			$valid=true;

    			//VERIFICANDO FOTO
    			$newFoto = isset($_FILES['foto'])?$_FILES['foto']['name']:'';
    			if (!empty($newFoto)) {
    				$libFile = new Porto80_Arquivo();
	    			if ($id_produto>0) {
		    			$select = $dbAdapter->select()->from('produto',array('foto'))->where('id_produto='.$id_produto);
		    			$resultadoC = $dbAdapter->fetchRow($select);
	    				$oldFoto = $resultadoC['foto'];
	    				if ($resultadoC['foto']!='') {
		    				$cmdFile = $libFile->isFile($oldFoto,'produtos');
		    				if ($cmdFile['status']==1) {
								$cmd = $libFile->delete($oldFoto,'produtos');
							} else {
								$this->_helper->FlashMessenger( array('error' => htmlentities($cmdFile['mensagem']) ) );
							}
	    				}
	    			}
					//FAZENDO UPLOAD DA FOTO
					$cmd = $libFile->upload('foto','produtos');
		    		if ($cmd['status']==1) {
		    			$newFoto = $cmd['mensagem'];
		    			$redim = $libFile->sizeImg($newFoto,'produtos',250,250);
		    			$dados['foto']=$newFoto;
		    		} else {
		    			$this->_helper->FlashMessenger( array('error' => htmlentities($cmd['mensagem']) ) );
		    		}
    			} else {
    				$delfoto = $this->getRequest()->getPost('delfoto','');
    				if (!empty($delfoto)) {
    					$dados['foto']=''; //LIMPAR FOTO
    					//EXCLUINDO FOTO ATUAL
						$libFile = new Porto80_Arquivo();
		    			$select = $dbAdapter->select()->from('produto',array('foto'))->where('id_produto='.$id_produto);
		    			$resultadoC = $dbAdapter->fetchRow($select);
	    				$oldFoto = $resultadoC['foto'];
	    				if ($resultadoC['foto']!='') {
		    				$cmdFile = $libFile->isFile($oldFoto,'produtos');
		    				if ($cmdFile['status']==1) {
								$cmd = $libFile->delete($oldFoto,'produtos');
							} else {
								$this->_helper->FlashMessenger( array('error' => htmlentities($cmdFile['mensagem']) ) );
							}
	    				}
    				} else {
    					unset($dados['foto']); //N�O H� FOTOS
    				}
    			}
				
    			if ($valid) {
			    	if ($id_produto<=0) {
			    		//INCLUINDO REGISTRO
			    		$insert= $dbAdapter->insert('produto',$dados);
			    		$lastID = $dbAdapter->lastInsertId('produto','id_produto');
			    		$this->_helper->FlashMessenger( array('success' => htmlentities('Registro inclu�do!') ) );
			    	} else {
			    		//ATUALIZANDO REGISTRO
			    		$update = $dbAdapter->update('produto',$dados,'id_produto='.$id_produto);
			    		$this->_helper->FlashMessenger( array('success' => htmlentities('Registro atualizado!') ) );
			    		$lastID = $id_produto;
			    	}
			    	
    		    	//ADICIONANDO EXTRAS
			    	$delete = $dbAdapter->delete('produto_extra','id_produto='.$lastID);
					$extras = $this->_request->getPost('pextra', 0);
					foreach($extras as $extra) {
						$json = json_decode(stripslashes($extra));
						$insert = $dbAdapter->insert('produto_extra',array(
							'id_produto'=>$lastID,
							'local'=>$json->local,
							'desconto'=>Porto80_Core::converteFloat($json->desconto,2),
							'preco'=>Porto80_Core::converteFloat($json->preco,2),
							'iva'=>Porto80_Core::converteFloat($json->iva,2),
							'ipi'=>Porto80_Core::converteFloat($json->ipi,2),
							'icms_origem'=>Porto80_Core::converteFloat($json->icms_origem,2),
							'icms_destino'=>Porto80_Core::converteFloat($json->icms_destino,2),
							'frete'=>Porto80_Core::converteFloat($json->frete,2),
							'margem'=>Porto80_Core::converteFloat($json->margem,2)
						));	
					}
					
    				/* MANIPULA URL DE RETORNO */
			    	$filtros = new Zend_Session_Namespace('filtros');
			    	$keyfiltro = $this->_request->getParam('b');
			    	if (!empty($keyfiltro) && isset($filtros->$keyfiltro)) {
			    		$this->view->url_back=$filtros->$keyfiltro;
			    	} else {
			    		$this->view->url_back=$this->view->baseUrl('admin-produto/pesquisar');
			    	}
		    	
			    	//REDIRECIONA
		    		$this->_redirect('http://' . $this->getRequest()->getHttpHost() . $this->view->url_back);
	    		} else {
	    			if ($id_produto<=0) {
	    				$this->_helper->redirector('novo','admin-produto');
	    			} else {
	    				$this->_helper->redirector('editar','admin-produto',null,array('id'=>$id_produto));
	    			}
	    		}
					
			} catch (Zend_Db_Exception $e) {
				$this->_helper->FlashMessenger( array('error' => htmlentities($e->getMessage()) ) );
				$this->_helper->redirector('pesquisar','admin-produto');
			}
			
		}
    	
    }
    
    public function excluirAction() {
    	
        $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
    	
    	try {
    		
    		$dbAdapter = Zend_Db_Table::getDefaultAdapter ();
    		
        	$id = $this->_request->getParam('id');
    		
	    	if (!$id) {
	    		$this->_helper->FlashMessenger( array('warning' => htmlentities('Aten��o! ID incorreto, tente novamente.') ) );
	    	} else {
	    		
    			$select = $dbAdapter->select()->from('produto',array('foto'))->where('id_produto='.$id);
    			$resultadoC = $dbAdapter->fetchRow($select);
    			
    			//REMOVENDO REGISTROS
	    		$delete = $dbAdapter->delete('produto','id_produto = '.$id);
	    		
	    		//REMOVENDO AVATAR
    			$libFile = new Porto80_Arquivo();
    			$oldFoto = $resultadoC['foto'];
    			$cmdFile = $libFile->isFile($oldFoto,'produtos');
    			if ($cmdFile['status']==1) {
					$cmd = $libFile->delete($oldFoto,'produtos');
				}
	    		//MENSAGEM DE SUCESSO
	    		$this->_helper->FlashMessenger( array('success' => htmlentities('Registro exclu�do!') ) );
	    	}
    			
    	} catch (Zend_Db_Exception $e) {
    		$this->_helper->FlashMessenger( array('warning' => htmlentities( $e->getMessage()) ) );
    	}
		
    	/* MANIPULA URL DE RETORNO */
    	$filtros = new Zend_Session_Namespace('filtros');
    	$keyfiltro = $this->_request->getParam('b');
    	if (!empty($keyfiltro) && isset($filtros->$keyfiltro)) {
    		$this->view->url_back=$filtros->$keyfiltro;
    	} else {
    		$this->view->url_back=$this->view->baseUrl('admin-produto/pesquisar');
    	}
    
    	//REDIRECIONA
    	$this->_redirect('http://' . $this->getRequest()->getHttpHost() . $this->view->url_back);
    	
    }
    
}

