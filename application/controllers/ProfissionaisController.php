<?php

class ProfissionaisController extends Zend_Controller_Action
{

    public function init()
    {

    }

    public function indexAction()
    {
    	$categoria = $this->_request->getParam('categoria');
    	
		//CARREGAR BANCO DE DADOS		
		try {
			
			$dbAdapter = Zend_Db_Table::getDefaultAdapter ();
			
	    	if ($categoria=='') {
	    		//SELECIONAR PRIMEIRA CATEGORIA
				$select = $dbAdapter->select()->from(array('PC'=>'profissional_categoria'),array('perm_link'))->joinInner(array('PCL'=>'profissional_categoria_link'),'PCL.id_profissional_categoria=PC.id_profissional_categoria')->order('PC.titulo ASC')->limit(1);
				$categoria = $dbAdapter->fetchOne($select);
				if ($categoria=='') {
					$this->_helper->FlashMessenger( array('info' => htmlentities( 'N�o h� profissionais cadastrados.' ) ) );
					$this->_helper->redirector('index','index');
				} else {
					$this->_helper->redirector($categoria,'profissionais');
				}
	    	} else {
	    		//VERIFICAR SE EXISTE
    			$select = $dbAdapter->select()->from(array('PC'=>'profissional_categoria'),array('titulo','id_profissional_categoria'))->where('PC.perm_link="'.$categoria.'"')->limit(1);
				$categorias = $dbAdapter->fetchRow($select);
				if ($categoria=='') {
					$this->_helper->FlashMessenger( array('info' => htmlentities( 'A Categoria de Profissional que voc� selecionou � inv�lida ou n�o existe mais.' ) ) );
					$this->_helper->redirector('index','index');
				}
	    	}
	    	
	    	$this->view->categoria = $categorias;
	    	//SELECIONAR PROFISSIONAIS DA CATEGORIA SELECIONADA
	    	
	    	$select = $dbAdapter->select()->from(array('P'=>'profissional'))->joinInner(array('PCL'=>'profissional_categoria_link'),'PCL.id_profissional=P.id_profissional AND PCL.id_profissional_categoria='.$categorias['id_profissional_categoria'])->order('nome ASC');
	    	$dados = $dbAdapter->fetchAll($select);
	    	$this->view->profissionais = $dados;
			
			/* CATEGORIA DE PROFISSIONAIS */
			$select = $dbAdapter->select()->from(array('PC'=>'profissional_categoria'))->joinInner(array('PCL'=>'profissional_categoria_link'),'PCL.id_profissional_categoria=PC.id_profissional_categoria')->order('PC.titulo ASC');
			$dados = $dbAdapter->fetchAll($select);
			$this->view->profissional_categoria = $dados;
				
		} catch (Zend_Db_Exception $e) {
			
			$this->_helper->FlashMessenger( array('warning' => htmlentities( $e->getMessage()) ) );
			$this->_helper->redirector('index','index');
			
		}

    }

}

