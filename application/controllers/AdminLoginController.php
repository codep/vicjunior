<?php

class AdminLoginController extends Zend_Controller_Action
{
	function init() {
		$this->_helper->layout->setLayout('layout_login');
	}

	public function indexAction() {

    	$this->view->headScript()
			->appendFile( $this->view->baseUrl('/js/onload_login.js'),'text/javascript')
		;
	
	}
	
	public function validarAction() {
		
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		
		$retorno = array(
			'mensagem'=>'',
			'status'=>0
		);
		
		if ($this->_request->isPost ()) {
			//ZERAR SESS�O
			Zend_Session::namespaceUnset(MODULE_NAME);
			//TENTAR LOGAR NOVAMENTE
			try {
				Zend_Loader::loadClass ( 'Zend_Filter_StripTags' );
				$filter = new Zend_Filter_StripTags ();
				
				$username = $filter->filter ( $this->_request->getPost ( 'username' ) );
				$password = $filter->filter ( $this->_request->getPost ( 'password' ) );
				
				if (! empty ( $username ) && ! empty ( $password )) {
					
					$dbAdapter = Zend_Db_Table::getDefaultAdapter ();
										
					Zend_Loader::loadClass ( 'Zend_Auth_Adapter_DbTable' );
					$authAdapter = new Zend_Auth_Adapter_DbTable ( $dbAdapter );
					$authAdapter->setTableName ( 'usuario' )->setIdentityColumn ( 'usuario' )->setCredentialColumn ( 'senha' )->setCredentialTreatment("SHA1(?)") ->setIdentity ( $username )->setCredential ( $password );
					
					//Configura o Zend_Auth
					$auth = Zend_Auth::getInstance();
					
					try {
						$auth->setStorage(new Zend_Auth_Storage_Session(MODULE_NAME));
						$result = $auth->authenticate($authAdapter);
					} catch (Zend_Exception $e) {
						$retorno['mensagem']=$e->getMessage();
					}

					$dbAdapter->closeConnection ();
					
					switch ($result->getCode()) {
						case Zend_Auth_Result::SUCCESS :
							$data = $authAdapter->getResultRowObject ( null, "senha" );
							if ( $data->role=='admin' ) {
								$auth->getStorage()->write( $data );
								$retorno['status']=1;
							} else {
								Zend_Registry::_unsetInstance();
								Zend_Session::namespaceUnset(MODULE_NAME);
								$retorno['mensagem']='Voc� n�o tem n�vel de acesso suficiente.';
							}	
							break;
						case Zend_Auth_Result::FAILURE_IDENTITY_NOT_FOUND :
						case Zend_Auth_Result::FAILURE_CREDENTIAL_INVALID :
							$retorno['mensagem']='Usu�rio ou Senha inv�lidos';
							break;
					}
				
				} else {
					$retorno['mensagem']='Preencha corretamente todos os campos!';
				}
			} catch ( Zend_Db_Adapter_Exception $e ) {
				$retorno['mensagem']=$e->getMessage();
			} catch ( Zend_Exception $e ) {
				$retorno['mensagem']=$e->getMessage();
			}
		} else {
			$retorno['mensagem'] = 'Envio de dados de forma inv�lida';
		}
		
		$retorno['mensagem'] = rawurlencode($retorno['mensagem']);
		echo json_encode($retorno);
		
	} 
	
	public function logoutAction() {
		//REMOVE REGISTRO / SESS�O E ENVIA PARA TELA DE LOGIN
		Zend_Registry::_unsetInstance();
		Zend_Session::namespaceUnset(MODULE_NAME);
		$this->_helper->redirector('index','index');
	}


}




