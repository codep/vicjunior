<?php

class AdminConfiguracaoController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
    	
    	$this->view->headScript()
    		->appendFile( $this->view->baseUrl('/admin_js/jquery-custom-file-input.js'),'text/javascript')
			->appendFile( $this->view->baseUrl('/admin_js/jquery.validate_pack.js'),'text/javascript')
			->appendFile( $this->view->baseUrl('/admin_js/tiny_mce/jquery.tinymce.js'),'text/javascript')
			->appendFile( $this->view->baseUrl('/admin_js/jquery.maskedinput-1.2.2.min.js'),'text/javascript')
			->appendFile( $this->view->baseUrl('/admin_js/jquery.tipTip.min.js'),'text/javascript')
		;
    	
    	//CARREGAR BANCO DE DADOS		
		try {
			
			$dbAdapter = Zend_Db_Table::getDefaultAdapter ();
			
			$select = $dbAdapter->select()->from(array('P'=>'parametros'))->where('status=1')->limit(1);			
			$dados = $dbAdapter->fetchRow($select);
			$this->view->dados = $dados;
				
		} catch (Zend_Db_Exception $e) {
			
			$this->_helper->FlashMessenger( array('warning' => htmlentities( $e->getMessage()) ) );
			$this->_helper->redirector('index','admin-resumo');
			
		}
    	
    	$this->_helper->viewRenderer->render('formulario');
    }
    
    public function salvarAction() {
    	
        $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);    	
    	
		if ($this->_request->isPost()) {
			
			$id_parametro = $this->_request->getPost('id_parametro', 0);
			
			try {
				
				$dbAdapter = Zend_Db_Table::getDefaultAdapter ();
				
    			$dados = array(
    				'desconto_vista'=>			floatval(str_replace(',','.',$this->getRequest()->getPost('desconto_vista'))),
    				'desconto_prazo'=>			floatval(str_replace(',','.',$this->getRequest()->getPost('desconto_prazo'))),
    				'param_telefone'=>			$this->getRequest()->getPost('param_telefone'),
    				'param_fax'=>				$this->getRequest()->getPost('param_fax'),    			
    				'param_email_publico'=>		trim($this->getRequest()->getPost('param_email_publico')),
    				'param_email_contato'=>		trim($this->getRequest()->getPost('param_email_contato')),
    				'param_endereco'=>			htmlentities($this->getRequest()->getPost('param_endereco')),
    				'param_mapa_google'=>		$this->getRequest()->getPost('param_mapa_google'),
    				'param_uid_msn'=>			$this->getRequest()->getPost('param_uid_msn'),
    				'home_frase_topo'=>			htmlentities($this->getRequest()->getPost('home_frase_topo')),
					'home_horario_func'=>		htmlentities($this->getRequest()->getPost('home_horario_func')),
    				'home_b1_titulo'=>			htmlentities($this->getRequest()->getPost('home_b1_titulo')),
    				'home_b1_subtitulo'=>		htmlentities($this->getRequest()->getPost('home_b1_subtitulo')),
    				'home_b1_texto'=>			htmlentities($this->getRequest()->getPost('home_b1_texto')),
    				'home_b2_titulo'=>			htmlentities($this->getRequest()->getPost('home_b2_titulo')),
    				'home_b2_subtitulo'=>		htmlentities($this->getRequest()->getPost('home_b2_subtitulo')),
    				'home_b2_texto'=>			htmlentities($this->getRequest()->getPost('home_b2_texto')),
    				'home_b3_titulo'=>			htmlentities($this->getRequest()->getPost('home_b3_titulo')),
    				'home_b3_subtitulo'=>		htmlentities($this->getRequest()->getPost('home_b3_subtitulo')),
    				'status'=>1
    			);

		    	if ($id_parametro<=0) {
		    		/*
		    		//INCLUINDO REGISTRO
		    		$insert= $dbAdapter->insert('parametros',$dados);
		    		$lastID = $dbAdapter->lastInsertId('parametros','id_parametro');
		    		$this->_helper->FlashMessenger( array('success' => htmlentities('Registro inclu�do!') ) );
		    		*/
					$this->_helper->FlashMessenger( array('error' => htmlentities('Nao e possivel incluir um novo registro de parametro!') ) );
					$this->_helper->redirector('index','admin-resumo');
		    	} else {
		    		//ATUALIZANDO REGISTRO
		    		$update = $dbAdapter->update('parametros',$dados,'id_parametro='.$id_parametro);
		    		$this->_helper->FlashMessenger( array('success' => htmlentities('Registro atualizado!') ) );
		    		$lastID = $id_parametro;
		    	}
		    	
		    	$this->_helper->redirector('index','admin-configuracao');
					
			} catch (Zend_Db_Exception $e) {
				$this->_helper->FlashMessenger( array('error' => htmlentities($e->getMessage()) ) );
				$this->_helper->redirector('index','admin-resumo');
			}
			
		}
    	
    }


}

