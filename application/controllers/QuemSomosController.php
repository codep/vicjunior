<?php

class QuemSomosController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
        try {
    		
    		$dbAdapter = Zend_Db_Table::getDefaultAdapter ();
    		
			//SELECIONAR AS REVENDAS
	        $select = $dbAdapter->select()->from(array('PR'=>'produto_revenda'))->joinInner(array('PL'=>'produto_linha'),'PR.id_produto_revenda=PL.id_produto_revenda',array())->joinInner(array('PC'=>'produto_categoria'),'PL.id_produto_linha=PC.id_produto_linha',array())->joinInner(array('P'=>'produto'),'PC.id_produto_categoria=P.id_produto_categoria',array())->group('PL.id_produto_revenda')->order('PR.ordem ASC');
	    	$this->view->revendas = $dbAdapter->fetchAll($select);
    		
    	} catch (Zend_Db_Exception $e) {
			
			$this->_helper->FlashMessenger( array('warning' => htmlentities( $e->getMessage()) ) );
			$this->_helper->redirector('index','index');
			
		}
    }


}

