<?php

class AdminProfissionalCategoriaController extends Zend_Controller_Action
{

    public function init()
    {
    }

    public function indexAction()
    {
		$this->_helper->redirector('pesquisar','admin-profissional-categoria');
    }
    
    public function pesquisarAction() {
    	
        $pagina = $this->_request->getParam('pagina', 1) ;
        
        try {
        	
			$dbAdapter = Zend_Db_Table::getDefaultAdapter ();
	        
			/* FILTRO */
	        $key = $this->getRequest()->getPost('key');
	        $this->view->key = $key;
	        
	        $select = $dbAdapter->select()->from(array('PC'=>'profissional_categoria'),array('PC.*','total_categorias'=>'COUNT(PCL.id_profissional_categoria)'))->joinLeft(array('PCL'=>'profissional_categoria_link'),'PCL.id_profissional_categoria=PC.id_profissional_categoria',array())->order('PC.titulo ASC')->group('PC.id_profissional_categoria');
	        
	        if ($key!='') {
	        	$select->where('PC.titulo LIKE "%'.$key.'%"');
	        }
	        
	        $result = $dbAdapter->fetchAll($select);
	    	
		    /* PAGINA��O */
		    $dados = Zend_Paginator::factory($result);
		    $dados->setCurrentPageNumber( intval($pagina) );
		    $dados->setItemCountPerPage(20);
	        $this->view->dados = $dados;
        	
        	
        } catch (Zend_Db_Exception $e) {
        	$this->_helper->FlashMessenger( array('warning' => htmlentities($e->getMessage()) ) );
        	$this->_helper->redirector('index','admin-resumo');
        }
    	
    	
    }

    public function novoAction()
    {
    	$this->view->headScript()
			->appendFile( $this->view->baseUrl('/admin_js/jquery.validate_pack.js'),'text/javascript')
		;
		
		$this->_helper->viewRenderer->render('formulario');
		
    }
    
    public function editarAction() {
    	
		$id = $this->_request->getParam('id');
		
    	if (!$id) {
    		$this->_helper->FlashMessenger( array('warning' => htmlentities('O registro informado n�o existe. Verifique e tente novamente.') ) );
    		$this->_helper->redirector('pesquisar','admin-profissional-categoria');
    	}
		
    	$this->view->headScript()
			->appendFile( $this->view->baseUrl('/admin_js/jquery.validate_pack.js'),'text/javascript')
		;
		
    	//CARREGAR BANCO DE DADOS		
		try {
			
			$dbAdapter = Zend_Db_Table::getDefaultAdapter ();
			
			$select = $dbAdapter->select()->from(array('PC'=>'profissional_categoria'))->where('id_profissional_categoria='.$id)->limit(1);			
			$dados = $dbAdapter->fetchRow($select);
			$this->view->dados = $dados;
				
		} catch (Zend_Db_Exception $e) {
			
			$this->_helper->FlashMessenger( array('warning' => htmlentities( $e->getMessage()) ) );
			$this->_helper->redirector('pesquisar','admin-profissional-categoria');
			
		}
    	
    	$this->_helper->viewRenderer->render('formulario');
    }
    
    public function salvarAction() {
    	
        $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);    	
    	
		if ($this->_request->isPost()) {
			
			$id_profissional_categoria = $this->_request->getPost('id_profissional_categoria', 0);
			
			try {
				
				$dbAdapter = Zend_Db_Table::getDefaultAdapter ();
				
    			$dados = array(
    				'titulo'=>			$this->getRequest()->getPost('titulo'),
    				'perm_link'=>		Porto80_Core::url_amigavel($this->getRequest()->getPost('titulo'))
    			);
    			
		    	if ($id_profissional_categoria<=0) {
		    		//INCLUINDO REGISTRO
		    		$insert= $dbAdapter->insert('profissional_categoria',$dados);
		    		$lastID = $dbAdapter->lastInsertId('profissional_categoria','id_profissional_categoria');
		    		$this->_helper->FlashMessenger( array('success' => htmlentities('Registro inclu�do!') ) );
		    	} else {
		    		//ATUALIZANDO REGISTRO
		    		$update = $dbAdapter->update('profissional_categoria',$dados,'id_profissional_categoria='.$id_profissional_categoria);
		    		$this->_helper->FlashMessenger( array('success' => htmlentities('Registro atualizado!') ) );
		    		$lastID = $id_profissional_categoria;
		    	}
		    	
		    	$this->_helper->redirector('pesquisar','admin-profissional-categoria');
					
			} catch (Zend_Db_Exception $e) {
				$this->_helper->FlashMessenger( array('error' => htmlentities($e->getMessage()) ) );
				$this->_helper->redirector('pesquisar','admin-profissional-categoria');
			}
			
		}
    	
    }
    
    public function excluirAction() {
    	
        $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
    	
    	try {
    		
    		$dbAdapter = Zend_Db_Table::getDefaultAdapter ();
    		
        	$id = $this->_request->getParam('id');
    		
	    	if (!$id) {
	    		$this->_helper->FlashMessenger( array('warning' => htmlentities('Aten��o! ID incorreto, tente novamente.') ) );
	    	} else {	    		
    			//REMOVENDO REGISTROS
	    		$delete = $dbAdapter->delete('profissional_categoria','id_profissional_categoria = '.$id);
	    		//MENSAGEM DE SUCESSO
	    		$this->_helper->FlashMessenger( array('success' => htmlentities('Registro exclu�do!') ) );
	    	}
    			
    	} catch (Zend_Db_Exception $e) {
    		$this->_helper->FlashMessenger( array('warning' => htmlentities( $e->getMessage()) ) );
    	}
		
    	$this->_helper->redirector('pesquisar','admin-profissional-categoria');
    	
    }

}

