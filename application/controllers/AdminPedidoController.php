<?php

class AdminPedidoController extends Zend_Controller_Action
{

    public function init()
    {
    }

    public function indexAction()
    {
		$this->_helper->redirector('pesquisar','admin-pedido');
    }

    public function pesquisarAction() {

        $pagina = $this->_request->getParam('pagina', 1) ;

        //PRECISO PARA ABRIR VISUALIZAR
    	$this->view->headScript()
			->appendFile( $this->view->baseUrl('/admin_js/tiny_mce/jquery.tinymce.js'),'text/javascript')
			->appendFile( $this->view->baseUrl('/admin_js/jquery.maskedinput-1.2.2.min.js'),'text/javascript')
		;

        try {

			$dbAdapter = Zend_Db_Table::getDefaultAdapter ();

			/* FILTRO */
	        $id_cliente = $this->getRequest()->getParam('cliente');
	        $this->view->id_cliente = $id_cliente;

	        $arquivado = $this->_request->getParam('arquivado');
	        $this->view->arquivado = $arquivado;
	        $status = $this->_request->getParam('status');
	        $this->view->status = $status;
	    	$mes = $this->_request->getParam('mes');
	    	$ano = $this->_request->getParam('ano');
	    	$this->view->ano = $ano;
	    	$this->view->mes = $mes;
	    	$id_produto_revenda = $this->getRequest()->getParam('revenda');
	    	$this->view->id_produto_revenda = $id_produto_revenda;

			$select = $dbAdapter->select()->from(array('P'=>'pedido'),array('ano'=>'YEAR(datahora)'))->group('ano');
			$this->view->anos_validos = $dbAdapter->fetchAll($select);

	        $select = $dbAdapter->select()->from(array('P'=>'pedido'),array('P.*','total_qtd'=>'(SELECT SUM(PP.quantidade) FROM pedido_produto PP WHERE PP.id_pedido=P.id_pedido)'))
	        ->joinLeft(array('U'=>'usuario'),'U.id_usuario=P.id_usuario',array('usuario_nome'=>'U.usuario'))
	        ->joinLeft(array('C'=>'cliente'),'C.id_cliente=U.id_cliente',array('cliente_nome'=>'C.razao_social'))
	        ->order('P.status ASC')->order('P.datahora DESC');

	        if (!empty($id_cliente)) {
	        	$select->where('C.id_cliente='.$id_cliente);
	        }

	        if (!empty($mes) || !empty($ano)) {

	    		if (empty($mes)) $mes = date('n');
	    		if (empty($ano)) $ano = date('Y');

		    	$dateDB_ini = date('Y-m-d', mktime(0,0,0,$mes,1,$ano));
		    	$dateDB_fim = date('Y-m-d', mktime(24,59,59,($mes+1),0,$ano));

	        	$select->where('P.datahora>="'.$dateDB_ini.'" AND P.datahora<="'.$dateDB_fim.'"');

	        }

	        /* REVENDA */
	        if (!empty($id_produto_revenda)) {
	        	$select->where('P.id_produto_revenda='.$id_produto_revenda);
	        }

	        if (!empty($status)) {
	        	$select->where('P.status="'.$status.'"');
	        }

	        if (empty($arquivado)) {
	        	$select->where('P.arquivado=0');
	        }

	        $result = $dbAdapter->fetchAll($select);

		    /* PAGINACAO */
		    $dados = Zend_Paginator::factory($result);
		    $dados->setCurrentPageNumber( intval($pagina) );
		    $dados->setItemCountPerPage(20);
	        $this->view->dados = $dados;

			/* CLIENTES */
			$select = $dbAdapter->select()->from(array('C'=>'cliente'),array('id_cliente','nome','razao_social'))
			->joinLeft(array('CR'=>'cliente_revenda'),'CR.id_cliente=C.id_cliente',array('id_produto_revenda'))
			->order('C.razao_social ASC');
			/* REVENDA */
			if (!empty($id_produto_revenda)) {
				$select->where('CR.id_produto_revenda='.$id_produto_revenda);
			}
			$this->view->clientes = $dbAdapter->fetchAll($select);

			/* REVENDAS */
			$select  = $dbAdapter->select()->from(array('PR'=>'produto_revenda'))->order('PR.titulo ASC');
			$this->view->revendas = $dbAdapter->fetchAll($select);

        } catch (Zend_Db_Exception $e) {
        	$this->_helper->FlashMessenger( array('warning' => htmlentities($e->getMessage()) ) );
        	$this->_helper->redirector('index','admin-resumo');
        }

        /* ARMAZENA A URL DE PESQUISA */
    	$filtros = new Zend_Session_Namespace('filtros');
    	$keyfiltro = md5($this->_request->getControllerName().'_'.$this->_request->getActionName());
		$filtros->$keyfiltro = $this->view->url();
		$this->view->keyfiltro = $keyfiltro;

    }

    public function editarAction() {

		$id = $this->_getParam('id');
		$this->view->id_pedido = $id;

    	if (!$id) {
    		$this->_helper->FlashMessenger( array('warning' => htmlentities('O registro informado n�o existe. Verifique e tente novamente.') ) );
    		$this->_helper->redirector('pesquisar','admin-pedido');
    	}

    	$this->view->headScript()
    		->appendFile( $this->view->baseUrl('/admin_js/jquery-custom-file-input.js'),'text/javascript')
			->appendFile( $this->view->baseUrl('/admin_js/jquery.validate_pack.js'),'text/javascript')
			->appendFile( $this->view->baseUrl('/admin_js/tiny_mce/jquery.tinymce.js'),'text/javascript')
			->appendFile( $this->view->baseUrl('/admin_js/jquery.maskedinput-1.2.2.min.js'),'text/javascript')
			->appendFile( $this->view->baseUrl('/admin_js/jquery.tipTip.min.js'),'text/javascript')
		;

    	//CARREGAR BANCO DE DADOS
		try {

			$dbAdapter = Zend_Db_Table::getDefaultAdapter ();

			/*
			//ADICIONANDO PRODUTOS
			$produtos = (isset($_POST['prod_id_produto'])?$_POST['prod_id_produto']:'');
			if (!empty($produtos)) {
				$delete = $dbAdapter->delete('pedido_produto','id_pedido='.$id);
				$prod_quantidade = (isset($_POST['prod_qtdProd'])?$_POST['prod_qtdProd']:'');
				$prod_valor_unitario = (isset($_POST['prod_valor_final'])?$_POST['prod_valor_final']:'');
				$prod_valor_liquido = (isset($_POST['prod_valor_liquido'])?$_POST['prod_valor_liquido']:'');
				foreach($produtos as $i=>$produto) {
					$dadosProd = array(
							'id_pedido'=>$id,
							'id_produto'=>(isset($produto)?$produto:0),
							'quantidade'=>(isset($prod_quantidade[$i])?$prod_quantidade[$i]:0),
							'valor_unitario'=>(isset($prod_valor_unitario[$i])?$prod_valor_unitario[$i]:0),
							'valor_liquido'=>(isset($prod_valor_liquido[$i])?Porto80_Core::converteFloat($prod_valor_liquido[$i],2):0)
					);
					$insert = $dbAdapter->insert('pedido_produto',$dadosProd);
				}
			}
			*/

			$select = $dbAdapter->select()->from(array('P'=>'pedido'))
			->joinLeft(array('U'=>'usuario'),'U.id_usuario=P.id_usuario',array('U.id_cliente'))
			->joinLeft(array('C'=>'cliente'),'C.id_cliente=U.id_cliente',array('cliente_obs'=>'C.observacao','cliente_nome'=>'C.razao_social','cliente_cidade'=>'CONCAT(C.cidade,"-",C.estado)'))
			->joinLeft(array('CR'=>'cliente_revenda'),'CR.id_cliente=C.id_cliente AND CR.id_produto_revenda=P.id_produto_revenda',array('cliente_codigo'=>'CR.codigo'))
			->where('P.id_pedido='.$id)
			->limit(1);
			$dadosPedido = $dbAdapter->fetchRow($select);
			$this->view->pedido = $dadosPedido;

			/* PRODUTOS */
			$select = $dbAdapter->select()->from(array('PP'=>'pedido_produto'))
			->joinLeft(array('P'=>'produto'),'P.id_produto=PP.id_produto',array('titulo','codigo','qtde_emb','peso','foto'))
			->joinLeft(array('PE'=>'produto_embalagem'),'PE.id_produto_embalagem=P.id_produto_embalagem',array('embalagem'=>'PE.titulo'))
			->where('PP.id_pedido='.$id)
			->order('P.produto_ordem ASC')
			->order('PE.ordem ASC');
			$dadosProduto = $dbAdapter->fetchAll($select);

			$produtos='';
			$totais=array(
				'quantidade'=>0,
				'liquido'=>0,
				'ajustado'=>0,
				'unitario'=>0,
				'final'=>0,
				'peso'=>0,
				'frete'=>0,
				'baseicms'=>0,
				'icms_origem'=>0,
				'icms_destino'=>0,
				'basesubtrib'=>0,
				'subtrib'=>0,
				'ipi'=>0,
				'desconto'=>0
			);
			$local = empty($dadosPedido['local'])?$this->view->auth->local_padrao:$dadosPedido['local'];
			$desconto = (empty($dadosPedido['tipo_pag'])?'PRAZO':$dadosPedido['tipo_pag']);

    		foreach($dadosProduto as $i=>$produto) {

    			$produto_valor = Porto80_Core::getProdutoValor($produto['id_produto'], $local, $dadosPedido['id_cliente'], $desconto,$id);

    			//INFORMACOES SOBRE O PRODUTO
    			$produtos[$i] = array(
    				'id_produto'=>$produto['id_produto'],
    				'codigo'=>$produto['codigo'],
    				'titulo'=>$produto['titulo'],
    				'embalagem'=>$produto['embalagem'],
    				'qtde_emb'=>$produto['qtde_emb'],
    				'quantidade'=>$produto['quantidade'],
					'valor_ajustado'=>$produto_valor['ajustado'],
    				'valor_liquido'=>$produto_valor['liquido'],
    				'valor_final'=>$produto_valor['final'],
    				'total_liquido'=>$produto['quantidade']*$produto_valor['liquido'],
    				'total_final'=>$produto['quantidade']*$produto_valor['final']
    			);
    			/* //DEBUG
    			echo '<pre>';
    			print_r(array_merge($produtos[$i],$produto_valor));
    			echo '<pre>'; //die();
    			*/


    			//INFORMACOES SOBRE OS TOTAIS
    			$totais['quantidade'] = round($totais['quantidade'] + $produto['quantidade'],2);
    			$totais['peso'] = round($totais['peso'] + floatval($produto['quantidade']*$produto['peso']),2);
    			$totais['frete'] = round($totais['frete'] + floatval($produto['quantidade']*$produto_valor['frete']),2);
    			$totais['baseicms'] = round($totais['baseicms'] + floatval($produto['quantidade']*$produto_valor['baseicms']),2);
    			$totais['icms_origem'] = round($totais['icms_origem'] + floatval($produto['quantidade']*$produto_valor['icms_origem']),2);
    			$totais['icms_destino'] = round($totais['icms_destino'] + floatval($produto['quantidade']*$produto_valor['icms_destino']),2);
    			$totais['basesubtrib'] = round($totais['basesubtrib'] + floatval($produto['quantidade']*$produto_valor['basesubtrib']),2);
    			$totais['subtrib'] = round($totais['subtrib'] + floatval($produto['quantidade']*$produto_valor['subtrib']),2);
    			$totais['ipi'] = round($totais['ipi'] + floatval($produto['quantidade']*$produto_valor['ipi']),2);
    			$totais['desconto'] = round($totais['desconto'] + floatval($produto['quantidade']*$produto_valor['desconto']),2);
    			$totais['ajustado'] = round($totais['ajustado'] + $produtos[$i]['total_ajustado'],2);
    			$totais['liquido'] = round($totais['liquido'] + $produtos[$i]['total_liquido'],2);
    			$totais['final'] = round($totais['final'] + $produtos[$i]['total_final'],2);

    		}

    		/* //DEBUG
    		echo '<pre>';
    		print_r($totais);
    		echo '<pre>';
    		die();
    		*/

			$this->view->pedido_produtos = $produtos;
			$this->view->pedido_totais = $totais;

			/* CATEGORIAS */
			$select = $dbAdapter->select()->from(array('PC'=>'produto_categoria'),array('categoria'=>'PC.titulo','PC.id_produto_categoria'))
			->joinInner(array('PG'=>'produto_linha'),'PG.id_produto_linha=PC.id_produto_linha',array('grupo'=>'PG.titulo'))
			->joinInner(array('PR'=>'produto_revenda'),'PR.id_produto_revenda=PG.id_produto_revenda',array('revenda'=>'PR.titulo'))
			->where('PR.id_produto_revenda="'.$dadosPedido['id_produto_revenda'].'"')
			->order('PR.titulo ASC')->order('PG.titulo ASC')->order('PC.titulo ASC');
			$dados = $dbAdapter->fetchAll($select);
			$categorias='';
			foreach($dados as $dado) {
				$categorias[$dado['grupo'].' ['.$dado['revenda'].']'][]=array('id_produto_categoria'=>$dado['id_produto_categoria'],'titulo'=>$dado['categoria']);
			}
			if ($categorias=='') $categorias=array();
			$this->view->categorias = $categorias;

			/* CATEGORIAS
			$select = $dbAdapter->select()->from(array('PC'=>'produto_categoria'),array('categoria'=>'PC.titulo','PC.id_produto_categoria'))
			->joinInner(array('PG'=>'produto_linha'),'PG.id_produto_linha=PC.id_produto_linha',array('grupo'=>'PG.titulo'))->order('PG.titulo ASC')->order('PC.titulo ASC');
			$dados = $dbAdapter->fetchAll($select);
			$categorias='';
			foreach($dados as $dado) {
				$categorias[$dado['grupo']][]=array('id_produto_categoria'=>$dado['id_produto_categoria'],'titulo'=>$dado['categoria']);
			}
			if ($categorias=='') $categorias=array();
			$this->view->categorias = $categorias;
			*/

		} catch (Zend_Db_Exception $e) {

			$this->_helper->FlashMessenger( array('warning' => htmlentities( $e->getMessage()) ) );
			$this->_helper->redirector('pesquisar','admin-pedido');

		}

		/* MANIPULA URL DE RETORNO */
    	$filtros = new Zend_Session_Namespace('filtros');
    	$keyfiltro = $this->_request->getParam('b');
    	$this->view->keyfiltro = $keyfiltro;
    	if (!empty($keyfiltro) && isset($filtros->$keyfiltro)) {
    		$this->view->url_back=$filtros->$keyfiltro;
    	} else {
    		$this->view->url_back=$this->view->baseUrl('admin-pedido/pesquisar');
    	}

    	$this->_helper->viewRenderer->render('formulario');
    }

    public function salvarAction() {

        $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);

		if ($this->_request->isPost()) {

			$id_pedido = $this->_request->getPost('id_pedido', 0);

			try {

				$dbAdapter = Zend_Db_Table::getDefaultAdapter ();

    			$dados = array(
    				'id_pedido'=>		$this->getRequest()->getPost('id_pedido'),
    				'id_usuario'=>		$this->getRequest()->getPost('id_usuario'),
    				'codigo_pedido'=>	$this->getRequest()->getPost('codigo_pedido',0),
	    			'peso'=>			$this->getRequest()->getPost('peso',0),
	    			'frete'=>			$this->getRequest()->getPost('frete',0),
	    			'baseicms'=>		$this->getRequest()->getPost('baseicms',0),
	    			'icms_origem'=>		$this->getRequest()->getPost('icms_origem',0),
					'icms_destino'=>	$this->getRequest()->getPost('icms_destino',0),
	    			'basesubtrib'=>		$this->getRequest()->getPost('basesubtrib',0),
	    			'subtrib'=>			$this->getRequest()->getPost('subtrib',0),
	    			'ipi'=>				$this->getRequest()->getPost('ipi',0),
	    			'total_desc'=>		$this->getRequest()->getPost('total_desc',0),
    				'total_liquido'=>	$this->getRequest()->getPost('total_liquido',0),
	    			'total_final'=>		$this->getRequest()->getPost('total_final',0),
    				'total_desc'=>		$this->getRequest()->getPost('total_desc',0),
    				'tipo_pag'=>		$this->getRequest()->getPost('tipo_pag'),
    				'local'=>			$this->getRequest()->getPost('tipo_local'),
	    			'cond_pag'=>		$this->getRequest()->getPost('cond_pag'),
	    			'status'=>			$this->getRequest()->getPost('status',1),
    				'dt_faturamento'=>	Porto80_Core::formatDbData($this->getRequest()->getPost('dt_faturamento')),
	    			'observacoes'=>		htmlentities($this->getRequest()->getPost('observacoes'))
    			);

	    		//ATUALIZANDO REGISTRO
	    		$update = $dbAdapter->update('pedido',$dados,'id_pedido='.$id_pedido);
	    		$this->_helper->FlashMessenger( array('success' => htmlentities('Registro atualizado!') ) );

		    	//ADICIONANDO PRODUTOS
		    	$delete = $dbAdapter->delete('pedido_produto','id_pedido='.$id_pedido);
		    	$produtos = (isset($_POST['prod_id_produto'])?$_POST['prod_id_produto']:'');
		    	if (!empty($produtos)) {
					$prod_quantidade = (isset($_POST['prod_qtdProd'])?$_POST['prod_qtdProd']:'');
					$prod_valor_unitario = (isset($_POST['prod_valor_final'])?$_POST['prod_valor_final']:'');
					$prod_valor_liquido = (isset($_POST['prod_valor_liquido'])?$_POST['prod_valor_liquido']:'');
					$prod_valor_ajustado = (isset($_POST['prod_valor_ajustado'])?$_POST['prod_valor_ajustado']:'');
					foreach($produtos as $i=>$produto) {
						$vUni = is_numeric($prod_valor_unitario[$i])?$prod_valor_unitario[$i]:Porto80_Core::converteFloat($prod_valor_unitario[$i],2);
						$vLiq = is_numeric($prod_valor_liquido[$i])?$prod_valor_liquido[$i]:Porto80_Core::converteFloat($prod_valor_liquido[$i],2);
						$vAju = is_numeric($prod_valor_ajustado[$i])?$prod_valor_ajustado[$i]:Porto80_Core::converteFloat($prod_valor_ajustado[$i],2);
						$dadosProd = array(
							'id_pedido'=>$id_pedido,
							'id_produto'=>(isset($produto)?$produto:0),
							'quantidade'=>(isset($prod_quantidade[$i])?$prod_quantidade[$i]:0),
							'valor_unitario'=>$vUni,
							'valor_ajustado'=>$vAju,
							'valor_liquido'=>$vLiq
						);
						$insert = $dbAdapter->insert('pedido_produto',$dadosProd);
					}
				}

				//SE O STATUS FOR CONCLUIDO, ENVIAR E-MAIL PARA O CLIENTE E PARA O VICJUNIOR
				$status = intval($this->getRequest()->getPost('status'));
				switch($status) {
					case 2: Porto80_Core::enviarEmailPedido($id_pedido,2); break; //CANCELADO
					case 3: Porto80_Core::enviarEmailPedido($id_pedido,3); break; //CONCLUIDO
					default: break;
				}

				$rback = intval($this->getRequest()->getPost('rback',0));
				if ($rback==1) {
					$keyfiltro = $this->_request->getParam('b');
		    		$this->_helper->redirector('editar','admin-pedido',null,array('id'=>$id_pedido,'b'=>$keyfiltro));
				} else {
					$this->_helper->redirector('pesquisar','admin-pedido');
				}

			} catch (Zend_Db_Exception $e) {
				$this->_helper->FlashMessenger( array('error' => htmlentities($e->getMessage()) ) );
				$this->_helper->redirector('pesquisar','admin-pedido');
			}

		}

    }

    public function removerAction() {

    	$this->_helper->layout->disableLayout();
    	$this->_helper->viewRenderer->setNoRender(TRUE);

    	try {

    		$dbAdapter = Zend_Db_Table::getDefaultAdapter ();

    		$id = $this->_request->getParam('id');

    		if (!$id) {
    			$this->_helper->FlashMessenger( array('warning' => htmlentities('Aten&ccedil;&atilde;o! ID incorreto, tente novamente.') ) );
    		} else {

    			//REMOVENDO PRODUTOS
    			$delete = $dbAdapter->delete('pedido_produto','id_pedido = '.$id);

    			//REMOVENDO REGISTROS
    			$delete = $dbAdapter->delete('pedido','id_pedido = '.$id);

    			//MENSAGEM DE SUCESSO
    			$this->_helper->FlashMessenger( array('success' => htmlentities('Registro exclu?o!') ) );
    		}

    	} catch (Zend_Db_Exception $e) {
    		$this->_helper->FlashMessenger( array('warning' => htmlentities( $e->getMessage()) ) );
    	}

    	$this->_helper->redirector('pesquisar','admin-pedido');

    }

    public function adicionarProdutoAction() {

    	$keyfiltro = $this->_request->getParam('b');
    	$id_pedido = $this->_request->getParam('id');

		if ($this->_request->isPost()) {

	    	try {

	    		$dbAdapter = Zend_Db_Table::getDefaultAdapter ();

	    		$id_produto = $this->_request->getPost('id_produto');
	    		$quantidade = $this->_request->getPost('quantidade');
	    		$valor = Porto80_Core::converteFloat($this->_request->getPost('valor'),2);
	    		$valor_liquido = Porto80_Core::converteFloat($this->_request->getPost('liquido'),2);

		    	if (empty($id_pedido) || empty($id_produto) || empty($quantidade) || empty($valor) || empty($valor_liquido)) {
		    		$this->_helper->FlashMessenger( array('error' => htmlentities('Valores incorretos, n�o foi poss�vel adicionar o produto ao pedido, tente novamente.') ) );
		    	} else {

					$insert = $dbAdapter->insert('pedido_produto',array(
						'id_pedido'=>$id_pedido,
						'id_produto'=>$id_produto,
						'quantidade'=>$quantidade,
						'valor_unitario'=>$valor,
						'valor_liquido'=>$valor_liquido
					));

		    		//MENSAGEM DE SUCESSO
		    		$this->_helper->FlashMessenger( array('success' => htmlentities('Produto adicionado ao Pedido!') ) );
		    	}

	    	} catch (Zend_Db_Exception $e) {
	    		$this->_helper->FlashMessenger( array('warning' => htmlentities( $e->getMessage()) ) );
	    	}

		}

    	$this->_helper->redirector('editar','admin-pedido',null,array('id'=>$id_pedido,'b'=>$keyfiltro));

    }

    public function salvarProdutoAction() {

		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);

		if ($this->_request->isPost()) {

	    	try {

	    		$dbAdapter = Zend_Db_Table::getDefaultAdapter ();

		    	$id_pedido = $this->_request->getParam('id_pedido');
	    		$id_produto = $this->_request->getPost('id_produto');
	    		$quantidade = $this->_request->getPost('quantidade');
	    		$valor = Porto80_Core::converteFloat($this->_request->getPost('valor'),2);
	    		$valor_liquido = Porto80_Core::converteFloat($this->_request->getPost('liquido'),2);

		    	if (empty($id_pedido) || empty($id_produto) || empty($quantidade) || empty($valor) || empty($valor_liquido)) {
		    		$retorno = array('status' => 'Valores incorretos, n�o foi poss�vel adicionar o produto ao pedido, tente novamente.' );
		    	} else {

					$insert = $dbAdapter->update('pedido_produto',array(
						'quantidade'=>$quantidade,
						'valor_unitario'=>$valor,
						'valor_liquido'=>$valor_liquido
					),'id_pedido='.$id_pedido.' AND id_produto='.$id_produto);

					$retorno = array('status'=>true);
		    	}



	    	} catch (Zend_Db_Exception $e) {
	    		$retorno = array('status' => $e->getMessage() );
	    	}

	    	echo Zend_Json::encode($retorno);
		}

    }

    public function removerProdutoAction() {

    	$keyfiltro = $this->_request->getParam('b');
    	$id_pedido = $this->_request->getParam('idp');
    	$id_produto = $this->_request->getParam('id');

    	try {

    		$dbAdapter = Zend_Db_Table::getDefaultAdapter ();

	    	if (empty($id_pedido) || empty($id_produto)) {
	    		$this->_helper->FlashMessenger( array('error' => htmlentities('Valores incorretos, n&atilde;o foi poss&iacute;vel remover o produto do pedido, tente novamente.') ) );
	    	} else {

				$delete = $dbAdapter->delete('pedido_produto','id_pedido = '.$id_pedido.' AND id_produto='.$id_produto);

	    		//MENSAGEM DE SUCESSO
	    		$this->_helper->FlashMessenger( array('success' => htmlentities('Produto removido do Pedido!') ) );
	    	}

    	} catch (Zend_Db_Exception $e) {
    		$this->_helper->FlashMessenger( array('warning' => htmlentities( $e->getMessage()) ) );
    	}

    	$this->_helper->redirector('editar','admin-pedido',null,array('id'=>$id_pedido,'b'=>$keyfiltro));

    }

    public function excluirAction() {

        $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);

    	try {

    		$dbAdapter = Zend_Db_Table::getDefaultAdapter ();

        	$id = $this->_request->getParam('id');

	    	if (!$id) {
	    		$this->_helper->FlashMessenger( array('warning' => htmlentities('Aten��o! ID incorreto, tente novamente.') ) );
	    	} else {

    			//REMOVENDO PRODUTOS
    			$delete = $dbAdapter->delete('pedido_produto','id_pedido = '.$id);

    			//REMOVENDO PEDIDOSS
	    		$delete = $dbAdapter->delete('pedido','id_pedido = '.$id);

	    		//MENSAGEM DE SUCESSO
	    		$this->_helper->FlashMessenger( array('success' => htmlentities('Registro exclu�do!') ) );
	    	}

    	} catch (Zend_Db_Exception $e) {
    		$this->_helper->FlashMessenger( array('warning' => htmlentities( $e->getMessage()) ) );
    	}

    	$this->_helper->redirector('pesquisar','admin-pedido');

    }

    public function arquivarAction() {

    	$this->_helper->layout->disableLayout();
    	$this->_helper->viewRenderer->setNoRender(TRUE);

    	try {

    		$dbAdapter = Zend_Db_Table::getDefaultAdapter ();

    		$id = $this->_request->getParam('id');

    		if (!$id) {
    			$this->_helper->FlashMessenger( array('warning' => htmlentities('Aten��o! ID incorreto, tente novamente.') ) );
    		} else {

    			//ARQUIVANDO PEDIDO
    			$dbAdapter->update('pedido',array('arquivado'=>1),'id_pedido = '.$id);

    			//MENSAGEM DE SUCESSO
    			$this->_helper->FlashMessenger( array('success' => htmlentities('Registro arquivado!') ) );
    		}

    	} catch (Zend_Db_Exception $e) {
    		$this->_helper->FlashMessenger( array('warning' => htmlentities( $e->getMessage()) ) );
    	}

    	$this->_helper->redirector('pesquisar','admin-pedido');

    }

    public function ajaxProdutosAction() {
    	
		header('Content-Type: text/html; charset=utf-8');

        $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);

    	try {

	    	$dbAdapter = Zend_Db_Table::getDefaultAdapter ();
	        $id = $this->_request->getPost('id');

	        $select = $dbAdapter->select()->from(array('P'=>'produto'))
	        ->joinLeft(array('PE'=>'produto_embalagem'),'PE.id_produto_embalagem=P.id_produto_embalagem',array('embalagem'=>'PE.titulo'))
	        ->where('P.id_produto_categoria='.$id)
	        //->order('P.produto_ordem ASC')->order('PE.ordem ASC');
			->order('P.grupo_ordem ASC')->order('P.produto_ordem ASC')->order('P.titulo ASC')->order('PE.ordem ASC');
	        $rs = $dbAdapter->fetchAll($select);
			
			foreach($rs as $i=>$r) {
				$rs[$i]['titulo']=Porto80_Core::remove_accents($rs[$i]['titulo']);
			}
			 
			echo json_encode((array)$rs); 


        } catch (Zend_Db_Exception $e) {
    		//$this->_helper->FlashMessenger( array('warning' => htmlentities( $e->getMessage()) ) );
    	}

    }

    public function ajaxProdutoAction() {

        $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);

    	try {

	    	$dbAdapter = Zend_Db_Table::getDefaultAdapter ();
	        $id_produto = $this->_request->getPost('id');
	        $id_cliente = $this->_request->getPost('id_cliente');

    		if (!isset($id_cliente) && $id_cliente>0) {
				$select = $dbAdapter->select()->from('usuario','local_padrao')->where('id_cliente='.$id_cliente);
				$local = $dbAdapter->fetchOne($select);
    		} else {
    			$id_cliente = 0;
    			$local = 'SP';
    		}
    		$produto = Porto80_Core::getProdutoValor($id_produto, $local, $id_cliente);
    		$produto_valor = $produto['final'];
    		$produto_valor_liquido = $produto['liquido'];
    		if (!is_numeric($produto_valor)) $produto_valor=0;
    		if (!is_numeric($produto_valor_liquido)) $produto_valor_liquido=0;

    		//SELECIONANDO EMBALAGEM
    		$select = $dbAdapter->select()->from('produto','qtde_emb')->where('id_produto='.$id_produto);
    		$qtd_emb = $dbAdapter->fetchOne($select);
    		if (!is_numeric($qtd_emb)) $qtd_emb=0;


	        echo Zend_Json::encode(array('liquido'=>$produto_valor_liquido,'valor'=>$produto_valor,'qtdemb'=>$qtd_emb));

        } catch (Zend_Db_Exception $e) {
    		//$this->_helper->FlashMessenger( array('warning' => htmlentities( $e->getMessage()) ) );
    	}

    }

}
