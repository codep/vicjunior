<?php

class AdminUsuarioController extends Zend_Controller_Action
{

    public function init()
    {
    }

    public function indexAction()
    {
		$this->_helper->redirector('pesquisar','admin-usuario');
    }
    
    public function pesquisarAction() {
    	
        $pagina = $this->_request->getParam('pagina', 1) ;
        $perfil = $this->_request->getParam('perfil', '') ;
        
        try {
        	
			$dbAdapter = Zend_Db_Table::getDefaultAdapter ();
	        
			/* FILTRO */
	        $key = $this->getRequest()->getPost('key');
	        $this->view->key = $key;
	        
	        $select = $dbAdapter->select()->from(array('U'=>'usuario'),array('U.*'))->order('U.role ASC');
	        
	        if ($key!='') {
	        	$select->where('U.usuario LIKE "%'.$key.'%"');
	        }
	        
	        if (!empty($perfil)) {
	        	$select->where('U.role="'.$perfil.'"');
	        }
	        
	        $result = $dbAdapter->fetchAll($select);
	    	
		    /* PAGINACAO */
		    $dados = Zend_Paginator::factory($result);
		    $dados->setCurrentPageNumber( intval($pagina) );
		    $dados->setItemCountPerPage(20);
	        $this->view->dados = $dados;
	        
	        //PERFIL
	        $select = $dbAdapter->select()->from('role')->where('role!="visitante"')->order('role ASC');
	        $this->view->perfis = $dbAdapter->fetchAll($select);
	        $this->view->uPerfil = $perfil; 
        	
        } catch (Zend_Db_Exception $e) {
        	$this->_helper->FlashMessenger( array('warning' => htmlentities($e->getMessage()) ) );
        	$this->_helper->redirector('index','admin-resumo');
        }
    	
    	
    }

    public function novoAction()
    {

		$this->_helper->viewRenderer->render('formulario');

		
    }
    
    public function editarAction() {
    	
		$id = $this->_request->getParam('id');
		
    	if (!$id) {
    		$this->_helper->FlashMessenger( array('warning' => htmlentities('O registro informado nao existe. Verifique e tente novamente.') ) );
    		$this->_helper->redirector('pesquisar','admin-usuario');
    	}
		
    	//CARREGAR BANCO DE DADOS		
		try {
			
			$dbAdapter = Zend_Db_Table::getDefaultAdapter ();
			
			$select = $dbAdapter->select()->from(array('U'=>'usuario'))->where('id_usuario='.$id)->limit(1);			
			$dados = $dbAdapter->fetchRow($select);
			$this->view->dados = $dados;
				
		} catch (Zend_Db_Exception $e) {
			
			$this->_helper->FlashMessenger( array('warning' => htmlentities( $e->getMessage()) ) );
			$this->_helper->redirector('pesquisar','admin-usuario');
			
		}
    	
    	$this->_helper->viewRenderer->render('formulario');
    }
    
    public function salvarAction() {
    	
        $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);    	
    	
		if ($this->_request->isPost()) {
			
			$id_usuario = $this->_request->getPost('id_usuario', 0);
			
			try {
				
				$dbAdapter = Zend_Db_Table::getDefaultAdapter ();
				
    			$dados = array(
    				'usuario'=>			$this->getRequest()->getPost('usuario'),
    				'senha'=>			$this->getRequest()->getPost('senha'),
    				'nsenha'=>			$this->getRequest()->getPost('nsenha'),
    				'role'=>			$this->getRequest()->getPost('role'),    			
    				'status'=>			$this->getRequest()->getPost('status'),
    				'id_cliente'=>		0,
    				'local_padrao'=>	'SP',
    			);
    			
    			$valid=true;
				if ($dados['nsenha']!='' && $id_usuario>0){
					$dados['senha'] = $dados['nsenha']; 
    			}
				if ($dados['senha']=='') unset($dados['senha']);
				unset ($dados['nsenha']);
				
				//VERIFICANDO SE USU�RIO OU E-MAIL JA EXISTE
    			if ($id_usuario>0) {
	    			$select = $dbAdapter->select()->from('usuario',array('usuario'))->where('id_usuario='.$id_usuario);
	    			$resultadoC = $dbAdapter->fetchRow($select);
	    			if ($dados['usuario']!=$resultadoC['usuario']) {
	    				$select = $dbAdapter->select()->from('usuario',array('id_usuario'))->where('usuario LIKE "'.$dados['usuario'].'"');
	    				$resultado = $dbAdapter->fetchRow($select);
	    				if (is_array($resultado)) {
	    					$this->_helper->FlashMessenger( array('error' => htmlentities('Usuario ja existe em nosso banco de dados, por favor informe outro e tente novamente.') ) );
	    					$valid=false;
	    				}
	    			}
	    			
    			} else {
        			$select = $dbAdapter->select()->from('usuario',array('id_usuario'))->where('usuario LIKE "'.$dados['usuario'].'"');
    				$resultado = $dbAdapter->fetchRow($select);
    				if (is_array($resultado)) {
    					$this->_helper->FlashMessenger( array('error' => htmlentities('Usuario ja existe em nosso banco de dados, por favor informe outro e tente novamente.') ) );
    					$valid=false;
    				}    			
    			}
				
    			if ($valid) {
    				if (isset($dados['senha'])){
    					$dados['senha']=SHA1($dados['senha']);
    				}
    			
			    	if ($id_usuario<=0) {
			    		//INCLUINDO REGISTRO
			    		$insert= $dbAdapter->insert('usuario',$dados);
			    		$lastID = $dbAdapter->lastInsertId('usuario','id_usuario');
			    		$this->_helper->FlashMessenger( array('success' => htmlentities('Registro adicionado!') ) );
			    	} else {
			    		//ATUALIZANDO REGISTRO
			    		$update = $dbAdapter->update('usuario',$dados,'id_usuario='.$id_usuario);
			    		$this->_helper->FlashMessenger( array('success' => htmlentities('Registro atualizado!') ) );
			    		$lastID = $id_usuario;
			    	}
			    	
			    	$this->_helper->redirector('pesquisar','admin-usuario');
	    		} else {
	    			if ($id_usuario<=0) {
	    				$this->_helper->redirector('novo','admin-usuario');
	    			} else {
	    				$this->_helper->redirector('editar','admin-usuario',null,array('id'=>$id_usuario));
	    			}
	    		}
					
			} catch (Zend_Db_Exception $e) {
				$this->_helper->FlashMessenger( array('error' => htmlentities($e->getMessage()) ) );
				$this->_helper->redirector('pesquisar','admin-usuario');
			}
			
		}
    	
    }
    
    public function excluirAction() {
    	
        $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
    	
    	try {
    		
    		$dbAdapter = Zend_Db_Table::getDefaultAdapter ();
    		
        	$id = $this->_request->getParam('id');
    		
	    	if (!$id) {
	    		$this->_helper->FlashMessenger( array('warning' => htmlentities('Atencao! ID incorreto, tente novamente.') ) );
	    	} else {
	    		
    			//REMOVENDO REGISTROS
	    		$delete = $dbAdapter->delete('usuario','id_usuario = '.$id);
	    		//MENSAGEM DE SUCESSO
	    		$this->_helper->FlashMessenger( array('success' => htmlentities('Registro excluido!') ) );
	    	}
    			
    	} catch (Zend_Db_Exception $e) {
    		$this->_helper->FlashMessenger( array('warning' => htmlentities( $e->getMessage()) ) );
    	}
		
    	$this->_helper->redirector('pesquisar','admin-usuario');
    	
    }

}

