<?php
require_once 'thumb/ThumbLib.inc.php'; //IMPORTA CLASSE DE IMAGEM
class ImagemController extends Zend_Controller_Action
{

    public function init()
    {
    	$this->view->headMeta()
			->setName('ROBOTS', 'NOINDEX,NOFOLLOW')
		;		
    }

    public function indexAction()
    {
    	/*
    	$this->view->headScript()
			->appendFile( $this->view->baseUrl('/js/highcharts.js'),'text/javascript')
			->appendFile( $this->view->baseUrl('/js/flot/excanvas.min.js'),'text/javascript', array('conditional'=>'IE'))
    	;
    	*/
    }
    
    public function galeriaAction() {
    	
    	/* PLUGIN */
    	$this->view->headScript()
			->appendFile( $this->view->baseUrl('/js/jquery.validate_pack.js'),'text/javascript')
			->appendFile( $this->view->baseUrl('/js/jquery.uploadify.v2.1.0.min.js'),'text/javascript')
			->appendFile( $this->view->baseUrl('/js/jcrop/jquery.Jcrop.min.js'),'text/javascript')
		;
    	$this->view->headLink()
			->appendStylesheet( $this->view->baseUrl('/css/uploadify.css') )
			->appendStylesheet( $this->view->baseUrl('/js/jcrop/jquery.Jcrop.css') )
		;
    	
    	$popup = $this->_getParam('popup');
    	if ($popup=='true') {
	        $this->_helper->layout->disableLayout();
			//$this->_helper->viewRenderer->setNoRender(TRUE);
			$this->view->popup=true;    		
    	} else {
    		$this->view->popup=false;
    	}
    	
    	//PARAMETROS DE CONFIGURA��O DA P�GINA
    	$limite = intval($this->_getParam('limite')); //N�MERO DE IMAGENS (0=ILIMITADO)
    	if (!is_numeric($limite)||$limite=='') $limite=0;
    	$this->view->limite=$limite;
    	
    	//SETAR COMO GALERIA PADR�O
    	$this->view->id_galeria=0;
    	
    }
    
    public function infoAction() {
    	
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		
    	if ($this->_request->isPost()) {

    		$dbAdapter = Zend_Db_Table::getDefaultAdapter ();
    		
    		$id_galeria = $this->_request->getPost('id_galeria');
    		$filename = $this->_request->getPost('filename');
    		
    		$status=0;$msg='';$dados=array();
    		try {
		        $select = $dbAdapter->select()->from(array('GI'=>'galeria_imagem'),array('filename','path','legenda','datainc'))->joinInner(array('G'=>'galeria'),'G.id_galeria=GI.id_galeria',array('id_galeria','titulo')) ->where('GI.filename="'.$filename.'" AND GI.id_galeria='.$id_galeria)->limit(1);
				$dados = $dbAdapter->fetchRow($select);
				
				$status=1;
				
    		} catch (Zend_Db_Exception $e) {
    			$msg=$e->getMessage();
    		}
    		
			$retorno = array('status'=>$status,'mensagem'=>$msg,'dados'=>$dados);
			echo json_encode($retorno);die();
			
    	}		
    	
    }
    
    /* EXIBIR IMAGEM DA PASTA UPLOAD - NO RENDER */
    public function showAction() {
    	
        $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		
    	/* PARAMETROS */
		$imagem = $this->_getParam('i'); //NOME DA IMAGEM
		$id_galeria = $this->_getParam('g'); //GALERIA
		$pasta=$this->_getParam('p'); //GALERIA;
		$altura = intval($this->_getParam('a'));
		$largura = intval($this->_getParam('l'));
		$resizecrop = intval($this->_getParam('rc'));
		
		if ($id_galeria!='') {
			$pastaAnexo = 'galeria'.DIRECTORY_SEPARATOR.$id_galeria.DIRECTORY_SEPARATOR; 
		} else {
			if ($pasta!='') {
				$pastaAnexo= $pasta . DIRECTORY_SEPARATOR;
			} else {
				$pastaAnexo = '';	
			}
		}
		
		$pathImg = realpath(APPLICATION_PATH.'/../upload/'.$pastaAnexo.$imagem);
		
		if (!is_file($pathImg)) {
    		$pathImg = realpath(APPLICATION_PATH.'/../upload/no-photo.png');
    	}
    	
    	$this->thumb = PhpThumbFactory::create($pathImg);
    	
		if ($resizecrop!='') {
			$dim = $this->thumb->getCurrentDimensions();
			if ($dim['height']>$dim['width']) { //ALTURA MAIOR QUE LARGURA
				$this->thumb->resize($resizecrop,0);	
			} else {
				$this->thumb->resize(0,$resizecrop);
			}
			$this->thumb->cropFromCenter($resizecrop);
		} else {
			$this->thumb->resize($largura, $altura);
		}
		
		$this->thumb->show();
		
    }		
    
    /* LISTA AS IMAGENS DE UMA GALERIA */
    public function listAction() {
    	
        $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
    	
    	if ($this->_request->isPost()) {

    		$dbAdapter = Zend_Db_Table::getDefaultAdapter ();
    		
    		$id_galeria = $this->_request->getPost('id_galeria');
    		$status=0;$msg='';$registros=array();
    		
    		if (is_numeric($id_galeria)) {
    		
    			try {
    				$select = $dbAdapter->select()->from('galeria_imagem')->where('id_galeria='.$id_galeria)->order('datainc DESC');
					$registros = $dbAdapter->fetchAll($select);
    				$msg='Imagens recebidas com sucesso!';
    				$status=1;
    			} catch (Exception $e) {
    				$msg=$e->getMessage();	
    			}

    		} else {
    			$msg=utf8_encode('Galeria inv�lida');
    		}
    		
    		$retorno = array('status'=>$status,'mensagem'=>$msg,'dados'=>$registros);
    		echo json_encode($retorno);die();
    		
    	}
    	
    }
    
    /* FOR�AR DOWNLOAD DE UMA DETERMINADA PASTA */
    public function downloadAction() {
    	
    	
    }
    
    /* � NECESS�RIO EFETUAR O UPLOAD ANTES
     * SALVA A IMAGEM NO BANCO DE DADOS 
     */
    public function saveAction() {
    	
        $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		
    	$ajax = $this->_getParam('ajax');
    	if ($ajax=='true') {
    		
    		$dbAdapter = Zend_Db_Table::getDefaultAdapter ();
    		
    		$id_galeria = $this->_request->getPost('id_galeria');
    		$filename = $this->_request->getPost('filename');    		
    		$path = $this->_request->getPost('path');
    		$legenda = $this->_request->getPost('legenda');
    		$data = $this->_request->getPost('data');
    		if ($data=='') {
    			$datainc = date('Y-m-d H:i:s');
    		} else {
    			if (Zend_Date::isDate($data)) {
    				$datainc = new Zend_date($data, 'YYYY-MM-dd HH:mm:ss');
    			} else {
    				$datainc = date('Y-m-d H:i:s');
    			}
    		}
    		
    		$dados = array(
    			'id_galeria'=>$id_galeria,
	    		'filename'=>rawurldecode($filename),
	    		'path'=>rawurldecode($path),
	    		'legenda'=>$legenda,
    			'datainc'=>$datainc
    		);
    		
    		$status=0; $msg='';
    		
    		try {
    			$insert = $dbAdapter->insert('galeria_imagem',$dados);
    			$msg = 'Imagem cadastrada com sucesso!';
    			$status=1;    			
    		} catch (Zend_Db_Exception  $e) {
    			$status=2;
    			$msg = $e->getMessage();
    		}
    		
    		$retorno = array('status'=>$status,'mensagem'=>$msg);
    		//RETORNO EM JSON
    		echo json_encode($retorno);die();

    	}
    	
    }
    
    public function deleteAction() {
    	
    	
    }

}

