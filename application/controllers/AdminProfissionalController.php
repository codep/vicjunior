<?php

class AdminProfissionalController extends Zend_Controller_Action
{

    public function init()
    {
    }

    public function indexAction()
    {
		$this->_helper->redirector('pesquisar','admin-profissional');
    }
    
    public function pesquisarAction() {
    	
        $pagina = $this->_request->getParam('pagina', 1) ;
        
        try {
        	
			$dbAdapter = Zend_Db_Table::getDefaultAdapter ();
	        
			/* FILTRO */
	        $key = $this->getRequest()->getPost('key');
	        $this->view->key = $key;
	        
	        $select = $dbAdapter->select()
	        ->from(array('P'=>'profissional'),array('P.nome','P.id_profissional','categoria'=>'(SELECT GROUP_CONCAT(PC.titulo) FROM profissional_categoria PC WHERE PC.id_noticia_categoria=PCL.id_noticia_categoria)'))
	        ->joinLeft(array('PCL'=>'profissional_categoria_link'),'PCL.id_profissional=P.id_profissional',array())
	        ->order('categoria ASC')->order('P.nome ASC');
	        if ($key!='') {
	        	$select->where('P.nome LIKE "%'.$key.'%"');
	        }
	        
	        $result = $dbAdapter->fetchAll($select);
	    	
		    /* PAGINA��O */
		    $dados = Zend_Paginator::factory($result);
		    $dados->setCurrentPageNumber( intval($pagina) );
		    $dados->setItemCountPerPage(20);
	        $this->view->dados = $dados;
        	
        	
        } catch (Zend_Db_Exception $e) {
        	$this->_helper->FlashMessenger( array('warning' => htmlentities($e->getMessage()) ) );
        	$this->_helper->redirector('index','admin-resumo');
        }
    	
    	
    }

    public function novoAction()
    {
    	$this->view->headScript()
			->appendFile( $this->view->baseUrl('/admin_js/jquery.validate_pack.js'),'text/javascript')
			->appendFile( $this->view->baseUrl('/admin_js/tiny_mce/jquery.tinymce.js'),'text/javascript')
		;
		
		try {
			
			$dbAdapter = Zend_Db_Table::getDefaultAdapter ();
			
			/* CATEGORIAS */
			$select = $dbAdapter->select()->from(array('PC'=>'profissional_categoria'))->order('PC.titulo ASC');
			$categorias = $dbAdapter->fetchAll($select);
			$this->view->categorias = $categorias;
						
		} catch (Zend_Db_Exception $e) {
        	$this->_helper->FlashMessenger( array('warning' => htmlentities($e->getMessage()) ) );
        	$this->_helper->redirector('pesquisar','admin-produto');
		}
		
		$this->_helper->viewRenderer->render('formulario');
		
    }
    
    public function editarAction() {
    	
		$id = $this->_request->getParam('id');
		
    	if (!$id) {
    		$this->_helper->FlashMessenger( array('warning' => htmlentities('O registro informado n�o existe. Verifique e tente novamente.') ) );
    		$this->_helper->redirector('pesquisar','admin-profissional');
    	}
		
    	$this->view->headScript()
			->appendFile( $this->view->baseUrl('/admin_js/jquery.validate_pack.js'),'text/javascript')
			->appendFile( $this->view->baseUrl('/admin_js/tiny_mce/jquery.tinymce.js'),'text/javascript')
		;
		
    	//CARREGAR BANCO DE DADOS		
		try {
			
			$dbAdapter = Zend_Db_Table::getDefaultAdapter ();
			
			/* CATEGORIAS */
			$select = $dbAdapter->select()->from(array('PC'=>'profissional_categoria'))->order('PC.titulo ASC');
			$categorias = $dbAdapter->fetchAll($select);
			$this->view->categorias = $categorias;
			
			/* PROFISSIONAL */
			$select = $dbAdapter->select()->from(array('P'=>'profissional'))->joinLeft(array('PCL'=>'profissional_categoria_link'),'PCL.id_profissional=P.id_profissional',array('id_categorias'=>'GROUP_CONCAT(PCL.id_profissional_categoria)'))->where('P.id_profissional='.$id)->limit(1);			
			$dados = $dbAdapter->fetchRow($select);
			$dados['id_categorias'] = explode(',',$dados['id_categorias']);
			$this->view->dados = $dados;
				
		} catch (Zend_Db_Exception $e) {
			
			$this->_helper->FlashMessenger( array('warning' => htmlentities( $e->getMessage()) ) );
			$this->_helper->redirector('pesquisar','admin-profissional');
			
		}
    	
    	$this->_helper->viewRenderer->render('formulario');
    }
    
    public function salvarAction() {
    	
        $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);    	
    	
		if ($this->_request->isPost()) {
			
			$id_profissional = $this->_request->getPost('id_profissional', 0);
			
			try {
				
				$dbAdapter = Zend_Db_Table::getDefaultAdapter ();
				
    			$dados = array(
    				'nome'=>			$this->getRequest()->getPost('nome'),
    				'info'=>			htmlentities($this->getRequest()->getPost('info'))
    			);
    			
		    	if ($id_profissional<=0) {
		    		//INCLUINDO REGISTRO
		    		$insert= $dbAdapter->insert('profissional',$dados);
		    		$lastID = $dbAdapter->lastInsertId('profissional','id_profissional');
		    		$this->_helper->FlashMessenger( array('success' => htmlentities('Registro inclu�do!') ) );
		    	} else {
		    		//ATUALIZANDO REGISTRO
		    		$update = $dbAdapter->update('profissional',$dados,'id_profissional='.$id_profissional);
		    		$this->_helper->FlashMessenger( array('success' => htmlentities('Registro atualizado!') ) );
		    		$lastID = $id_profissional;
		    	}
		    	
		    	//VINCULANDO CATEGORIAS
    	    	$delete = $dbAdapter->delete('profissional_categoria_link','id_profissional='.$lastID);
        		$categorias = $this->_request->getPost('id_profissional_categoria', 0);
				foreach($categorias as $categoria) {
					$insert = $dbAdapter->insert('profissional_categoria_link',array('id_profissional'=>$lastID,'id_profissional_categoria'=>$categoria));
				}
		    	
		    	$this->_helper->redirector('pesquisar','admin-profissional');
					
			} catch (Zend_Db_Exception $e) {
				$this->_helper->FlashMessenger( array('error' => htmlentities($e->getMessage()) ) );
				$this->_helper->redirector('pesquisar','admin-profissional');
			}
			
		}
    	
    }
    
    public function excluirAction() {
    	
        $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
    	
    	try {
    		
    		$dbAdapter = Zend_Db_Table::getDefaultAdapter ();
    		
        	$id = $this->_request->getParam('id');
    		
	    	if (!$id) {
	    		$this->_helper->FlashMessenger( array('warning' => htmlentities('Aten��o! ID incorreto, tente novamente.') ) );
	    	} else {	    		
    			//REMOVENDO REGISTROS
	    		$delete = $dbAdapter->delete('profissional','id_profissional = '.$id);
	    		//MENSAGEM DE SUCESSO
	    		$this->_helper->FlashMessenger( array('success' => htmlentities('Registro exclu�do!') ) );
	    	}
    			
    	} catch (Zend_Db_Exception $e) {
    		$this->_helper->FlashMessenger( array('warning' => htmlentities( $e->getMessage()) ) );
    	}
		
    	$this->_helper->redirector('pesquisar','admin-profissional');
    	
    }

}

