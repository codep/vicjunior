<?php

class AdminClienteController extends Zend_Controller_Action
{

    public function init()
    {
    }
	
    public function indexAction()
    {
		$this->_helper->redirector('pesquisar','admin-cliente');
    }
    
    public function pesquisarAction() {
    	
        $pagina = $this->_request->getParam('pagina', 1) ;
        
        //PRECISO PARA ABRIR VISUALIZAR
    	$this->view->headScript()
			->appendFile( $this->view->baseUrl('/admin_js/tiny_mce/jquery.tinymce.js'),'text/javascript')
			->appendFile( $this->view->baseUrl('/admin_js/jquery.maskedinput-1.2.2.min.js'),'text/javascript')
		;
        
        try {
        	
			$dbAdapter = Zend_Db_Table::getDefaultAdapter ();
	        
			/* FILTRO */
	        $key = $this->getRequest()->getParam('key');
	        $id_cliente_grupo = $this->getRequest()->getParam('grupo');
	        $id_produto_revenda = $this->getRequest()->getParam('revenda');
	        $this->view->key = $key;
	        $this->view->id_cliente_grupo = $id_cliente_grupo;
	        $this->view->id_produto_revenda = $id_produto_revenda;
	        
	        $select = $dbAdapter->select()->from(array('C'=>'cliente'),array('C.*'))
	        ->joinInner(array('U'=>'usuario'),'U.id_cliente=C.id_cliente',array('status_acesso'=>'U.status'))
	        ->order('C.cidade ASC')->order('C.estado ASC')->order('C.razao_social ASC');
	        
	        if (!empty($key)) {
	        	if ($key=='ao') {
	        		$select->where('C.observacao !=""');
	        	} else {
	        		$select->where('C.razao_social LIKE "%'.$key.'%" OR C.email LIKE "%'.$key.'%" OR C.nome LIKE "%'.$key.'%" OR C.cidade LIKE "%'.$key.'%"');
	        	}
	        }
	        
	        if (!empty($id_cliente_grupo)) {
	        	$select->joinInner( array('CGP'=>'cliente_grupo_produto') , 'CGP.id_cliente=C.id_cliente', array())
	        	->where('CGP.id_cliente_grupo='.$id_cliente_grupo)
	        	->group('C.id_cliente');
	        }
	        
	        if (!empty($id_produto_revenda)) {
	        	$select->joinInner( array('CR'=>'cliente_revenda') , 'CR.id_cliente=C.id_cliente', array())
	        	->where('CR.id_produto_revenda='.$id_produto_revenda)
	        	->group('C.id_cliente');
	        }
	        
	        $result = $dbAdapter->fetchAll($select);
	    	
		    /* PAGINACAO */
		    $dados = Zend_Paginator::factory($result);
		    $dados->setCurrentPageNumber( intval($pagina) );
		    $dados->setItemCountPerPage(20);
	        $this->view->dados = $dados;
	        
	        /* GRUPOS */
	        $select  = $dbAdapter->select()->from(array('CG'=>'cliente_grupo'))->order('CG.titulo ASC');
	        $this->view->grupos = $dbAdapter->fetchAll($select);
	        
	        /* REVENDAS */
	        $select  = $dbAdapter->select()->from(array('PR'=>'produto_revenda'))->order('PR.titulo ASC');
	        $this->view->revendas = $dbAdapter->fetchAll($select);
        	
        } catch (Zend_Db_Exception $e) {
        	$this->_helper->FlashMessenger( array('warning' => htmlentities($e->getMessage()) ) );
        	$this->_helper->redirector('index','admin-resumo');
        }
        
        /* ARMAZENA A URL DE PESQUISA */
    	$filtros = new Zend_Session_Namespace('filtros');
    	$keyfiltro = md5($this->_request->getControllerName().'_'.$this->_request->getActionName());
		$filtros->$keyfiltro = $this->view->url();
		$this->view->keyfiltro = $keyfiltro;
    	
    }

    public function novoAction()
    {
    	$this->view->headScript()
    		->appendFile( $this->view->baseUrl('/admin_js/jquery-custom-file-input.js'),'text/javascript')
			->appendFile( $this->view->baseUrl('/admin_js/jquery.validate_pack.js'),'text/javascript')
			->appendFile( $this->view->baseUrl('/admin_js/tiny_mce/jquery.tinymce.js'),'text/javascript')
			->appendFile( $this->view->baseUrl('/admin_js/jquery.maskedinput-1.2.2.min.js'),'text/javascript')
		;
		
        //CARREGAR BANCO DE DADOS		
		try {
			
			$dbAdapter = Zend_Db_Table::getDefaultAdapter ();
			
			/* LINHAS */
			$select = $dbAdapter->select()
			->from(array('PL'=>'produto_linha'))
			->joinInner(array('PR'=>'produto_revenda'),'PR.id_produto_revenda=PL.id_produto_revenda',array('revenda'=>'PR.titulo'))
			->order('PR.titulo ASC')->order('PL.titulo ASC');
			$dados = $dbAdapter->fetchAll($select);
			//SEPARANDO POR REVENDA
			$produtos_linhas = '';
			foreach($dados as $dado) {
				$produtos_linhas[$dado['revenda']][]=$dado;
			}
			$this->view->produto_linha = $produtos_linhas;
			
			/* GRUPOS */
			$select = $dbAdapter->select()->from(array('CG'=>'cliente_grupo'))->order('CG.titulo ASC');
			$dados = $dbAdapter->fetchAll($select);
			$this->view->cliente_grupo = $dados;
			
			/* REVENDAS */
			$select  = $dbAdapter->select()->from(array('PR'=>'produto_revenda'))->order('PR.titulo ASC');
			$this->view->revendas = $dbAdapter->fetchAll($select);
			
			$this->view->clientes_produtos = array();
			
				
		} catch (Zend_Db_Exception $e) {
			
			$this->_helper->FlashMessenger( array('warning' => htmlentities( $e->getMessage()) ) );
			$this->_helper->redirector('pesquisar','admin-cliente');
			
		}
		
		/* MANIPULA URL DE RETORNO */
    	$filtros = new Zend_Session_Namespace('filtros');
    	$keyfiltro = $this->_request->getParam('b');
    	$this->view->keyfiltro = $keyfiltro;
    	if (!empty($keyfiltro) && isset($filtros->$keyfiltro)) {
    		$this->view->url_back=$filtros->$keyfiltro;
    	} else {
    		$this->view->url_back=$this->view->baseUrl('admin-cliente/pesquisar');
    	}
		
		$this->_helper->viewRenderer->render('formulario');
		
    }
    
    public function editarAction() {
    	
		$id = $this->_request->getParam('id');
		
    	if (!$id) {
    		$this->_helper->FlashMessenger( array('warning' => htmlentities('O registro informado n�o existe. Verifique e tente novamente.') ) );
    		$this->_helper->redirector('pesquisar','admin-cliente');
    	}
		
    	$this->view->headScript()
    		->appendFile( $this->view->baseUrl('/admin_js/jquery-custom-file-input.js'),'text/javascript')
			->appendFile( $this->view->baseUrl('/admin_js/jquery.validate_pack.js'),'text/javascript')
			->appendFile( $this->view->baseUrl('/admin_js/tiny_mce/jquery.tinymce.js'),'text/javascript')
			->appendFile( $this->view->baseUrl('/admin_js/jquery.maskedinput-1.2.2.min.js'),'text/javascript')
		;
		
    	//CARREGAR BANCO DE DADOS		
		try {
			
			$dbAdapter = Zend_Db_Table::getDefaultAdapter ();
			
			$select = $dbAdapter->select()->from(array('C'=>'cliente'))
			->joinInner(array('U'=>'usuario'),'U.id_cliente=C.id_cliente',array('status_acesso'=>'status','usuario','senha','local_padrao'))
			->where('C.id_cliente='.$id)
			->limit(1);
			$dados = $dbAdapter->fetchRow($select);
			$this->view->dados = $dados;
			
			/* CLIENTE REVENDAS */
			$select = $dbAdapter->select()->from(array('CR'=>'cliente_revenda'))->where('CR.id_cliente='.$id);
			$rs = $dbAdapter->fetchAll($select);
			$crDados = ''; 
			foreach($rs as $r) {
				$crDados[$r['id_produto_revenda']]=$r['codigo'];
			}
			$this->view->clientes_revendas = $crDados;
			
			/* LINHAS */
			$select = $dbAdapter->select()
			->from(array('PL'=>'produto_linha'))
			->joinInner(array('PR'=>'produto_revenda'),'PR.id_produto_revenda=PL.id_produto_revenda',array('revenda'=>'PR.titulo'))
			->order('PR.titulo ASC')->order('PL.titulo ASC');
			$dados = $dbAdapter->fetchAll($select);
			//SEPARANDO POR REVENDA
			$produtos_linhas = '';
			foreach($dados as $dado) {
				$produtos_linhas[$dado['revenda']][]=$dado;
			}
			$this->view->produto_linha = $produtos_linhas;
			
			/* Cliente GRUPOS */
			$select = $dbAdapter->select()->from(array('CG'=>'cliente_grupo'))->order('CG.titulo ASC');
			$dados = $dbAdapter->fetchAll($select);
			$this->view->cliente_grupo = $dados;
			
			/* GRUPOS CADASTRADOS */
			$select = $dbAdapter->select()
			->from(array('CGP'=>'cliente_grupo_produto'))
			->joinInner(array('CG'=>'cliente_grupo'),'CGP.id_cliente_grupo=CG.id_cliente_grupo',array('cliente_grupo'=>'CG.titulo','id_cliente_grupo'))
			->joinInner(array('PG'=>'produto_linha'),'CGP.id_produto_linha=PG.id_produto_linha',array('produto_linha'=>'PG.titulo','id_produto_linha'))
			->joinInner(array('PR'=>'produto_revenda'),'PR.id_produto_revenda=PG.id_produto_revenda',array('revenda'=>'PR.titulo','id_produto_revenda'))
			->where('CGP.id_cliente='.$id)
			->order('PR.titulo ASC')->order('PG.titulo ASC');
			$dados = $dbAdapter->fetchAll($select);
			//SEPARANDO POR REVENDA
			$clientes_produtos = '';
			foreach($dados as $dado) {
				$clientes_produtos[$dado['revenda']][]=$dado;
			}
			$this->view->clientes_produtos = $clientes_produtos;
			
			/* REVENDAS */
			$select  = $dbAdapter->select()->from(array('PR'=>'produto_revenda'))->order('PR.titulo ASC');
			$this->view->revendas = $dbAdapter->fetchAll($select);
			
				
		} catch (Zend_Db_Exception $e) {
			
			$this->_helper->FlashMessenger( array('warning' => htmlentities( $e->getMessage()) ) );
			$this->_helper->redirector('pesquisar','admin-cliente');
			
		}
		
		/* MANIPULA URL DE RETORNO */
    	$filtros = new Zend_Session_Namespace('filtros');
    	$keyfiltro = $this->_request->getParam('b');
    	$this->view->keyfiltro = $keyfiltro;
    	if (!empty($keyfiltro) && isset($filtros->$keyfiltro)) {
    		$this->view->url_back=$filtros->$keyfiltro;
    	} else {
    		$this->view->url_back=$this->view->baseUrl('admin-cliente/pesquisar');
    	}
    	
    	$this->_helper->viewRenderer->render('formulario');
    }
    
    public function visualizarAction() {
    	
        $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		
		header("Content-Type: text/html; charset=ISO-8859-1",true);
    	
		$id = $this->_request->getParam('id');
		
    	if (!$id) {
    		$this->_helper->FlashMessenger( array('warning' => htmlentities('O registro informado n�o existe. Verifique e tente novamente.') ) );
    		$this->_helper->redirector('pesquisar','admin-cliente');
    	}
		
    	$this->view->headScript()
			->appendFile( $this->view->baseUrl('/admin_js/jquery.maskedinput-1.2.2.min.js'),'text/javascript')
		;
		
    	//CARREGAR BANCO DE DADOS		
		try {
			
			$dbAdapter = Zend_Db_Table::getDefaultAdapter ();
			
			$select = $dbAdapter->select()->from(array('C'=>'cliente'))
			->joinInner(array('U'=>'usuario'),'U.id_cliente=C.id_cliente',array('status_acesso'=>'U.status','usuario','senha','local_padrao'))
			->where('C.id_cliente='.$id)->limit(1);
			$dados = $dbAdapter->fetchRow($select);
			$this->view->dados = $dados;
			
			/* CLIENTE REVENDAS */
			$select = $dbAdapter->select()->from(array('CR'=>'cliente_revenda'))->where('CR.id_cliente='.$id);
			$rs = $dbAdapter->fetchAll($select);
			$crDados = '';
			foreach($rs as $r) {
				$crDados[$r['id_produto_revenda']]=$r['codigo'];
			}
			$this->view->clientes_revendas = $crDados;
			
			/* GRUPOS */
			$select = $dbAdapter->select()->from(array('PG'=>'produto_linha'))->order('PG.titulo ASC');
			$dados = $dbAdapter->fetchAll($select);
			$this->view->produto_linha = $dados;
			
			/* GRUPOS */
			$select = $dbAdapter->select()->from(array('CG'=>'cliente_grupo'))->order('CG.titulo ASC');
			$dados = $dbAdapter->fetchAll($select);
			$this->view->cliente_grupo = $dados;
			
			/* GRUPOS CADASTRADOS */
			$select = $dbAdapter->select()
			->from(array('CGP'=>'cliente_grupo_produto'))
			->joinInner(array('CG'=>'cliente_grupo'),'CGP.id_cliente_grupo=CG.id_cliente_grupo',array('cliente_grupo'=>'CG.titulo','id_cliente_grupo'))
			->joinInner(array('PG'=>'produto_linha'),'CGP.id_produto_linha=PG.id_produto_linha',array('produto_linha'=>'PG.titulo','id_produto_linha'))
			->where('CGP.id_cliente='.$id)
			->order('PG.titulo ASC');
			$dados = $dbAdapter->fetchAll($select);
			$this->view->clientes_produtos = $dados;
			
			/* REVENDAS */
			$select  = $dbAdapter->select()->from(array('PR'=>'produto_revenda'))->order('PR.titulo ASC');
			$this->view->revendas = $dbAdapter->fetchAll($select);
				
		} catch (Zend_Db_Exception $e) {
			
			$this->_helper->FlashMessenger( array('warning' => htmlentities( $e->getMessage()) ) );
			$this->_helper->redirector('pesquisar','admin-cliente');
			
		}
    	
    	$this->_helper->viewRenderer->render('visualizar');
    }
    
    public function salvarAction() {
    	
        $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);    	
    	
		if ($this->_request->isPost()) {
			
			$id_cliente = $this->_request->getPost('id_cliente', 0);
			
			try {
				
				$dbAdapter = Zend_Db_Table::getDefaultAdapter ();
				
    			$dados = array(
    				'nome'=>			$this->getRequest()->getPost('nome'),
    				'razao_social'=>	$this->getRequest()->getPost('razao_social'),
	    			'cnpj'=>			$this->getRequest()->getPost('cnpj'),
	    			'ie'=>				$this->getRequest()->getPost('ie'),
	    			'endereco'=>		$this->getRequest()->getPost('endereco'),
	    			'bairro'=>			$this->getRequest()->getPost('bairro'),
	    			'cidade'=>			$this->getRequest()->getPost('cidade'),
	    			'estado'=>			$this->getRequest()->getPost('estado'),
	    			'cep'=>				Porto80_Core::converteCep($this->getRequest()->getPost('cep')),
	    			'telefone_com'=>	Porto80_Core::converteTelefone($this->getRequest()->getPost('telefone_com')),
	    			'telefone_fax'=>	Porto80_Core::converteTelefone($this->getRequest()->getPost('telefone_fax')),
	    			'resp_cel'=>		Porto80_Core::converteTelefone($this->getRequest()->getPost('resp_cel')),
	    			'responsavel'=>		$this->getRequest()->getPost('responsavel'),
	    			'email'=>			$this->getRequest()->getPost('email'),
					'emailpublico'=>$this->_request->getPost('emailpublico'),
					'exibirpublico'=>$this->_request->getPost('exibirpublico'),
	    			'observacao'=>		$this->getRequest()->getPost('observacao')
    			);
    			if (empty($dados['codigo'])) $dados['codigo']=substr(time(),-6);
    			
				$Udados = array(
    				'usuario'=>			$this->getRequest()->getPost('usuario'),
    				'senha'=>			$this->getRequest()->getPost('senha'),
    				'nsenha'=>			$this->getRequest()->getPost('nsenha'),
    				'status'=>			$this->getRequest()->getPost('status_acesso'),
					'local_padrao'=>	$this->getRequest()->getPost('local_padrao')
				);
    			
    			$valid=true;
				if ($Udados['nsenha']!='' && $id_cliente>0){
					$Udados['senha'] = $Udados['nsenha']; 
    			}
				if ($Udados['senha']=='') unset($Udados['senha']);
				unset ($Udados['nsenha']);
				
				//VERIFICANDO SE USU�RIO OU E-MAIL JA EXISTE
    			if ($id_cliente>0) {
	    			$select = $dbAdapter->select()->from(array('C'=>'cliente'),array('email','foto'))
	    			->joinInner(array('U'=>'usuario'),'U.id_cliente=C.id_cliente',array('usuario'))
	    			->where('C.id_cliente='.$id_cliente);
	    			$resultadoC = $dbAdapter->fetchRow($select);
	    			if ($Udados['usuario']!=$resultadoC['usuario']) {
	    				$select = $dbAdapter->select()->from('usuario',array('id_cliente'))->where('usuario LIKE "'.$Udados['usuario'].'"');
	    				$resultado = $dbAdapter->fetchRow($select);
	    				if (is_array($resultado)) {
	    					$this->_helper->FlashMessenger( array('error' => htmlentities('Usu�rio j� existe em nosso banco de dados, por favor informe outro e tente novamente.') ) );
	    					$valid=false;
	    				}
	    			}
	    			/*
        			if ($dados['email']!=$resultadoC['email'] && $dados['email']!='') {
	    				$select = $dbAdapter->select()->from('cliente',array('id_cliente'))->where('email LIKE "'.$dados['email'].'"');
	    				$resultado = $dbAdapter->fetchRow($select);
        				if (is_array($resultado)) {
        					$this->_helper->FlashMessenger( array('error' => htmlentities('E-mail j� existe em nosso banco de dados, por favor informe outro e tente novamente.') ) );
	    					$valid=false;
	    				}
    				}
	    			*/
    			} else {
        			$select = $dbAdapter->select()->from('usuario',array('id_cliente'))->where('usuario LIKE "'.$Udados['usuario'].'"');
    				$resultado = $dbAdapter->fetchRow($select);
    				if (is_array($resultado)) {
    					$this->_helper->FlashMessenger( array('error' => htmlentities('Usu�rio j� existe em nosso banco de dados, por favor informe outro e tente novamente.') ) );
    					$valid=false;
    				}
    				/*
    				if ($dados['email']!='') {
	        			$select = $dbAdapter->select()->from('cliente',array('id_cliente'))->where('email LIKE "'.$dados['email'].'"');
	    				$resultado = $dbAdapter->fetchRow($select);
	        			if (is_array($resultado)) {
	        				$this->_helper->FlashMessenger( array('error' => htmlentities('E-mail j� existe em nosso banco de dados, por favor informe outro e tente novamente.') ) );
	    					$valid=false;
	    				}
    				}
    				*/
    			}

    			//VERIFICANDO FOTO
    			$newFoto = isset($_FILES['foto'])?$_FILES['foto']['name']:'';
    			if ($newFoto!='') {
    				$libFile = new Porto80_Arquivo();
	    			if ($id_cliente>0) {
		    			$select = $dbAdapter->select()->from('cliente',array('foto'))->where('id_cliente='.$id_cliente);
		    			$resultadoC = $dbAdapter->fetchRow($select);
	    				$oldFoto = $resultadoC['foto'];
	    				$cmdFile = $libFile->isFile($oldFoto,'avatar');
	    				if ($cmdFile['status']==1) {
							$cmd = $libFile->delete($oldFoto,'avatar');
						} else {
							$this->_helper->FlashMessenger( array('error' => htmlentities($cmdFile['mensagem']) ) );
						}
	    			}
					//FAZENDO UPLOAD DA FOTO
					$cmd = $libFile->upload('foto','avatar');
		    		if ($cmd['status']==1) {
		    			$newFoto = $cmd['mensagem'];
		    			$redim = $libFile->sizeImg($newFoto,'avatar',250,250);
		    			$dados['foto']=$newFoto;
		    		} else {
		    			$this->_helper->FlashMessenger( array('error' => htmlentities($cmd['mensagem']) ) );
		    		}
    			}
				
    			if ($valid) {
    				if (isset($Udados['senha'])){
    					$Udados['senha']=SHA1($Udados['senha']);
    				}
    			
			    	if ($id_cliente<=0) {
			    		//INCLUINDO REGISTRO
			    		$insert= $dbAdapter->insert('cliente',$dados);
			    		$lastID = $dbAdapter->lastInsertId('cliente','id_cliente');
			    		//INCLUINDO USU�RIO
			    		$Udados['id_cliente']=$lastID;
			    		$Udados['role']='cliente';
						$insert= $dbAdapter->insert('usuario',$Udados);
			    		$this->_helper->FlashMessenger( array('success' => htmlentities('Registro inclu�do!') ) );
			    	} else {
			    		//ATUALIZANDO REGISTRO
			    		$update = $dbAdapter->update('cliente',$dados,'id_cliente='.$id_cliente);
			    		$insert= $dbAdapter->update('usuario',$Udados,'id_cliente='.$id_cliente);
			    		$this->_helper->FlashMessenger( array('success' => htmlentities('Registro atualizado!') ) );
			    		$lastID = $id_cliente;
			    	}
			    	
			    	/*
			    	//ADICIONANDO REVENDA ANJO (TEMPORARIO)
			    	$delete = $dbAdapter->delete('cliente_revenda','id_cliente='.$lastID);
			    	$insert = $dbAdapter->insert('cliente_revenda',array('id_cliente'=>$lastID,'id_produto_revenda'=>1));
			    	*/
			    	
			    	//ADICIONANDO GRUPOS
			    	$delete = $dbAdapter->delete('cliente_grupo_produto','id_cliente='.$lastID);
					$id_grupos = $this->_request->getPost('id_grupos', 0);
					foreach($id_grupos as $id_grupo) {
						$arr = explode(',',$id_grupo);
						$id_produto_linha = $arr[0];
						$id_cliente_grupo = $arr[1];
						$insert = $dbAdapter->insert('cliente_grupo_produto',array('id_cliente'=>$lastID,'id_produto_linha'=>$id_produto_linha,'id_cliente_grupo'=>$id_cliente_grupo));	
					}
					
					//VINCULANDO REVENDAS
					$delete = $dbAdapter->delete('cliente_revenda','id_cliente='.$lastID);
					$clientes_revendas = isset($_POST['cliente_revenda'])?$_POST['cliente_revenda']:'';
					if (is_array($clientes_revendas) && count($clientes_revendas)>0) {
						foreach($clientes_revendas as $revenda=>$codigo) {
							if (!empty($codigo)) {
								$insert = $dbAdapter->insert('cliente_revenda',array('id_cliente'=>$lastID,'id_produto_revenda'=>$revenda,'codigo'=>$codigo));
							}							
						}
					}
			    	
			    	$this->_helper->redirector('pesquisar','admin-cliente');
	    		} else {
	    			if ($id_cliente<=0) {
	    				$this->_helper->redirector('novo','admin-cliente');
	    			} else {
	    				$this->_helper->redirector('editar','admin-cliente',null,array('id'=>$id_cliente));
	    			}
	    		}
					
			} catch (Zend_Db_Exception $e) {
				$this->_helper->FlashMessenger( array('error' => htmlentities($e->getMessage()) ) );
				$this->_helper->redirector('pesquisar','admin-cliente');
			}
			
		}
    	
    }
    
    public function excluirAction() {
    	
        $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
    	
    	try {
    		
    		$dbAdapter = Zend_Db_Table::getDefaultAdapter ();
    		
        	$id = $this->_request->getParam('id');
    		
	    	if (!$id) {
	    		$this->_helper->FlashMessenger( array('warning' => htmlentities('Aten��o! ID incorreto, tente novamente.') ) );
	    	} else {
	    		
    			$select = $dbAdapter->select()->from('cliente',array('foto'))->where('id_cliente='.$id);
    			$resultadoC = $dbAdapter->fetchRow($select);
    			
    			//REMOVENDO REGISTROS
	    		$delete = $dbAdapter->delete('cliente','id_cliente = '.$id);
	    		
	    		//REMOVENDO VINCULO COM REVENDAS
	    		$delete = $dbAdapter->delete('cliente_revenda','id_cliente = '.$id);
	    		
	    		//REMOVENDO AVATAR
    			$libFile = new Porto80_Arquivo();
    			$oldFoto = $resultadoC['foto'];
    			$cmdFile = $libFile->isFile($oldFoto,'avatar');
    			if ($cmdFile['status']==1) {
					$cmd = $libFile->delete($oldFoto,'avatar');
				}
	    		//MENSAGEM DE SUCESSO
	    		$this->_helper->FlashMessenger( array('success' => htmlentities('Registro exclu�do!') ) );
	    	}
    			
    	} catch (Zend_Db_Exception $e) {
    		$this->_helper->FlashMessenger( array('warning' => htmlentities( $e->getMessage()) ) );
    	}
		
    	$this->_helper->redirector('pesquisar','admin-cliente');
    	
    }
    
}