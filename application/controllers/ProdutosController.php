<?php
class ProdutosController extends Zend_Controller_Action {

    public function init() {
        /* Initialize action controller here */
    }

    public function indexAction() {
        try {

            $dbAdapter = Zend_Db_Table::getDefaultAdapter();
            //SELECIONAR AS REVENDAS
            $select = $dbAdapter->select()->from(array('PR' => 'produto_revenda'))->joinInner(array('PL' => 'produto_linha'), 'PR.id_produto_revenda=PL.id_produto_revenda', array())->joinInner(array('PC' => 'produto_categoria'), 'PL.id_produto_linha=PC.id_produto_linha', array())->joinInner(array('P' => 'produto'), 'PC.id_produto_categoria=P.id_produto_categoria', array())->group('PL.id_produto_revenda')->order('PR.ordem ASC');
            //VERIFICA SE POSSUI CLIENTE LOGADO
            $id_usuario = isset($_COOKIE['adm_id_usuario']) && $_COOKIE['adm_id_usuario'] > 0 ? $_COOKIE['adm_id_usuario'] : (isset($this->view->auth->id_usuario) ? $this->view->auth->id_usuario : 0);
            if (!empty($id_usuario)) {
                $select->joinInner(array('CR' => 'cliente_revenda'), 'PL.id_produto_revenda=CR.id_produto_revenda', array())->joinInner(array('U' => 'usuario'), 'U.id_cliente=CR.id_cliente', array())->where('U.id_usuario="' . $id_usuario . '"');
            }
            $this->view->revendas = $dbAdapter->fetchAll($select);
        }
        catch(Zend_Db_Exception $e) {

            $this->_helper->FlashMessenger(array('warning' => htmlentities($e->getMessage())));
            $this->_helper->redirector('index', 'index');
        }
    }

    public function exibirAction() {
        $revenda = $this->_request->getParam('revenda', '');
        $linha = $this->_request->getParam('linha');
        $categoria = $this->_request->getParam('categoria');

        if (empty($revenda)) {
            $this->_helper->redirector('index');
        }
        //CARREGAR BANCO DE DADOS
        try {

            $dbAdapter = Zend_Db_Table::getDefaultAdapter();
            //SELECIONAR INFORMA��ES DA REVENDA
            $select = $dbAdapter->select()->from('produto_revenda')->where('permlink="' . $revenda . '"')->limit(1);
            $this->view->revenda = $dbAdapter->fetchRow($select);
            $id_produto_revenda = $this->view->revenda['id_produto_revenda'];

                $id_usuario = isset($_COOKIE['adm_id_usuario']) && $_COOKIE['adm_id_usuario'] > 0 ? $_COOKIE['adm_id_usuario'] : (isset($this->view->auth->id_usuario) ? $this->view->auth->id_usuario : 0);
				if (!empty($id_usuario)) {
	                //VERIFICANDO LOCAL DO CLIENTE SELECIONADO
	                $select_cliente = $dbAdapter->select()->from('usuario', array('id_cliente', 'local_padrao'))->where('id_usuario="' . $id_usuario . '"');
	                $rs_cliente = $dbAdapter->fetchRow($select_cliente);
	                $id_cliente = $rs_cliente['id_cliente'];
				} else {
					$id_cliente = 0;
				}
				$local_padrao = isset($rs_cliente['local_padrao']) ? $rs_cliente['local_padrao'] : 'SP';

            //SELECIONAR AS LINHAS
            $select = $dbAdapter->select()->from(array('PL' => 'produto_linha'))
            ->joinInner(array('PC' => 'produto_categoria'), 'PL.id_produto_linha=PC.id_produto_linha', array())
            ->joinInner(array('P' => 'produto'), 'PC.id_produto_categoria=P.id_produto_categoria', array())
            ->where('id_produto_revenda=' . $id_produto_revenda)
            ->group('PL.id_produto_linha')->order('PL.ordem ASC');
			if (!empty($id_cliente)) {
				$select->joinInner(array('CGP' => 'cliente_grupo_produto'), 'CGP.id_produto_linha=PL.id_produto_linha AND CGP.id_cliente='.$id_cliente, array());
			}
			
            $this->view->linhas = $dbAdapter->fetchAll($select);
            //VERIFICA SE EXISTE UMA LINHA PARA EXIBIR AS CATEGORIAS
            if ($linha != '') {
                $select = $dbAdapter->select()->from(array('PC' => 'produto_categoria'), array('PC.titulo', 'PC.permlink'))
                ->joinInner(array('PL' => 'produto_linha'), 'PL.id_produto_linha=PC.id_produto_linha', array('id_produto_linha'))
                ->joinInner(array('P' => 'produto'), 'P.id_produto_categoria=PC.id_produto_categoria', array())
                ->group('PC.id_produto_categoria')
                ->where('PL.permlink="' . $linha . '"')
                ->order('PC.titulo ASC');
                //echo $select;
                $this->view->categorias = $dbAdapter->fetchAll($select);
                $this->view->linha = $linha;
                $select = $dbAdapter->select()->from('produto_linha')->where('permlink="' . $linha . '"')->limit(1);
                $row_linha = $dbAdapter->fetchRow($select);
                //PEGA O ID DA LINHA
                $this->view->linha_banner = !empty($row_linha['foto']) ? $row_linha['foto'] : '';
                $id_produto_linha = $row_linha['id_produto_linha'];
            }
            //VERIFICA SE EXISTE UMA CATEGORIA, ENT�O EXIBIR PRODUTOS
            if ($categoria != '') {
                //PEGA O ID DA CATEGORIA
                $select = $dbAdapter->select()->from('produto_categoria')
                ->where('id_produto_linha=' . $id_produto_linha . ' AND permlink="' . $categoria . '"')->limit(1);
                $id_produto_categoria = $dbAdapter->fetchOne($select);
/*
                $id_usuario = isset($_COOKIE['adm_id_usuario']) && $_COOKIE['adm_id_usuario'] > 0 ? $_COOKIE['adm_id_usuario'] : (isset($this->view->auth->id_usuario) ? $this->view->auth->id_usuario : 0);
                //VERIFICANDO LOCAL DO CLIENTE SELECIONADO
                $select_cliente = $dbAdapter->select()->from('usuario', array('id_cliente', 'local_padrao'))->where('id_usuario="' . $id_usuario . '"');
                $rs_cliente = $dbAdapter->fetchRow($select_cliente);
                $id_cliente = $rs_cliente['id_cliente'];
                $local_padrao = isset($rs_cliente['local_padrao']) ? $rs_cliente['local_padrao'] : 'SP';
*/
                $select = $dbAdapter->select()->from(array('P' => 'produto'))
                ->joinInner(array('PC' => 'produto_categoria'), 'PC.id_produto_categoria=P.id_produto_categoria', array())
                ->joinInner(array('PE' => 'produto_extra'), 'P.id_produto=PE.id_produto AND PE.local="' . $local_padrao . '"', array('local', 'frete', 'margem', 'desconto', 'preco', 'iva', 'ipi', 'icms_origem', 'icms_origem'))
                ->joinLeft(array('E' => 'produto_embalagem'), 'E.id_produto_embalagem=P.id_produto_embalagem', array('embalagem_titulo' => 'E.titulo'))->where('PC.id_produto_categoria="' . $id_produto_categoria . '" AND status=1')
                ->order('P.grupo_ordem ASC')->order('P.produto_ordem ASC')->order('P.titulo ASC')->order('E.ordem ASC');
                $produtos = $dbAdapter->fetchAll($select);
				
				//CASO NÃO EXISTA, TALVEZ SEJA POR QUE NÃO TEM LOCAL PADRÃO CORRETO PARA ESSA REVENDA, ENTÃO PEGAR O PRIMEIRO DISPONÍVEL
				if (empty($produtos)) {
	                $select = $dbAdapter->select()->from(array('P' => 'produto'))
	                ->joinInner(array('PC' => 'produto_categoria'), 'PC.id_produto_categoria=P.id_produto_categoria', array())
	                ->joinInner(array('PE' => 'produto_extra'), 'P.id_produto=PE.id_produto', array('local', 'frete', 'margem', 'desconto', 'preco', 'iva', 'ipi', 'icms_origem', 'icms_origem'))
	                ->joinLeft(array('E' => 'produto_embalagem'), 'E.id_produto_embalagem=P.id_produto_embalagem', array('embalagem_titulo' => 'E.titulo'))->where('PC.id_produto_categoria="' . $id_produto_categoria . '" AND status=1')
	                ->group('P.id_produto')->order('P.grupo_ordem ASC')->order('P.produto_ordem ASC')->order('P.titulo ASC')->order('E.ordem ASC');
					$produtos = $dbAdapter->fetchAll($select);
					//PEGA O LOCAL PADRÃO DESSES NOVOS PRODUTOS
					if (!empty($produtos)) {
						$local_padrao = $produtos[0]['local'];
					}
				}
				
                $this->view->categoria = $categoria;
            } else {
                $id_produto_categoria = 0;
                $produtos = array();
            }

            $bloco_produtos = '';
            if (is_array($produtos) && count($produtos) > 0) {
                //ARRUMANDO PRODUTOS PARA AGRUPAR
                foreach ($produtos as $produto) {
                    $desconto = 'PRAZO';
                    //VERIFICA SE ESSE PRODUTO J� EST� NO CARRINHO
                    $select = $dbAdapter->select()->from('carrinho', 'quantidade')->where('id_usuario="' . $id_usuario . '" AND id_produto="' . $produto['id_produto'] . '"');
                    $quantidade = $dbAdapter->fetchOne($select);
                    if (!isset($quantidade) || empty($quantidade) || !is_numeric($quantidade)) $quantidade = 0;
                    $produto_valor = Porto80_Core::getProdutoValor($produto['id_produto'], $local_padrao, $id_cliente, $desconto);
                    $produto['vlrliquido'] = Porto80_Core::converteFloat($produto_valor['liquido'], 2);
                    $produto['vlrfinal'] = Porto80_Core::converteFloat($produto_valor['final'], 2);
                    $produto['vlrmargem'] = Porto80_Core::converteFloat($produto_valor['margem'], 2);
                    $produto['quantidade'] = $quantidade;
                    $bloco_produtos[$produto['grupo']][$produto['titulo']][] = $produto;
                }
            } else {
                $bloco_produtos = array();
            }
            $this->view->produtos = $bloco_produtos;
        }
        catch(Zend_Db_Exception $e) {

            $this->_helper->FlashMessenger(array('warning' => htmlentities($e->getMessage())));
            $this->_helper->redirector('index', 'index');
        }
    }
}

