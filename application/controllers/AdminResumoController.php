<?php

class AdminResumoController extends Zend_Controller_Action
{

    public function init() { 
    
    }

    public function indexAction()
    {
    	
    	try {	
    	
			$dbAdapter = Zend_Db_Table::getDefaultAdapter ();
			
			/* CLIENTE */
			$select_pendente = $dbAdapter->select()->from(array('C'=>'cliente'), array('total'=>'COUNT(C.id_cliente)'))->joinInner(array('U'=>'usuario'),'U.id_cliente=C.id_cliente')->where('U.status=0');
			$select_total = $dbAdapter->select()->from(array('C'=>'cliente'), array('total'=>'COUNT(C.id_cliente)'))->joinInner(array('U'=>'usuario'),'U.id_cliente=C.id_cliente')->where('U.status=1');
			
			$rs_pendente = $dbAdapter->fetchOne($select_pendente);
			$rs_total = $dbAdapter->fetchOne($select_total);
			
			$this->view->cliente = array(
				'pendente'=>$rs_pendente,
				'total'=>$rs_total			
			);
			
			/* CONTATO */
			$select_naolido = $dbAdapter->select()->from(array('C'=>'contato'), array('total'=>'COUNT(C.id_contato)'))->where('C.status=0');
			$select_lido = $dbAdapter->select()->from(array('C'=>'contato'), array('total'=>'COUNT(C.id_contato)'))->where('C.status=1');
			$select_respondido = $dbAdapter->select()->from(array('C'=>'contato'), array('total'=>'COUNT(C.id_contato)'))->where('C.status=2');
			
			$rs_naolido = $dbAdapter->fetchOne($select_naolido);
			$rs_lido = $dbAdapter->fetchOne($select_lido);
			$rs_respondido = $dbAdapter->fetchOne($select_respondido);
			
			$this->view->contato = array(
				'naolido'=>$rs_naolido,
				'lido'=>$rs_lido,
				'respondido'=>$rs_respondido			
			);
			
			/* PRODUTOS */
			$select_linha = $dbAdapter->select()->from('produto_linha', array('total'=>'COUNT(id_produto_linha)'));
			$select_categoria = $dbAdapter->select()->from('produto_categoria', array('total'=>'COUNT(id_produto_categoria)'));
			$select_produto = $dbAdapter->select()->from('produto', array('total'=>'COUNT(id_produto)'));
			
			$rs_linha = $dbAdapter->fetchOne($select_linha);
			$rs_categoria = $dbAdapter->fetchOne($select_categoria);
			$rs_produto = $dbAdapter->fetchOne($select_produto);
			
			$this->view->produto = array(
				'linha'=>$rs_linha,
				'categoria'=>$rs_categoria,
				'produto'=>$rs_produto			
			);
			
			/* PEDIDO */
			$select_pendente = $dbAdapter->select()->from(array('P'=>'pedido'), array('total'=>'COUNT(P.id_pedido)'))->where('P.status=1');
			$select_cancelado = $dbAdapter->select()->from(array('P'=>'pedido'), array('total'=>'COUNT(P.id_pedido)'))->where('P.status=2');
			$select_concluido = $dbAdapter->select()->from(array('P'=>'pedido'), array('total'=>'COUNT(P.id_pedido)'))->where('P.status=3');
			
			$rs_pendente = $dbAdapter->fetchOne($select_pendente);
			$rs_cancelado = $dbAdapter->fetchOne($select_cancelado);
			$rs_concluido = $dbAdapter->fetchOne($select_concluido);
			
			$this->view->pedido = array(
				'pendente'=>$rs_pendente,
				'cancelado'=>$rs_cancelado,
				'concluido'=>$rs_concluido			
			);
    	
    	} catch (Zend_Db_Exception $e) {
			
			$this->_helper->FlashMessenger( array('warning' => htmlentities( $e->getMessage()) ) );
			$this->_helper->redirector('index','admin-index');
			
		}
    	
    }

}

