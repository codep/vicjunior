<?php

class PedidosController extends Zend_Controller_Action
{

    public function init()
    { }

    public function indexAction() {

    	$this->_helper->redirector('exibir','pedidos', null, array('m'=>date('n'),'a'=>date('y')));
    
    }

    public function exibirAction() {
    	
    	$mes = $this->_request->getParam('m');
    	$ano = $this->_request->getParam('a');
		$revenda = $this->_request->getParam('rev','');
    	
    	if (empty($mes) || !is_numeric($mes)) $mes = date('n');
    	if (empty($ano) || !is_numeric($ano)) $ano = date('Y');
    	
    	$this->view->ano = $ano;
    	$this->view->mes = $mes;
    	
    	$dateDB_ini = date('Y-m-d', mktime(0,0,0,$mes,1,$ano));
    	$dateDB_fim = date('Y-m-d', mktime(24,59,29,($mes+1),0,$ano));
    	
		try {
			
			$id_usuario = isset($_COOKIE['adm_id_usuario']) && $_COOKIE['adm_id_usuario'] > 0 ? $_COOKIE['adm_id_usuario'] : (isset($this->view->auth->id_usuario) ? $this->view->auth->id_usuario : 0);
				
			$dbAdapter = Zend_Db_Table::getDefaultAdapter ();
			
	        //SELECIONAR AS REVENDAS
	        $select = $dbAdapter->select()->from(array('PR' => 'produto_revenda'))->joinInner(array('PL' => 'produto_linha'), 'PR.id_produto_revenda=PL.id_produto_revenda', array())->joinInner(array('PC' => 'produto_categoria'), 'PL.id_produto_linha=PC.id_produto_linha', array())->joinInner(array('P' => 'produto'), 'PC.id_produto_categoria=P.id_produto_categoria', array())->group('PL.id_produto_revenda')->order('PR.ordem ASC');
	        if (!empty($id_usuario)) {
	            $select->joinInner(array('CR' => 'cliente_revenda'), 'PL.id_produto_revenda=CR.id_produto_revenda', array())->joinInner(array('U' => 'usuario'), 'U.id_cliente=CR.id_cliente', array())->where('U.id_usuario="' . $id_usuario . '"');
	        }
	        $this->view->revendas = $dbAdapter->fetchAll($select);
			if (empty($revenda)) {
				$revenda = reset($this->view->revendas);
			} else {
	        	$select_rev = $dbAdapter->select()->from('produto_revenda')->where('permlink="'.$revenda.'"');
	        	$revenda = $dbAdapter->fetchRow($select_rev);
			}
			$this->view->revenda = $revenda;
			$this->view->rev = $revenda['permlink'];

			//PEDIDOS
			$select = $dbAdapter->select()->from(array('P'=>'pedido'))
			->where('P.id_usuario='.$id_usuario.' AND P.datahora>="'.$dateDB_ini.'" AND P.datahora<="'.$dateDB_fim.'"')
			->where('P.id_produto_revenda='.$revenda['id_produto_revenda']);
			$pedidos = $dbAdapter->fetchAll($select);
			
			foreach($pedidos as $key => $pedido) {
				
				$select = $dbAdapter->select()->from(array('PP'=>'pedido_produto'))
				->joinInner(array('P'=>'produto'),'P.id_produto=PP.id_produto')
				->joinInner(array('PE'=>'produto_embalagem'),'PE.id_produto_embalagem=P.id_produto_embalagem', array('embalagem'=>'PE.titulo'))
				->where('PP.id_pedido='.$pedido['id_pedido']);
				$produtos = $dbAdapter->fetchAll($select);
				
				foreach($produtos as $produto) {
					
					$pedido['produtos'][] = array(
						'codigo'=>$produto['codigo'],
						'titulo'=>$produto['titulo'],
						'embalagem'=>$produto['embalagem'],					
						'quantidade'=>$produto['quantidade'],
						'foto'=>$produto['foto'],
						'valor_liquido'=>$produto['valor_liquido'],
						'valor_unitario'=>$produto['valor_unitario']
					);
					
				}
				
				$pedidos[$key] = $pedido;
				
			}
			
			$select = $dbAdapter->select()->from(array('P'=>'pedido'),array('ano'=>'YEAR(datahora)'))->where('P.id_usuario='.$this->view->auth->id_usuario)->group('ano');
			$anos_validos = $dbAdapter->fetchAll($select);
			if (empty($anos_validos)) $anos_validos[] = array('ano'=>date('Y'));
			$this->view->anos_validos = $anos_validos;
			
			$this->view->pedidos = $pedidos;
    	
    	} catch (Zend_Db_Exception $e) {
			echo $e->getMessage(); die();
			$this->_helper->FlashMessenger( array('warning' => htmlentities( $e->getMessage()) ) );
			$this->_helper->redirector('index','pedidos');
			
		}
    	
		

    	
    }

}

