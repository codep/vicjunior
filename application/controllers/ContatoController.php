<?php

class ContatoController extends Zend_Controller_Action
{

    public function init() {}

    public function indexAction() {

    	try {
    		
    		$dbAdapter = Zend_Db_Table::getDefaultAdapter ();
    		
			//SELECIONAR OS TEXTOS
	        $select = $dbAdapter->select()->from(array('P'=>'parametros'),array('param_telefone','param_fax','param_email_publico','param_endereco','param_mapa_google'))->where('status=1')->limit(1);
	    	$this->view->contato = $dbAdapter->fetchRow($select);
    		
    	} catch (Zend_Db_Exception $e) {
			
			$this->_helper->FlashMessenger( array('warning' => htmlentities( $e->getMessage()) ) );
			$this->_helper->redirector('index','index');
			
		}
    
    }
    
    public function enviarAction() {
    	
        $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		
		$response['status']=0;
    	
    	$dados = array(
    		'nome' => $this->_request->getPost('name',''),
    		'email' => $this->_request->getPost('email',''),
    		'empresa' => $this->_request->getPost('empresa',''),
    		'telefone' => $this->_request->getPost('telefone'),    	
    		'mensagem' => $this->_request->getPost('message','')
    	);
    	
    	if ($dados['nome']=='' || $dados['email']=='' || $dados['mensagem']=='') {
    		$response['mensagem']="Os campos Nome, E-mail e Mensagem s�o obrigat�rios";
    	} else {
    	
	    	try {
				
				$dbAdapter = Zend_Db_Table::getDefaultAdapter ();
				
	        	$select = $dbAdapter->select()->from(array('P'=>'parametros'),array('param_email_contato'))->where('status=1')->limit(1);
	    		$email_contato = $dbAdapter->fetchOne($select);
				
				$dados['data_envio'] = date('Y-m-d H:i:s');
				$response['mensagem'] = rawurlencode($response['mensagem']);
				$insert = $dbAdapter->insert('contato',$dados);
		    	
		    	$response['status']=1;
				$response['mensagem']="Mensagem enviada com sucesso. Em breve retornaremos o contato.";
				
				//ENVIAR C�PIA NO E-MAIL DE CONTATO
				$corpo  = "Voc&ecirc; recebeu um contato atrav&eacute;s do site vicjunior.com.br: <br>";
				$corpo .= "Nome: " . $dados['nome'] . "<br>";
				$corpo .= "E-mail: " . $dados['email'] . "<br>";
				$corpo .= "Empresa: " . $dados['empresa'] . "<br>";
				$corpo .= "Telefone: " . $dados['telefone'] . "<br>";
				$corpo .= "Mensagem: " . $dados['mensagem'] . "<br>";
				$corpo .= $dados['data_envio'] . "<br>";
			   
				$mail = new Zend_Mail();
				$mail->setSubject('[CONTATO] vicjunior.com.br');
				$mail->addTo($email_contato,'vicjunior.com.br');
				$mail->setBodyHtml($corpo);
				$mail->send();
					
			} catch (Zend_Db_Exception $e) {
				$response['mensagem']=$e->getMessage();
			}
			
    	}

		$response['mensagem']=rawurlencode($response['mensagem']);
		echo json_encode($response);
    }

}

