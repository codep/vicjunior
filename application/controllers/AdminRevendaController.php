<?php

class AdminRevendaController extends Zend_Controller_Action
{

    public function init()
    {
    }

    public function indexAction()
    {
		$this->_helper->redirector('pesquisar','admin-revenda');
    }
    
    public function pesquisarAction() {
    	
        $pagina = $this->_request->getParam('pagina', 1) ;
        
        try {
        	
			$dbAdapter = Zend_Db_Table::getDefaultAdapter ();
			
			$select = $dbAdapter->select()->from(array('PR'=>'produto_revenda'))
			->joinLeft(array('PL'=>'produto_linha'),'PR.id_produto_revenda=PL.id_produto_revenda',array())
			->joinLeft(array('PC'=>'produto_categoria'),'PL.id_produto_linha=PC.id_produto_linha',array())
			->joinLeft(array('P'=>'produto'),'PC.id_produto_categoria=P.id_produto_categoria',array())
			->group('PL.id_produto_revenda')->order('PR.ordem ASC');
	        
	        $result = $dbAdapter->fetchAll($select);
	    	
		    /* PAGINACAO */
		    $dados = Zend_Paginator::factory($result);
		    $dados->setCurrentPageNumber( intval($pagina) );
		    $dados->setItemCountPerPage(20);
	        $this->view->dados = $dados;
        	        	
        } catch (Zend_Db_Exception $e) {
        	$this->_helper->FlashMessenger( array('warning' => htmlentities($e->getMessage()) ) );
        	$this->_helper->redirector('index','admin-resumo');
        }
        
        /* ARMAZENA A URL DE PESQUISA */
    	$filtros = new Zend_Session_Namespace('filtros');
    	$keyfiltro = md5($this->_request->getControllerName().'_'.$this->_request->getActionName());
		$filtros->$keyfiltro = $this->view->url();
		$this->view->keyfiltro = $keyfiltro;
    	
    }
    
    public function novoAction()
    {
    	$this->view->headScript()
    		->appendFile( $this->view->baseUrl('/admin_js/jquery-custom-file-input.js'),'text/javascript')
			->appendFile( $this->view->baseUrl('/admin_js/jquery.validate_pack.js'),'text/javascript')
			->appendFile( $this->view->baseUrl('/admin_js/jquery.tipTip.min.js'),'text/javascript')
		;
		
		try {
			
			//$dbAdapter = Zend_Db_Table::getDefaultAdapter ();
			
			
		} catch (Zend_Db_Exception $e) {
        	$this->_helper->FlashMessenger( array('warning' => htmlentities($e->getMessage()) ) );
        	$this->_helper->redirector('pesquisar','admin-revenda');
		}
		
		/* MANIPULA URL DE RETORNO */
    	$filtros = new Zend_Session_Namespace('filtros');
    	$keyfiltro = $this->_request->getParam('b');
    	$this->view->keyfiltro = $keyfiltro;
    	if (!empty($keyfiltro) && isset($filtros->$keyfiltro)) {
    		$this->view->url_back=$filtros->$keyfiltro;
    	} else {
    		$this->view->url_back=$this->view->baseUrl('admin-revenda/pesquisar');
    	}
		
		$this->_helper->viewRenderer->render('formulario');
		
    }
    
    public function editarAction() {
    	
    	$this->view->headScript()
    		->appendFile( $this->view->baseUrl('/admin_js/jquery-custom-file-input.js'),'text/javascript')
			->appendFile( $this->view->baseUrl('/admin_js/jquery.validate_pack.js'),'text/javascript')
			->appendFile( $this->view->baseUrl('/admin_js/tiny_mce/jquery.tinymce.js'),'text/javascript')
			->appendFile( $this->view->baseUrl('/admin_js/jquery.maskedinput-1.2.2.min.js'),'text/javascript')
			->appendFile( $this->view->baseUrl('/admin_js/jquery.tipTip.min.js'),'text/javascript')
		;

		try {
			
			$id_produto_revenda = $this->_request->getParam('id');
						
			$dbAdapter = Zend_Db_Table::getDefaultAdapter ();
			
			/* REVENDA */
			$select = $dbAdapter->select()->from(array('PR'=>'produto_revenda'))->where('PR.id_produto_revenda='.$id_produto_revenda)->order('PR.titulo ASC')->limit(1);
			$dados = $dbAdapter->fetchRow($select);
			$dados['pedido_valor_minimo']=intVal($dados['pedido_valor_minimo']);
			$this->view->dados = $dados;
						
		} catch (Zend_Db_Exception $e) {
        	$this->_helper->FlashMessenger( array('warning' => htmlentities($e->getMessage()) ) );
        	$this->_helper->redirector('pesquisar','admin-revenda');
		}
		
		/* MANIPULA URL DE RETORNO */
    	$filtros = new Zend_Session_Namespace('filtros');
    	$keyfiltro = $this->_request->getParam('b');
    	$this->view->keyfiltro = $keyfiltro;
    	if (!empty($keyfiltro) && isset($filtros->$keyfiltro)) {
    		$this->view->url_back=$filtros->$keyfiltro;
    	} else {
    		$this->view->url_back=$this->view->baseUrl('admin-revenda/pesquisar');
    	}
    	
    	/* ENVIA SAIDA DE DADOS */
    	$this->_helper->viewRenderer->render('formulario');
    	
    }
    
    public function salvarAction() {
    	
        $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);    	
    	
		if ($this->_request->isPost()) {
			
			$id_produto_revenda = $this->_request->getPost('id_produto_revenda', 0);
			
			try {
				
				$dbAdapter = Zend_Db_Table::getDefaultAdapter ();
				
    			$dados = array(
    				'titulo'=>			htmlentities($this->getRequest()->getPost('titulo')),
					'permlink'=>		Porto80_Core::url_amigavel($this->getRequest()->getPost('titulo')),
					'pedido_valor_minimo'=>$this->getRequest()->getPost('pedido_valor_minimo'),
	    			'status'=>			$this->getRequest()->getPost('status')
    			);
    			
    			$valid=true;

    			//VERIFICANDO FOTO
    			$newFoto = isset($_FILES['foto'])?$_FILES['foto']['name']:'';
    			if (!empty($newFoto)) {
    				$libFile = new Porto80_Arquivo();
	    			if ($id_produto_revenda>0) {
		    			$select = $dbAdapter->select()->from('produto_revenda',array('logotipo'))->where('id_produto_revenda='.$id_produto_revenda);
		    			$resultadoC = $dbAdapter->fetchRow($select);
	    				$oldFoto = $resultadoC['logotipo'];
	    				if ($resultadoC['logotipo']!='') {
		    				$cmdFile = $libFile->isFile($oldFoto,'revenda');
		    				if ($cmdFile['status']==1) {
								$cmd = $libFile->delete($oldFoto,'revenda');
							} else {
								$this->_helper->FlashMessenger( array('error' => htmlentities($cmdFile['mensagem']) ) );
							}
	    				}
	    			}
					//FAZENDO UPLOAD DA FOTO
					$cmd = $libFile->upload('foto','revenda');
		    		if ($cmd['status']==1) {
		    			$newFoto = $cmd['mensagem'];
		    			$redim = $libFile->sizeImg($newFoto,'revenda',250,250);
		    			$dados['logotipo']=$newFoto;
		    		} else {
		    			$this->_helper->FlashMessenger( array('error' => htmlentities($cmd['mensagem']) ) );
		    		}
    			} else {
    				$delfoto = $this->getRequest()->getPost('delfoto','');
    				if (!empty($delfoto)) {
    					$dados['logotipo']=''; //LIMPAR FOTO
    					//EXCLUINDO FOTO ATUAL
						$libFile = new Porto80_Arquivo();
		    			$select = $dbAdapter->select()->from('produto_revenda',array('logotipo'))->where('id_produto_revenda='.$id_produto_revenda);
		    			$resultadoC = $dbAdapter->fetchRow($select);
	    				$oldFoto = $resultadoC['logotipo'];
	    				if ($resultadoC['logotipo']!='') {
		    				$cmdFile = $libFile->isFile($oldFoto,'revenda');
		    				if ($cmdFile['status']==1) {
								$cmd = $libFile->delete($oldFoto,'revenda');
							} else {
								$this->_helper->FlashMessenger( array('error' => htmlentities($cmdFile['mensagem']) ) );
							}
	    				}
    				} else {
    					unset($dados['logoptipo']); //NAO HA FOTOS
    				}
    			}
				
    			if ($valid) {
			    	if ($id_produto_revenda<=0) {
			    		//INCLUINDO REGISTRO
			    		$insert= $dbAdapter->insert('produto_revenda',$dados);
			    		$lastID = $dbAdapter->lastInsertId('produto_revenda','id_produto_revenda');
			    		$this->_helper->FlashMessenger( array('success' => htmlentities('Registro adicionado!') ) );
			    	} else {
			    		//ATUALIZANDO REGISTRO
			    		$update = $dbAdapter->update('produto_revenda',$dados,'id_produto_revenda='.$id_produto_revenda);
			    		$this->_helper->FlashMessenger( array('success' => htmlentities('Registro atualizado!') ) );
			    		$lastID = $id_produto_revenda;
			    	}
					
    				/* MANIPULA URL DE RETORNO */
			    	$filtros = new Zend_Session_Namespace('filtros');
			    	$keyfiltro = $this->_request->getParam('b');
			    	if (!empty($keyfiltro) && isset($filtros->$keyfiltro)) {
			    		$this->view->url_back=$filtros->$keyfiltro;
			    	} else {
			    		$this->view->url_back=$this->view->baseUrl('admin-revenda/pesquisar');
			    	}
		    	
			    	//REDIRECIONA
		    		$this->_redirect('http://' . $this->getRequest()->getHttpHost() . $this->view->url_back);
	    		} else {
	    			if ($id_produto_revenda<=0) {
	    				$this->_helper->redirector('novo','admin-revenda');
	    			} else {
	    				$this->_helper->redirector('editar','admin-revenda',null,array('id'=>$id_produto_revenda));
	    			}
	    		}
					
			} catch (Zend_Db_Exception $e) {
				$this->_helper->FlashMessenger( array('error' => htmlentities($e->getMessage()) ) );
				$this->_helper->redirector('pesquisar','admin-revenda');
			}
			
		}
    	
    }
    
    public function excluirAction() {
    	
        $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
    	
    	try {
    		
    		$dbAdapter = Zend_Db_Table::getDefaultAdapter ();
    		
        	$id = $this->_request->getParam('id');
    		
	    	if (!$id) {
	    		$this->_helper->FlashMessenger( array('warning' => htmlentities('OPS! ID incorreto, tente novamente.') ) );
	    	} else {
	    		
    			$select = $dbAdapter->select()->from('produto_revenda',array('logotipo'))->where('id_produto_revenda='.$id);
    			$resultadoC = $dbAdapter->fetchRow($select);
    			
    			//REMOVENDO REGISTROS
	    		$delete = $dbAdapter->delete('produto_revenda','id_produto_revenda = '.$id);
	    		
	    		//REMOVENDO AVATAR
    			$libFile = new Porto80_Arquivo();
    			$oldFoto = $resultadoC['logotipo'];
    			$cmdFile = $libFile->isFile($oldFoto,'revenda');
    			if ($cmdFile['status']==1) {
					$cmd = $libFile->delete($oldFoto,'revenda');
				}
	    		//MENSAGEM DE SUCESSO
	    		$this->_helper->FlashMessenger( array('success' => htmlentities('Registro removido!') ) );
	    	}
    			
    	} catch (Zend_Db_Exception $e) {
    		$this->_helper->FlashMessenger( array('warning' => htmlentities( $e->getMessage()) ) );
    	}
		
    	/* MANIPULA URL DE RETORNO */
    	$filtros = new Zend_Session_Namespace('filtros');
    	$keyfiltro = $this->_request->getParam('b');
    	if (!empty($keyfiltro) && isset($filtros->$keyfiltro)) {
    		$this->view->url_back=$filtros->$keyfiltro;
    	} else {
    		$this->view->url_back=$this->view->baseUrl('admin-revenda/pesquisar');
    	}
    
    	//REDIRECIONA
    	$this->_redirect('http://' . $this->getRequest()->getHttpHost() . $this->view->url_back);
    	
    }
    
}

