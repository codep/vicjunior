<?php

class AdminNoticiaController extends Zend_Controller_Action
{

    public function init()
    {
    }

    public function indexAction()
    {
		$this->_helper->redirector('pesquisar','admin-noticia');
    }
    
    public function pesquisarAction() {
    	
        $pagina = $this->_request->getParam('pagina', 1) ;
        
        try {
        	
			$dbAdapter = Zend_Db_Table::getDefaultAdapter ();
	        
			/* FILTRO */
	        $key = $this->getRequest()->getPost('key');
	        $this->view->key = $key;
	        
	        $select = $dbAdapter->select()
	        ->from(array('P'=>'noticia'),array('P.titulo','P.id_noticia', 'P.data','categoria'=>'(SELECT GROUP_CONCAT(PC.titulo) FROM noticia_categoria PC WHERE PC.id_noticia_categoria=PCL.id_noticia_categoria)'))
	        ->joinLeft(array('PCL'=>'noticia_categoria_link'),'PCL.id_noticia=P.id_noticia',array())
	        ->order('categoria ASC')->order('P.data DESC')->order('P.titulo ASC');
	        if ($key!='') {
	        	$select->where('P.nome LIKE "%'.$key.'%"');
	        }
	        
	        $result = $dbAdapter->fetchAll($select);
	    	
		    /* PAGINA��O */
		    $dados = Zend_Paginator::factory($result);
		    $dados->setCurrentPageNumber( intval($pagina) );
		    $dados->setItemCountPerPage(20);
	        $this->view->dados = $dados;
        	
        	
        } catch (Zend_Db_Exception $e) {
        	$this->_helper->FlashMessenger( array('warning' => htmlentities($e->getMessage()) ) );
        	$this->_helper->redirector('index','admin-resumo');
        }
    	
    	
    }

    public function novaAction()
    {
    	$this->view->headScript()
			->appendFile( $this->view->baseUrl('/admin_js/jquery.validate_pack.js'),'text/javascript')
			->appendFile( $this->view->baseUrl('/admin_js/tiny_mce/jquery.tinymce.js'),'text/javascript')
			->appendFile( $this->view->baseUrl('/admin_js/jquery.maskedinput-1.2.2.min.js'),'text/javascript')
		;
		
		try {
			
			$dbAdapter = Zend_Db_Table::getDefaultAdapter ();
			
			/* CATEGORIAS */
			$select = $dbAdapter->select()->from(array('PC'=>'noticia_categoria'))->order('PC.titulo ASC');
			$categorias = $dbAdapter->fetchAll($select);
			$this->view->categorias = $categorias;
						
		} catch (Zend_Db_Exception $e) {
        	$this->_helper->FlashMessenger( array('warning' => htmlentities($e->getMessage()) ) );
        	$this->_helper->redirector('pesquisar','admin-produto');
		}
		
		$this->_helper->viewRenderer->render('formulario');
		
    }
    
    public function editarAction() {
    	
		$id = $this->_request->getParam('id');
		
    	if (!$id) {
    		$this->_helper->FlashMessenger( array('warning' => htmlentities('O registro informado n�o existe. Verifique e tente novamente.') ) );
    		$this->_helper->redirector('pesquisar','admin-noticia');
    	}
		
    	$this->view->headScript()
			->appendFile( $this->view->baseUrl('/admin_js/jquery.validate_pack.js'),'text/javascript')
			->appendFile( $this->view->baseUrl('/admin_js/tiny_mce/jquery.tinymce.js'),'text/javascript')
			->appendFile( $this->view->baseUrl('/admin_js/jquery.maskedinput-1.2.2.min.js'),'text/javascript')
		;
		
    	//CARREGAR BANCO DE DADOS		
		try {
			
			$dbAdapter = Zend_Db_Table::getDefaultAdapter ();
			
			/* CATEGORIAS */
			$select = $dbAdapter->select()->from(array('PC'=>'noticia_categoria'))->order('PC.titulo ASC');
			$categorias = $dbAdapter->fetchAll($select);
			$this->view->categorias = $categorias;
			
			/* NOTICIA */
			$select = $dbAdapter->select()->from(array('P'=>'noticia'))->joinLeft(array('PCL'=>'noticia_categoria_link'),'PCL.id_noticia=P.id_noticia',array('id_categorias'=>'GROUP_CONCAT(PCL.id_noticia_categoria)'))->where('P.id_noticia='.$id)->limit(1);			
			$dados = $dbAdapter->fetchRow($select);
			$dados['id_categorias'] = explode(',',$dados['id_categorias']);
			$this->view->dados = $dados;
				
		} catch (Zend_Db_Exception $e) {
			
			$this->_helper->FlashMessenger( array('warning' => htmlentities( $e->getMessage()) ) );
			$this->_helper->redirector('pesquisar','admin-noticia');
			
		}
    	
    	$this->_helper->viewRenderer->render('formulario');
    }
    
    public function salvarAction() {
    	
        $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);    	
    	
		if ($this->_request->isPost()) {
			
			$id_noticia = $this->_request->getPost('id_noticia', 0);
			
			try {
				
				$dbAdapter = Zend_Db_Table::getDefaultAdapter ();
				
    			$dados = array(
    				'titulo'=>			$this->getRequest()->getPost('titulo'),
    				'fonte'=>			$this->getRequest()->getPost('fonte'),
					'autor'=>			$this->getRequest()->getPost('autor'),
    				'texto'=>			htmlentities($this->getRequest()->getPost('texto')),
    				'data'=>			Porto80_Core::formatDbData($this->getRequest()->getPost('data')),
    				'perm_link'=>		Porto80_Core::url_amigavel($this->getRequest()->getPost('titulo'))
    			);
    			
    			//VERIFICA SE O PERMLINK J� EXISTE
    			$sqlPL = $dbAdapter->select()->from('noticia',array('total'=>'COUNT(perm_link)'))->where('perm_link="'.$dados['perm_link'].'"');
    			$verPL = $dbAdapter->fetchOne($sqlPL);
    			if (!empty($verPL) && $verPL>0) {
    				//VERIFICA SE O ID JA EXISTE
    				if ($id_noticia>0) $dados['perm_link']=$dados['perm_link'].'_'.$id_noticia;
    				else {
    					//VERIFICA PROXIMO ID
    					$sqlID = $dbAdapter->select()->from('noticia',array('total'=>'MAX(id_noticia)+1'))->where('perm_link="'.$dados['perm_link'].'"');
    					$nextID = $dbAdapter->fetchOne($sqlID);
    					if (empty($nextID) || !is_numeric($nextID) || $nextID<=0) $nextID=1;
    					$dados['perm_link']=$dados['perm_link'].'_'.$nextID;
    				}
    			}
    			
		    	if ($id_noticia<=0) {
		    		//INCLUINDO REGISTRO
		    		$insert= $dbAdapter->insert('noticia',$dados);
		    		$lastID = $dbAdapter->lastInsertId('noticia','id_noticia');
		    		$this->_helper->FlashMessenger( array('success' => htmlentities('Registro inclu�do!') ) );
		    	} else {
		    		//ATUALIZANDO REGISTRO
		    		$update = $dbAdapter->update('noticia',$dados,'id_noticia='.$id_noticia);
		    		$this->_helper->FlashMessenger( array('success' => htmlentities('Registro atualizado!') ) );
		    		$lastID = $id_noticia;
		    	}
		    	
		    	//VINCULANDO CATEGORIAS
    	    	$delete = $dbAdapter->delete('noticia_categoria_link','id_noticia='.$lastID);
        		$categorias = $this->_request->getPost('id_noticia_categoria', 0);
				foreach($categorias as $categoria) {
					$insert = $dbAdapter->insert('noticia_categoria_link',array('id_noticia'=>$lastID,'id_noticia_categoria'=>$categoria));
				}
		    	
		    	$this->_helper->redirector('pesquisar','admin-noticia');
					
			} catch (Zend_Db_Exception $e) {
				$this->_helper->FlashMessenger( array('error' => htmlentities($e->getMessage()) ) );
				$this->_helper->redirector('pesquisar','admin-noticia');
			}
			
		}
    	
    }
    
    public function excluirAction() {
    	
        $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
    	
    	try {
    		
    		$dbAdapter = Zend_Db_Table::getDefaultAdapter ();
    		
        	$id = $this->_request->getParam('id');
    		
	    	if (!$id) {
	    		$this->_helper->FlashMessenger( array('warning' => htmlentities('Aten��o! ID incorreto, tente novamente.') ) );
	    	} else {	    		
    			//REMOVENDO REGISTROS
	    		$delete = $dbAdapter->delete('noticia','id_noticia = '.$id);
	    		//MENSAGEM DE SUCESSO
	    		$this->_helper->FlashMessenger( array('success' => htmlentities('Registro exclu�do!') ) );
	    	}
    			
    	} catch (Zend_Db_Exception $e) {
    		$this->_helper->FlashMessenger( array('warning' => htmlentities( $e->getMessage()) ) );
    	}
		
    	$this->_helper->redirector('pesquisar','admin-noticia');
    	
    }

}

