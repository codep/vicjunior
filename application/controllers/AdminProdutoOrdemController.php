<?php

class AdminProdutoOrdemController extends Zend_Controller_Action
{

    public function init()
    {
    }

    public function indexAction()
    {
		$this->_helper->redirector('pesquisar','admin-produto-ordem');
    }
    
    public function pesquisarAction() {
    	
        try {
        	
			$dbAdapter = Zend_Db_Table::getDefaultAdapter ();
	        
	        $id_produto_categoria = $this->getRequest()->getParam('categoria');
	        $this->view->id_produto_categoria = $id_produto_categoria;
	        $dados='';
	        
	        if (!empty($id_produto_categoria)) {
	        
		        $select = $dbAdapter->select()->from(array('P'=>'produto'))
		        ->joinLeft(array('PC'=>'produto_categoria'),'PC.id_produto_categoria=P.id_produto_categoria',array('categoria'=>'PC.titulo'))
		        ->joinLeft(array('PE'=>'produto_embalagem'),'PE.id_produto_embalagem=P.id_produto_embalagem',array('embalagem'=>'PE.titulo'))
		        ->where('PC.id_produto_categoria='.$id_produto_categoria)
		        ->order('P.grupo_ordem ASC')->order('P.produto_ordem ASC')->order('P.titulo ASC');
		        
		        $resultados = $dbAdapter->fetchAll($select);
		        
		        foreach($resultados as $resultado) {
					$dados[$resultado['grupo']][] = $resultado;
		        }
	        } else {
	        	$dados=array();
	        }
	            
	        $this->view->dados = $dados;
	        
	        
			/* CATEGORIAS */
			$select = $dbAdapter->select()->from(array('PC'=>'produto_categoria'),array('categoria'=>'PC.titulo','PC.id_produto_categoria'))->joinInner(array('PG'=>'produto_linha'),'PG.id_produto_linha=PC.id_produto_linha',array('grupo'=>'PG.titulo'))->order('PG.titulo ASC')->order('PC.titulo ASC');
			$dados = $dbAdapter->fetchAll($select);
			$categorias='';
			foreach($dados as $dado) {
				$categorias[$dado['grupo']][]=array('id_produto_categoria'=>$dado['id_produto_categoria'],'titulo'=>$dado['categoria']);
			}
			if ($categorias=='') $categorias=array();
			$this->view->categorias = $categorias;
        	        	
        } catch (Zend_Db_Exception $e) {
        	$this->_helper->FlashMessenger( array('warning' => htmlentities($e->getMessage()) ) );
        	$this->_helper->redirector('index','admin-resumo');
        }
        
        /* ARMAZENA A URL DE PESQUISA */
    	$filtros = new Zend_Session_Namespace('filtros');
    	$keyfiltro = md5($this->_request->getControllerName().'_'.$this->_request->getActionName());
		$filtros->$keyfiltro = $this->view->url();
		$this->view->keyfiltro = $keyfiltro;
    	
    }
    
    public function salvarOrdemGrupoAction() {
    	
        $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		
		$status=0; $mensagem='';
    	
        try {
        	
			$dbAdapter = Zend_Db_Table::getDefaultAdapter ();
			
			$grupos = $this->getRequest()->getPost('grupo');
			$categoria = $this->getRequest()->getPost('categoria');
			
			if (is_array($grupos)) {
				foreach($grupos as $ordem => $grupo) {
					$dbAdapter->update('produto',array('grupo_ordem'=>$ordem+1),'id_produto_categoria='.$categoria.' AND grupo='.$grupo);
				}
				$status=1; $mensagem='Dados atualizados com sucesso!';
			}
    	
		} catch (Zend_Db_Exception $e) {
        	$mensagem=$e->getMessage();
        }
        
        $resultado = array('status'=>$status,'mensagem'=>rawurlencode($mensagem));
        
        echo Zend_Json::encode($resultado);
    	
    }
    
    public function salvarOrdemProdutoAction() {
    	
        $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		
		$status=0; $mensagem='';
    	
        try {
        	
			$dbAdapter = Zend_Db_Table::getDefaultAdapter ();
			
			$produtos = $this->getRequest()->getPost('produto');
			
			if (is_array($produtos)) {
				foreach($produtos as $ordem => $id_produto) {
					$dbAdapter->update('produto',array('produto_ordem'=>$ordem+1),'id_produto='.$id_produto);
				}
				$status=1; $mensagem='Dados atualizados com sucesso!';
			}
    	
		} catch (Zend_Db_Exception $e) {
        	$mensagem=$e->getMessage();
        }
        
        $resultado = array('status'=>$status,'mensagem'=>rawurlencode($mensagem));
        
        echo Zend_Json::encode($resultado);
    	
    }
    
    public function salvarGrupoProdutoAction() {
    	
        $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		
		$status=0; $mensagem='';
    	
        try {
        	
			$dbAdapter = Zend_Db_Table::getDefaultAdapter ();
			
			$grupo = $this->getRequest()->getPost('grupo',0);
			$produtos = $this->getRequest()->getPost('produto');
			
			if (!empty($produtos)) {
			
				//VERIFICA SE � UM NOVO GRUPO
				if ($grupo=="-1") {
					$grupo = $dbAdapter->query('SELECT t1.grupo + 1 FROM produto t1 WHERE NOT EXISTS (SELECT t2.grupo FROM produto t2 WHERE t2.grupo = t1.grupo + 1) LIMIT 1')->fetchColumn();
				}
				
				if (is_array($produtos)) {
					foreach($produtos as $id_produto) {
						$dbAdapter->update('produto',array('grupo'=>$grupo),'id_produto='.$id_produto);
					}
					$status=1; $mensagem='Dados atualizados com sucesso!';
				}
				
			}
    	
		} catch (Zend_Db_Exception $e) {
        	$mensagem=$e->getMessage();
        }
        
        $resultado = array('status'=>$status,'mensagem'=>rawurlencode($mensagem));
        
        echo Zend_Json::encode($resultado);
    	
    }
    
}

