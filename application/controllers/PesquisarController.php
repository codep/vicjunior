<?php

class PesquisarController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }
    
	public function indexAction()
    {
    	try {
    		
    		$this->view->key = $key = $this->_request->getPost('key','');
    		
    		if (!empty($key)) {
    			
    			$dbAdapter = Zend_Db_Table::getDefaultAdapter ();
    			
    			$id_usuario = isset($_COOKIE['adm_id_usuario'])&&$_COOKIE['adm_id_usuario']>0?$_COOKIE['adm_id_usuario']:(isset($this->view->auth->id_usuario)?$this->view->auth->id_usuario:0);
    			//VERIFICANDO LOCAL DO CLIENTE SELECIONADO
    			$select_cliente = $dbAdapter->select()->from('usuario',array('id_cliente','local_padrao'))->where('id_usuario="'.$id_usuario.'"');
    			$rs_cliente = $dbAdapter->fetchRow($select_cliente);
    			$id_cliente = $rs_cliente['id_cliente'];
    			$local_padrao = isset($rs_cliente['local_padrao'])?$rs_cliente['local_padrao']:'SP';
    			
    			$select = $dbAdapter->select()->from(array('P'=>'produto'))
    			->joinInner(array('PC'=>'produto_categoria'),'PC.id_produto_categoria=P.id_produto_categoria',array())
    			->joinInner(array('PE'=>'produto_extra'),'P.id_produto=PE.id_produto AND PE.local="'.$local_padrao.'"',array('PE.local','PE.frete','PE.margem','PE.desconto','PE.preco','PE.iva','PE.ipi','PE.icms_origem','PE.icms_destino'))
    			->joinLeft(array('E'=>'produto_embalagem'),'E.id_produto_embalagem=P.id_produto_embalagem',array('embalagem_titulo'=>'E.titulo'))
    			->where('( P.codigo LIKE "%'.$key.'%" OR P.titulo LIKE "%'.$key.'%" ) AND P.status=1')
    			->order('P.grupo_ordem ASC')->order('P.produto_ordem ASC')->order('P.titulo ASC')->order('E.ordem ASC');
    			//VERIFICA SE POSSUI CLIENTE LOGADO
    			if (isset($id_usuario)) {
    				$select->joinInner(array('PL'=>'produto_linha'),'PL.id_produto_linha=PC.id_produto_linha',array())
    				->joinInner(array('CR'=>'cliente_revenda'),'PL.id_produto_revenda=CR.id_produto_revenda',array())
    				->joinInner(array('U'=>'usuario'),'U.id_cliente=CR.id_cliente',array())
    				->where('U.id_usuario="'.$id_usuario.'"');
    			}
    			$produtos = $dbAdapter->fetchAll($select);
    			
    			$bloco_produtos='';
    			if (is_array($produtos) && count($produtos) > 0 ) {
    				//ARRUMANDO PRODUTOS PARA AGRUPAR
    				foreach($produtos as $produto) {
    					$desconto = 'PRAZO';
    					//VERIFICA SE ESSE PRODUTO J� EST� NO CARRINHO
    					$select = $dbAdapter->select()->from('carrinho','quantidade')->where('id_usuario="'.$id_usuario.'" AND id_produto="'.$produto['id_produto'].'"');
    					$quantidade = $dbAdapter->fetchOne($select);
    					if (!isset($quantidade) || empty($quantidade) || !is_numeric($quantidade)) $quantidade=0;
    					$produto_valor = Porto80_Core::getProdutoValor($produto['id_produto'], $local_padrao, $id_cliente,$desconto);
    					$produto['vlrliquido']=Porto80_Core::converteFloat($produto_valor['liquido'],2);
    					$produto['vlrfinal']=Porto80_Core::converteFloat($produto_valor['final'],2);
    					$produto['vlrmargem']=Porto80_Core::converteFloat($produto_valor['margem'],2);
    					$produto['quantidade']=$quantidade;
    					$bloco_produtos[$produto['grupo']][$produto['titulo']][]=$produto;
    				}
    			} else {
    				$bloco_produtos=array();
    			}
    			$this->view->produtos=$bloco_produtos;
    			
    		}
    		
    	} catch (Zend_Db_Exception $e) {
			$this->_helper->FlashMessenger( array('warning' => htmlentities( $e->getMessage()) ) );
			$this->_helper->redirector('index','index');
			
		}
    	
    }

}

