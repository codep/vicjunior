<?php

class AdminProdutoRelatorioController extends Zend_Controller_Action
{

    public function init()
    {
    }

    public function indexAction()
    {
		$this->_helper->redirector('pesquisar','admin-produto-relatorio');
    }
	
	public function atualizarAction() {
		
        $this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(TRUE);
		
    	try {
        	
			$dbAdapter = Zend_Db_Table::getDefaultAdapter ();
			
			//------------------------------ VERIFICANDO SE PRECISA ATUALIZAR ALGUM PRODUTO
			$produto_extra_id_produto=$this->getRequest()->getPost('produto_extra_id_produto',0);
			$produto_extra_local=$this->getRequest()->getPost('produto_extra_local','');
			$id_cliente=$this->getRequest()->getPost('produto_extra_id_cliente',null);
			if (!empty($produto_extra_id_produto) && !empty($produto_extra_local)) {
				$dados_upd = array(
					'desconto'		=> Porto80_Core::converteFloat($this->getRequest()->getPost('produto_extra_desconto',0)),
					'preco'			=> Porto80_Core::converteFloat($this->getRequest()->getPost('produto_extra_preco',0)),
					'iva'			=> Porto80_Core::converteFloat($this->getRequest()->getPost('produto_extra_iva',0)),
					'ipi'			=> Porto80_Core::converteFloat($this->getRequest()->getPost('produto_extra_ipi',0)),
					'icms_origem'	=> Porto80_Core::converteFloat($this->getRequest()->getPost('produto_extra_icms_origem',0)),
					'frete'			=> Porto80_Core::converteFloat($this->getRequest()->getPost('produto_extra_frete',0)),
					'margem'		=> Porto80_Core::converteFloat($this->getRequest()->getPost('produto_extra_margem',0))
				);
				//REMOVENDO VIRGULA
				foreach($dados_upd as $i=>$v) $dados_upd[$i]=str_replace(',','.',$v);
				//var_dump($dados_upd);
				$dbAdapter->update('produto_extra', $dados_upd,'id_produto="'.$produto_extra_id_produto.'" AND local="'.$produto_extra_local.'"');
				
				//NOVO VALOR DO PRODUTO
				$produto_valor = Porto80_Core::getProdutoValor($produto_extra_id_produto, $produto_extra_local, $id_cliente);
				$dadosValor = array(
					'vlliquido'=>Porto80_Core::converteFloat($produto_valor['liquido'],2),
					'vlfinal'=>Porto80_Core::converteFloat($produto_valor['final'],2),
					'vlmargem'=>Porto80_Core::converteFloat($produto_valor['margem'],2)
				);
				
				//EXIBINDO DADOS
				echo json_encode(array('status'=>1,'dados'=>$dadosValor));
			}

     	} catch (Zend_Db_Exception $e) {
        	echo json_encode(array('status'=>0,'mensagem'=>Porto80_Core::remove_accents($e->getMessage())));
        }
	}
    
    public function pesquisarAction() {
    	
        try {
        	
			$dbAdapter = Zend_Db_Table::getDefaultAdapter ();
			
			//------------------------------ VERIFICANDO SE PRECISA ATUALIZAR ALGUM PRODUTO
			$produto_extra_id_produto=$this->getRequest()->getPost('produto_extra_id_produto',0);
			$produto_extra_local=$this->getRequest()->getPost('produto_extra_local','');
			if (!empty($produto_extra_id_produto) && !empty($produto_extra_local)) {
				$dados_upd = array(
					'desconto'		=> Porto80_Core::converteFloat($this->getRequest()->getPost('produto_extra_desconto',0)),
					'preco'			=> Porto80_Core::converteFloat($this->getRequest()->getPost('produto_extra_preco',0)),
					'iva'			=> Porto80_Core::converteFloat($this->getRequest()->getPost('produto_extra_iva',0)),
					'ipi'			=> Porto80_Core::converteFloat($this->getRequest()->getPost('produto_extra_ipi',0)),
					'icms_origem'	=> Porto80_Core::converteFloat($this->getRequest()->getPost('produto_extra_icms_origem',0)),
					'frete'			=> Porto80_Core::converteFloat($this->getRequest()->getPost('produto_extra_frete',0)),
					'margem'		=> Porto80_Core::converteFloat($this->getRequest()->getPost('produto_extra_margem',0))
				);
				//REMOVENDO VIRGULA
				foreach($dados_upd as $i=>$v) $dados_upd[$i]=str_replace(',','.',$v);
				//var_dump($dados_upd);
				$dbAdapter->update('produto_extra', $dados_upd,'id_produto="'.$produto_extra_id_produto.'" AND local="'.$produto_extra_local.'"');
			}
	        
			$id_produto_revenda = $this->getRequest()->getParam('revenda');
	        $id_produto_categoria = $this->getRequest()->getParam('categoria');
	        $id_cliente = $this->getRequest()->getParam('cliente');
	        
	        $this->view->id_produto_categoria = $id_produto_categoria;
	        $this->view->id_cliente = $id_cliente;
	        $this->view->id_produto_revenda = $id_produto_revenda;
	        
	        if (!empty($id_produto_categoria)) {
	        
		        $select = $dbAdapter->select()->from(array('P'=>'produto'))
		        ->joinLeft(array('PC'=>'produto_categoria'),'PC.id_produto_categoria=P.id_produto_categoria',array('categoria'=>'PC.titulo'))
		        ->joinLeft(array('PE'=>'produto_embalagem'),'PE.id_produto_embalagem=P.id_produto_embalagem',array('embalagem'=>'PE.titulo'))
				->joinLeft(array('PL'=>'produto_linha'),'PL.id_produto_linha=PC.id_produto_linha',array('linha'=>'PL.titulo'))
				->where('PC.id_produto_categoria='.$id_produto_categoria)
		        //->order('P.produto_ordem ASC')->order('PE.ordem');
				->order('P.grupo_ordem ASC')->order('P.produto_ordem ASC')->order('P.titulo ASC')->order('PE.ordem ASC');
		        
		        /* REVENDA */
		        if (!empty($id_produto_revenda)) {
		        	$select->where('PL.id_produto_revenda='.$id_produto_revenda);
		        }
		        
		        $resultados = $dbAdapter->fetchAll($select);
		        
		        if (!$resultados) {
		        	
		        	$dados=array();
		        		
		        } else {
		        
			        foreach($resultados as $resultado) {
			        	
						/* EXTRAS */
						$select = $dbAdapter->select()->from(array('PE'=>'produto_extra'))
						->where('PE.id_produto='.$resultado['id_produto'])
						->order('PE.local ASC');
						$dadosR = $dbAdapter->fetchAll($select); $dadosExtra='';
						foreach($dadosR as $extra) {
							$produto_valor = Porto80_Core::getProdutoValor($resultado['id_produto'], $extra['local'], $id_cliente);
							$dadosExtra[] = array(
								'local'=>$extra['local'],
								'desconto'=>Porto80_Core::converteFloat($extra['desconto'],2),
								'preco'=>Porto80_Core::converteFloat($extra['preco'],2),
								'iva'=>Porto80_Core::converteFloat($extra['iva'],2),
								'ipi'=>Porto80_Core::converteFloat($extra['ipi'],2),
								'icms_origem'=>Porto80_Core::converteFloat($extra['icms_origem'],2),
								'icms_destino'=>Porto80_Core::converteFloat($extra['icms_destino'],2),
								'frete'=>Porto80_Core::converteFloat($extra['frete'],2),
								'margem'=>Porto80_Core::converteFloat($extra['margem'],2),
								'vlliquido'=>Porto80_Core::converteFloat($produto_valor['liquido'],2),
								'vlfinal'=>Porto80_Core::converteFloat($produto_valor['final'],2),
								'vlmargem'=>Porto80_Core::converteFloat($produto_valor['margem'],2)
							);
						}
						
						$resultado['extras'] = $dadosExtra;
						$dados[] = $resultado;
			        }
		        	
		        }
		        
                if (!empty($id_cliente)) {
		        	/* GRUPO ATUAL */
	        		$select = $dbAdapter->select()->from(array('CG'=>'cliente_grupo'),array('titulo'))
	        		->joinInner(array('PC'=>'produto_categoria'),'PC.id_produto_categoria='.$id_produto_categoria,array())
	        		->joinInner(array('CGP'=>'cliente_grupo_produto'),'CG.id_cliente_grupo=CGP.id_cliente_grupo AND CGP.id_produto_linha=PC.id_produto_linha',array())
	        		->joinLeft(array('CGA'=>'cliente_grupo_acrescimo'),'CG.id_cliente_grupo=CGA.id_cliente_grupo AND PC.id_produto_linha=CGP.id_produto_linha',array('acrescimo'))		        	
		        	->where('CGP.id_cliente='.$id_cliente);
		        	$this->view->cliente_grupo = $dbAdapter->fetchRow($select);
	        	}
		        
	        } else {
	        	$dados=array();
	        }
	            
	        $this->view->dados = $dados;
	        

	        
			/* CATEGORIAS */
			$select = $dbAdapter->select()->from(array('PC'=>'produto_categoria'),array('categoria'=>'PC.titulo','PC.id_produto_categoria'))
			->joinInner(array('PG'=>'produto_linha'),'PG.id_produto_linha=PC.id_produto_linha',array('grupo'=>'PG.titulo'))
			->order('PG.titulo ASC')->order('PC.titulo ASC');
			/* REVENDA */
			if (!empty($id_produto_revenda)) {
				$select->where('PG.id_produto_revenda='.$id_produto_revenda);
			}
			$dados = $dbAdapter->fetchAll($select);
			$categorias='';
			foreach($dados as $dado) {
				$categorias[$dado['grupo']][]=array('id_produto_categoria'=>$dado['id_produto_categoria'],'titulo'=>$dado['categoria']);
			}
			if ($categorias=='') $categorias=array();
			$this->view->categorias = $categorias;
			
			/* CLIENTES */
			$select = $dbAdapter->select()->from(array('C'=>'cliente'),array('id_cliente','codigo','nome','razao_social'))
			->joinLeft(array('CR'=>'cliente_revenda'),'CR.id_cliente=C.id_cliente',array('id_produto_revenda'))
			->order('C.razao_social ASC');
			/* REVENDA */
			if (!empty($id_produto_revenda)) {
				$select->where('CR.id_produto_revenda='.$id_produto_revenda);
			}
			$this->view->clientes = $dbAdapter->fetchAll($select);
			
			/* REVENDAS */
			$select  = $dbAdapter->select()->from(array('PR'=>'produto_revenda'))->order('PR.titulo ASC');
			$this->view->revendas = $dbAdapter->fetchAll($select);
        	        	
        } catch (Zend_Db_Exception $e) {
        	$this->_helper->FlashMessenger( array('warning' => htmlentities($e->getMessage()) ) );
        	$this->_helper->redirector('index','admin-resumo');
        }
        
        /* ARMAZENA A URL DE PESQUISA */
    	$filtros = new Zend_Session_Namespace('filtros');
    	$keyfiltro = md5($this->_request->getControllerName().'_'.$this->_request->getActionName());
		$filtros->$keyfiltro = $this->view->url();
		$this->view->keyfiltro = $keyfiltro;
    	
    }
    
}

