<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{

	protected function _initRequest() {
		
        $this->bootstrap('FrontController');
        $front = $this->getResource('FrontController');
        $request = new Zend_Controller_Request_Http();
        $front->setRequest($request);
                
	}

	public function _initRoutes()
	{
	    $this->bootstrap('FrontController');
	    $this->_frontController = $this->getResource('FrontController');
	    $router = $this->_frontController->getRouter();

		$router->addRoute('profissional',
			new Zend_Controller_Router_Route(
        	'profissionais/:categoria',
				array(
					'controller' => 'profissionais',
					'action'     => 'index'
				)
			)
		);
		$router->addRoute('carrinho_revenda',
			new Zend_Controller_Router_Route(
        	'carrinho/r/:revenda',
				array(
					'controller' => 'carrinho',
					'action'     => 'index'
				)
			)
		);
		$router->addRoute('produtos_revenda',
			new Zend_Controller_Router_Route(
        	'produtos/:revenda',
				array(
					'controller' => 'produtos',
					'action'     => 'exibir'
				)
			)
		);
		$router->addRoute('produtos_linha',
			new Zend_Controller_Router_Route(
        	'produtos/:revenda/:linha',
				array(
					'controller' => 'produtos',
					'action'     => 'exibir'
				)
			)
		);
		$router->addRoute('produtos_categoria',
			new Zend_Controller_Router_Route(
        	'produtos/:revenda/:linha/:categoria',
				array(
					'controller' => 'produtos',
					'action'     => 'exibir'
				)
			)
		);
		
		$router->addRoute('onde_encontro_revenda',
			new Zend_Controller_Router_Route(
        	'onde-encontro/:revenda/',
				array(
					'controller' => 'onde-encontro',
					'action'     => 'exibir'
				)
			)
		);
		
		$router->addRoute('onde_encontro_cidade',
			new Zend_Controller_Router_Route(
        	'onde-encontro/:revenda/:estado/:cidade',
				array(
					'controller' => 'onde-encontro',
					'action'     => 'exibir'
				)
			)
		);
		
		$router->addRoute('noticias_categoria',
				new Zend_Controller_Router_Route(
						'noticias/:categoria/',
						array(
								'controller' => 'noticias',
								'action'     => 'index'
						)
				)
		);
		
		$router->addRoute('noticias_categoria_noticia',
				new Zend_Controller_Router_Route(
						'noticias/:categoria/:noticia',
						array(
								'controller' => 'noticias',
								'action'     => 'index'
						)
				)
		);
        
    }
    
    protected function _initDoctype()
    {
        $this->bootstrap('view');
        $view = $this->getResource('view');
        $view->doctype('XHTML1_STRICT');
        $this->view->headTitle('Vic Junior Representa��es Comerciais');
        
        //TEMA
        $view->temaColor='blue';
        
        //URL ATUAL
        $view->currentUrl = Zend_Controller_Front::getInstance()->getRequest()->getRequestUri();
        	
    }  

}

