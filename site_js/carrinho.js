function number_format( number, decimals, dec_point, thousands_sep ) {
    // %        nota 1: Para 1000.55 retorna com precis�o 1 no FF/Opera � 1,000.5, mas no IE � 1,000.6
    // *     exemplo 1: number_format(1234.56);
    // *     retorno 1: '1,235'
    // *     exemplo 2: number_format(1234.56, 2, ',', ' ');
    // *     retorno 2: '1 234,56'
    // *     exemplo 3: number_format(1234.5678, 2, '.', '');
    // *     retorno 3: '1234.57'
    // *     exemplo 4: number_format(67, 2, ',', '.');
    // *     retorno 4: '67,00'
    // *     exemplo 5: number_format(1000);
    // *     retorno 5: '1,000'
    // *     exemplo 6: number_format(67.311, 2);
    // *     retorno 6: '67.31'

    var n = number, prec = decimals;
    n = !isFinite(+n) ? 0 : +n;
    prec = !isFinite(+prec) ? 0 : Math.abs(prec);
    var sep = (typeof thousands_sep == "undefined") ? ',' : thousands_sep;
    var dec = (typeof dec_point == "undefined") ? '.' : dec_point;

    var s = (prec > 0) ? n.toFixed(prec) : Math.round(n).toFixed(prec); //fix for IE parseFloat(0.55).toFixed(0) = 0;

    var abs = Math.abs(n).toFixed(prec);
    var _, i;

    if (abs >= 1000) {
        _ = abs.split(/\D/);
        i = _[0].length % 3 || 3;

        _[0] = s.slice(0,i + (n < 0)) +
              _[0].slice(i).replace(/(\d{3})/g, sep+'$1');

        s = _.join(dec);
    } else {
        s = s.replace('.', dec);
    }

    return s;
}

//VERIFICA��O DE N�MERICO
function is_numeric(strString)
	//check for valid numeric strings
	{
	var strValidChars = "0123456789";
	var strChar;
	var blnResult = true;

	if (strString.length == 0) return false;

	//test strString consists of valid characters listed above
	for (i = 0; i < strString.length && blnResult == true; i++)
	{
	strChar = strString.charAt(i);
	if (strValidChars.indexOf(strChar) == -1)
	  {
	  blnResult = false;
	  }
	}
	return blnResult;
}

function insertCarrinho(quantidade, codigo) {

	var carrinho_baseUrl = $('#carrinho_baseUrl').val();

	$.ajax({
		url: carrinho_baseUrl+"add-produto",
		type: 'POST',
		data: "id="+codigo+"&q="+quantidade,
		dataType: "json",
		success: function(json){
			if (json.status==1) {
				atualizaCarrinho();
			} else {
				alert(unescape(json.mensagem));
			}
		}
	});

}
function updateCarrinho(quantidade, codigo) {

	var carrinho_baseUrl = $('#carrinho_baseUrl').val();

	$.ajax({
		url: carrinho_baseUrl+"upd-produto",
		type: 'POST',
		data: "id="+codigo+"&q="+quantidade,
		dataType: "json",
		success: function(json){
			if (json.status==1) {
				atualizaCarrinho();
			} else {
				alert(unescape(json.mensagem));
			}
		}
	});

}
function deleteCarrinho(codigo) {

	var carrinho_baseUrl = $('#carrinho_baseUrl').val();

	$.ajax({
		url: carrinho_baseUrl+"del-produto",
		type: 'POST',
		data: "id="+codigo,
		dataType: "json",
		success: function(json){
			if (json.status==1) {
				atualizaCarrinho();
			} else {
				alert(unescape(json.mensagem));
			}
		}
	});

}
function atualizaCarrinho() {

	var carrinho_baseUrl = $('#carrinho_baseUrl').val();

	if (carrinho_baseUrl!='' && carrinho_baseUrl!=undefined) {
		var desconto='';
		var vlrckvp = $('input[name=vistaprazo]:checked');
		if (vlrckvp!=undefined) desconto=vlrckvp.val();
		var revenda = $('#revenda_permlink').val();
		if (revenda==undefined || revenda==null) revenda='';

		$.ajax({
			url: carrinho_baseUrl+"status",
			type: 'POST',
			dataType: "json",
			data:"desconto="+desconto+"&revenda="+revenda,
			success: function(json){
				if (json.status==1) {
					$('.carrinho_ti').html(json.total_item);
					if (revenda!='') {
						$('.carrinho_tvl').html('R$ '+json.total_valor_liquido); //R$
						$('.carrinho_tvlf').val(json.float_valor_liquido); //FLOAT
						$('.carrinho_tv').html('R$ '+json.total_valor_final); //R$
						$('.carrinho_tvf').val(json.float_valor_final); //FLOAT
						$('.carrinho_peso').html(""+json.total_peso+" kg");
					}
				} else {
					alert(unescape(json.mensagem));
				}
			}
		});
	}
}

$('input[name="vistaprazo"]').live('click',function(e){
	var carrinho_baseUrl = $('#carrinho_baseUrl').val();
	var mp = $(this).val();
	var revenda = $('#revenda_permlink').val();
	$.get(carrinho_baseUrl+'upd-metodo-pagamento/mp/'+mp+'/revenda/'+revenda,function(retorno){
		//console.log(retorno);
		if (retorno=="1") window.location.href=window.location.href;
		else atualizaCarrinho(); 
	});
});

//HELPER PARA DEL
$('.btdelcart').live('click',function(e){
	e.preventDefault();

	var qtd_obj = $(this).parent().find('.qtd:first');
	var qtd_val = qtd_obj.val();
	/* O REMOVER PODE SER EM QUALQUER QUANTIDADE
	if (!is_numeric(qtd_val) || qtd_val<=0) {
		alert('O campo quantidade deve ser um valor inteiro maior que zero!');
		$(qtd_obj).focus();
		return false;
	}
	*/

	if (confirm("Deseja remover esse item da lista?")) {
		var prod_codigo = $(this).attr('rel');
		//ADICIONAR ITEM NO CARRINHO
		deleteCarrinho(prod_codigo);
		$objTR = $(this).parents('tr');
		$objTR.fadeOut('normal',function(){
			$objTable = $objTR.parents('table');
			$objTR.remove();
			//SE ERA O �LTIMO PRODUTO DO BLOCO, REMOVER O BLOCO
			if ($objTable.find('.dados').length==0) {
				$objTable.fadeOut('normal',function() {
					$objAbout = $objTable.parents('.about');
					$objTable.remove();
					//SE ERA A �LTIMA TABELA
					if ($objAbout.find('table').length==0) {
						$objAbout.fadeOut('normal',function(){
							$objAbout.remove();
							//VERIFICA SE REMOVEU TUDO
							if ( $('.about').length==0 ) window.location = window.location;
						});
					}

				});
			}
		});

	}

});

//HELPER PARA ADD
$('.btaddcart').live('click',function(e){
	e.preventDefault();
	var qtd_obj = $(this).parent().find('.qtd:first');
	var qtd_emb = $(this).parent().find('.qtd_emb:first').val();
	var qtd_val = parseInt(qtd_obj.val());
	if (!is_numeric(qtd_val) || qtd_val<=0) {
		alert('O campo quantidade deve ser um valor inteiro maior que zero!');
		$(qtd_obj).focus();
		return false;
	}
	//VERIFICA ARREDONDAMENTO DA EMBALAGEM
	var resto = parseInt(qtd_val) % parseInt(qtd_emb);
	if (resto!=0) {
		var somar = parseInt(qtd_emb) - resto;
		var new_qtd_val = parseInt(qtd_val)+somar;
		if (isNaN(new_qtd_val)) new_qtd_val=qtd_val;
		alert('Seu valor foi ajustado para '+new_qtd_val+' pois cada fardo/caixa possui '+qtd_emb+' unidades');
		//alert('qtde_emb: ' + qtd_emb + ' quantidade: ' + qtd_val + ' resto: '+ resto + ' new_qtde: '+ new_qtd_val); //DEBUG
		qtd_obj.val(new_qtd_val);
		qtd_val = new_qtd_val;
	}

	var prod_codigo = $(this).attr('rel');
	//ADICIONAR ITEM NO CARRINHO
	insertCarrinho(qtd_val, prod_codigo);

	//CRIA EFEITO
	$('.ui-effects-transfer').css('border','2px solid black');
		//$('.ui-effects-transfer').append($(this).clone());
	$(this).effect('transfer', { to: '#carrinho_img' }, 1500, function(){
		$('#carrinho').effect('shake', { times: 1 }, 200);
	});

});
//HELPER PARA PEGAR O ENTER
$('.tbaddcart .qtd, .tbupdcart .qtd').live('keypress',function(e){
	if ((e.keyCode)==13) {
		$btAdd = $(this).parent().find('.btaddcart:first');
		$btUpd = $(this).parent().find('.btupdcart:first');
		if ($btAdd.is(':visible')) $btAdd.trigger('click');
		if ($btUpd.is(':visible')) $btUpd.trigger('click');
	}
});

//HELPER PARA ADD
$('.btupdcart').live('click',function(e){
	e.preventDefault();
	var qtd_obj = $(this).parent().find('.qtd:first');
	var qtd_emb = $(this).parent().find('.qtd_emb:first').val();
	var qtd_val = qtd_obj.val();
	if (!is_numeric(qtd_val) || qtd_val<=0) {
		alert('O campo quantidade deve ser um valor inteiro maior que zero!');
		$(qtd_obj).focus();
		return false;
	}
	//VERIFICA ARREDONDAMENTO DA EMBALAGEM
	var resto = parseInt(qtd_val) % parseInt(qtd_emb);
	if (resto!=0) {
		var somar = parseInt(qtd_emb) - resto;
		var new_qtd_val = parseInt(qtd_val)+somar;
		if (isNaN(new_qtd_val)) new_qtd_val=qtd_val;
		alert('Seu valor foi ajustado para '+new_qtd_val+' pois cada fardo/caixa possui '+qtd_emb+' unidades');
		//alert('qtde_emb: ' + qtd_emb + ' quantidade: ' + qtd_val + ' resto: '+ resto + ' new_qtde: '+ new_qtd_val); //DEBUG
		qtd_obj.val(new_qtd_val);
		qtd_val = new_qtd_val;
	}

	var prod_codigo = $(this).attr('rel');
	//ATUALIZAR ITEM NO CARRINHO
	updateCarrinho(qtd_val, prod_codigo);

	//CRIA EFEITO
	$('.ui-effects-transfer').css('border','2px solid black');
		//$('.ui-effects-transfer').append($(this).clone());
	$(this).effect('transfer', { to: '#carrinho_img' }, 1500, function(){
		$('#carrinho').effect('shake', { times: 1 }, 200);
	});

});

$(document).ready(function(){

	$(".tipT").tipTip({
        defaultPosition:"top",
        edgeOffset:5,
        maxWidth:"auto"
	});

	$(".tipB").tipTip({
        defaultPosition:"bottom",
        edgeOffset:5,
        maxWidth:"auto"
	});

	$(".tipR").tipTip({
        defaultPosition:"right",
        edgeOffset:5,
        maxWidth:"auto"
	});

	//BOTAO TROCA VISTA PRAZO
	$('input[name=vistaprazo]').bind('click',function(e){
		//console.log('e');
		atualizaCarrinho();
	});
	atualizaCarrinho(); //ATUALIZA O CARRINHO ASSIM QUE CARREGA A P�GINA

	$('#btConcluirPed').bind('click',function(e){

		var pvm = parseFloat($('#carrinho_pvm').val());
		var tvl = parseFloat($('.carrinho_tvlf:first').val());
		
		if ( tvl < pvm) {
			alert('Essa revenda possui um limite minimo de pedido de R$ '+ (number_format(pvm, 2, ',', '.')) +'!\nNao e possivel fechar um pedido abaixo desse valor');
			e.preventDefault();
			return false;
		} else {
			if (confirm('Confirma a conclusao e envio do pedido?')) {
				$('#frmConcluirPed').submit();
			} else {
				e.preventDefault();
				return false;
			}
		}
	});

	//SELE��O DO CLIENTE
	$('#adm_cliente').bind('change',function(e){
		$('body').append('<form id="adm_frmSetCliente" method="post"></form>');
		$('#adm_cliente').appendTo($('#adm_frmSetCliente'));
		$('#adm_frmSetCliente').submit();
	});
	
	//SELEÇÃO DE REVENDA NO CARRINHO
	$('#carrinho_revenda').change(function(){
		window.location = $(this).val();
	});

});
