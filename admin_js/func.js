// Administry object setup
if (!Funcoes) var Funcoes = {}

Funcoes.wysiwyg = function (obj) {

	$(obj).wysiwyg({
	    controls: {
		justifyLeft: { visible: false },
		justifyCenter: { visible: false },
		justifyRight: { visible: false },
		justifyFull: { visible: false },
		indent: { visible: false },
		outdent: { visible: false },
		subscript: { visible: false },
		superscript: { visible: false },
		insertOrderedList: { visible: false },
		insertUnorderedList: { visible: false },
		insertHorizontalRule: { visible: false },
		createLink: { visible: false },
		insertImage: { visible: false },
		insertTable: { visible: false },
		removeFormat: { visible: false },
	    h1: { visible: false },
		h2: { visible : false },
		h3: { visible: false }
		}
	});
	
};

Funcoes.tinyMCE = function(baseUrlJs, obj) {
	
	//alert(baseUrlJs+'/tiny_mce/tiny_mce.js');
	
    //------------- TINY MCE -------------------------------
	$(obj).tinymce({
    		script_url : baseUrlJs+'/tiny_mce/tiny_mce.js',
            theme : "advanced",
            language: "pt",
            plugins : "table,safari,advlink,searchreplace,paste,fullscreen",
            theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,cut,copy,paste,pastetext,pasteword,|,search,replace,|,link,unlink,fullscreen",
            theme_advanced_buttons2 : "tablecontrols",
            theme_advanced_buttons3 : "",
            theme_advanced_buttons4 : "",
            theme_advanced_toolbar_location : "top",
            theme_advanced_toolbar_align : "left",
            theme_advanced_statusbar_location : "bottom",
            theme_advanced_resizing : true,
            paste_use_dialog : false,
            paste_auto_cleanup_on_paste : true,
            paste_convert_headers_to_strong : false,
            paste_strip_class_attributes : "all",
            paste_remove_spans : true,
            paste_remove_styles : true,
            paste_preprocess : function(pl, o) {
                o.content = o.content.replace(/<br[^<>]*>/ig,"[br]");
                o.content = o.content.replace(/(<p[^<>]*>)/ig,"[p]");
                o.content = o.content.replace(/(<\/p[^<>]*>)/ig,"[/p]");

                o.content = o.content.replace(/(<\S([^<>]*)>)/ig,"");

                o.content = o.content.replace(/(\[br\])/ig,"<br />");
                o.content = o.content.replace(/(\[p\])/ig,"<p>");
                o.content = o.content.replace(/(\[\/p\])/ig,"</p>");
            }
    });
    //------------- TINY MCE -------------------------------
	
};

//FUN��O RAPIDA PARA CALCULO DE PRE�O DO PRODUTO (SEM ACRESCIMO)
//TODOS OS CAMPOS JA DEVEM SER ENVIADOS PRE-FORMATADOS
//[R$]preco(float), [%]desconto(float), [%]frete(float), [%]ipi(float), [%]iva(float), [%]icms(float)
Funcoes.getProdutoValor = function(preco, desconto, frete, ipi, iva, icms, margem) {
	
	var valor_tabela, valor_liquido, valor_base, valor_x, valor_y, valor_substrib, valor_final, valor_margem;
	
	valor_tabela = preco;
	valor_liquido = valor_tabela - (valor_tabela * desconto / 100);
	valor_base = valor_liquido + (valor_liquido * frete / 100);
	valor_base = valor_base + (valor_base * ipi / 100);
	valor_base = valor_base + (valor_base * iva / 100);
	valor_x = (valor_base * icms / 100);
	valor_y = valor_liquido + (valor_liquido * frete / 100);
	valor_y = valor_y * (icms / 100);
	valor_substrib = valor_x - valor_y;
	valor_final = valor_liquido + (valor_liquido * frete / 100);
	valor_final = valor_final + (valor_final * ipi / 100);
	valor_final = valor_final + valor_substrib;
	valor_margem = valor_final + (valor_final * margem / 100);
	
	return {'liquido':valor_liquido,'final':valor_final,'margem':valor_margem};

};

Funcoes.number_format = function ( number, decimals, dec_point, thousands_sep ) {
    // %        nota 1: Para 1000.55 retorna com precis�o 1 no FF/Opera � 1,000.5, mas no IE � 1,000.6
    // *     exemplo 1: number_format(1234.56);
    // *     retorno 1: '1,235'
    // *     exemplo 2: number_format(1234.56, 2, ',', ' ');
    // *     retorno 2: '1 234,56'
    // *     exemplo 3: number_format(1234.5678, 2, '.', '');
    // *     retorno 3: '1234.57'
    // *     exemplo 4: number_format(67, 2, ',', '.');
    // *     retorno 4: '67,00'
    // *     exemplo 5: number_format(1000);
    // *     retorno 5: '1,000'
    // *     exemplo 6: number_format(67.311, 2);
    // *     retorno 6: '67.31'
 
    var n = number, prec = decimals;
    n = !isFinite(+n) ? 0 : +n;
    prec = !isFinite(+prec) ? 0 : Math.abs(prec);
    var sep = (typeof thousands_sep == "undefined") ? ',' : thousands_sep;
    var dec = (typeof dec_point == "undefined") ? '.' : dec_point;
 
    var s = (prec > 0) ? n.toFixed(prec) : Math.round(n).toFixed(prec); //fix for IE parseFloat(0.55).toFixed(0) = 0;
 
    var abs = Math.abs(n).toFixed(prec);
    var _, i;
 
    if (abs >= 1000) {
        _ = abs.split(/\D/);
        i = _[0].length % 3 || 3;
 
        _[0] = s.slice(0,i + (n < 0)) +
              _[0].slice(i).replace(/(\d{3})/g, sep+'$1');
 
        s = _.join(dec);
    } else {
        s = s.replace('.', dec);
    }
 
    return s;
    
};