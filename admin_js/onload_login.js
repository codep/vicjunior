$(document).ready(function()
{
	// We'll catch form submission to do it in AJAX, but this works also with JS disabled
	$('#loginform').submit(function(event)
	{
		// Stop full page load
		event.preventDefault();
		
		// Check fields
		var login = $('#username').val();
		var pass = $('#password').val();
		
		if (!login || login.length == 0)
		{
			alert('Por favor informe seu usu�rio');
			$('#username').focus();
		}
		else if (!pass || pass.length == 0)
		{
			alert('Por favor informe sua senha');
			$('#password').focus();
		}
		else
		{		
			// Target url
			var target = $(this).attr('action');
			if (!target || target == '')
			{
				// Page url without hash
				target = document.location.href.match(/^([^#]+)/)[1];
			}
			
			// Request
			var data = {
				username: login,
				password: pass
			};
			var redirect = $('#redirect');
			if (redirect.length > 0)
			{
				data.redirect = redirect.val();
			}
			
			var sendTimer = new Date().getTime();
			
			// Send
			$.ajax({
				url: target,
				dataType: 'json',
				type: 'POST',
				data: data,
				beforeSend: function() {
					$('#login-block').html('Aguarde, efetuando autentica��o ...');
				},
				success: function(json, textStatus, XMLHttpRequest)
				{
					if (json.status==1)
					{
						$('#login-block').html('Autenticado! Acessando sistema ...');
						setTimeout(function(){
							document.location.href = data.redirect;
						}, 1500);
					}
					else
					{
						$('#login-block').html(unescape(json.mensagem));
					}
				},
				error: function(XMLHttpRequest, textStatus, errorThrown)
				{
					$('#login-block').html('N�o foi poss�vel acessar o servidor, tente novamente');
				}
			});

		}
	});
});